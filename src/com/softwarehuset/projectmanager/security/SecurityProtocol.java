package com.softwarehuset.projectmanager.security;

import com.softwarehuset.projectmanager.project.Project;
import com.softwarehuset.projectmanager.project.ProjectActivity;
import com.softwarehuset.projectmanager.user.User;

// Tobias H. M�ller s184217
public interface SecurityProtocol {

	public boolean isAuthorizedForProjectCreation(User user);

	public boolean isAuthorizedToSetProjectManager(User user);

	public boolean isAuthorizedToCreateActivity(User user, Project project);

	public boolean isAuthorizedToDeleteActivity(User user, Project project);

	public boolean isAuthorizedToAssignEmployeeToActivity(User user, Project project);

	public boolean isAuthorizedToRegistrateWork(User user, ProjectActivity activity);

	public boolean isAuthorizedToRequestHelp(User user, ProjectActivity activity);

	public boolean isAuthorizedToEditForeignWorkRegistration(User user, String date, ProjectActivity activity);

	public boolean isAuthorizedToRegistrateForeignWork(User user, String date, ProjectActivity activity);

	public boolean isAuthorizedToSetActivitySpan(User user, Project project);

	public boolean isAuthorizedToCreatePersonalActivity(User user);

	public boolean isAuthorizedToSetExpectedHours(User user, Project project);

}
