package com.softwarehuset.projectmanager.security;

import com.softwarehuset.projectmanager.project.Project;
import com.softwarehuset.projectmanager.project.ProjectActivity;
import com.softwarehuset.projectmanager.user.User;

// Tobias H. M�ller s184217
public class DefaultSecurityProtocol implements SecurityProtocol {

	@Override
	public boolean isAuthorizedForProjectCreation(User user) {
		return user.isAdmin();
	}

	@Override
	public boolean isAuthorizedToSetProjectManager(User user) {
		return user.isAdmin();
	}

	@Override
	public boolean isAuthorizedToCreateActivity(User user, Project project) {
		return user.isAdmin() || user.getId().equals(project.getProjectManagerId());
	}

	@Override
	public boolean isAuthorizedToDeleteActivity(User user, Project project) {
		return user.isAdmin() || user.getId().equals(project.getProjectManagerId());
	}

	@Override
	public boolean isAuthorizedToAssignEmployeeToActivity(User user, Project project) {
		return user.isAdmin() || user.getId().equals(project.getProjectManagerId());
	}

	@Override
	public boolean isAuthorizedToRegistrateWork(User user, ProjectActivity activity) {
		return activity.hasEmployee(user.getId());
	}

	@Override
	public boolean isAuthorizedToRequestHelp(User user, ProjectActivity activity) {
		return user.isAdmin() || activity.hasEmployee(user.getId());
	}

	@Override
	public boolean isAuthorizedToEditForeignWorkRegistration(User user, String date, ProjectActivity activity) {
		return activity.getMainRecord().get(user.getId(), date).isPresent();
	}

	@Override
	public boolean isAuthorizedToRegistrateForeignWork(User user, String date, ProjectActivity activity) {
		return activity.getPreRecord().get(user.getId(), date).isPresent();
	}

	@Override
	public boolean isAuthorizedToSetActivitySpan(User user, Project project) {
		return user.isAdmin() || user.getId().equals(project.getProjectManagerId());
	}

	@Override
	public boolean isAuthorizedToCreatePersonalActivity(User user) {
		return !user.isAdmin();
	}

	@Override
	public boolean isAuthorizedToSetExpectedHours(User user, Project project) {
		return user.isAdmin() || user.getId().equals(project.getProjectManagerId());
	}
}
