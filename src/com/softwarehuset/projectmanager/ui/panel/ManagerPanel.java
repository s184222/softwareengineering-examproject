package com.softwarehuset.projectmanager.ui.panel;

import javax.swing.JPanel;

import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.ui.Display;

//Rasmus M. Larsen s184190
@SuppressWarnings("serial")
public abstract class ManagerPanel extends JPanel {

	protected final ProjectManagerFacade facade;
	protected final Display display;
	
	public ManagerPanel(ProjectManagerFacade facade, Display display) {
		this.facade = facade;
		this.display = display;
	}

	public abstract void onEnter();

	public abstract void onExit();
}
