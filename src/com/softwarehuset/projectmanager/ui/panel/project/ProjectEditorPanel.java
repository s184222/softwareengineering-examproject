package com.softwarehuset.projectmanager.ui.panel.project;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.softwarehuset.projectmanager.app.facade.ProjectActivityInfo;
import com.softwarehuset.projectmanager.app.facade.ProjectInfo;
import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.exceptions.InvalidEmployeeException;
import com.softwarehuset.projectmanager.exceptions.NameUnavailableException;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.projectmanager.exceptions.ProjectIdGenerationException;
import com.softwarehuset.projectmanager.ui.Display;
import com.softwarehuset.projectmanager.ui.GUIHelper;
import com.softwarehuset.projectmanager.ui.panel.FormPanel;
import com.softwarehuset.projectmanager.ui.panel.activity.ActivityEditorPanel;

//Christian M. Fuglsang s184222
@SuppressWarnings("serial")
public class ProjectEditorPanel extends FormPanel {

	private static final String PROJECT_INFO_BORDER_TITLE = "Project information";
	private static final String ACTIVITY_LIST_BORDER_TITLE = "Project activities";
	
	private static final String CREATE_NEW_TITLE = "Create new project";
	private static final String EDIT_PROJECT_TITLE = "Edit project";
	
	private static final String CREATE_LABEL = "Create project";
	private static final String CANCEL_LABEL = "Cancel";

	private static final String SAVE_LABEL = "Save project";
	private static final String BACK_LABEL = "Back";
	
	private static final String REMOVE_ACTIVITY_LABEL = "Remove activity";
	private static final String ADD_ACTIVITY_LABEL = "Add new activity";
	private static final String EDIT_ACTIVITY_LABEL = "Edit activity";
	
	private static final String EMPTY_FIELD = "-";
	
	private static final String SET_MANAGER_ERROR = "Unable to set project manager: ";
	private static final String UNABLE_TO_CREATE_ERROR = "Unable to create project: ";
	private static final String EMPTY_NAME_ERROR = "Name is empty";
	private static final String ACTIVITY_LOAD_ERROR = "Unable to load activity";
	private static final String REMOVE_ACTIVITY_ERROR = "Unable to remove activity: ";

	private static final String REMOVE_ACTIVITY_SUCCESS = "Removed activity successfully";
	private static final String PROJECT_SAVED_SUCCESS = "Saved project successfully";
	
	private final JButton saveButton;
	private final JButton cancelButton;
	
	private final JButton removeActivityButton;
	private final JButton addActivityButton;
	private final JButton editActivityButton;
	
	private final JLabel statusLabel;
	private final JPanel activityListContainer;
	
	private final ProjectInfoForm projectInfo;
	private final ActivityListPanel activityListPanel;
	
	private boolean createNewProject;
	private boolean allowEmptyManager;
	
	private String projectId;
	
	public ProjectEditorPanel(ProjectManagerFacade facade, Display display) {
		super(facade, display);
		
		saveButton = new JButton(SAVE_LABEL);
		cancelButton = new JButton(CANCEL_LABEL);
		
		removeActivityButton = new JButton(REMOVE_ACTIVITY_LABEL);
		addActivityButton = new JButton(ADD_ACTIVITY_LABEL);
		editActivityButton = new JButton(EDIT_ACTIVITY_LABEL);
		
		statusLabel = new JLabel();

		activityListContainer = new JPanel();
		activityListContainer.setBorder(BorderFactory.createTitledBorder(ACTIVITY_LIST_BORDER_TITLE));
		
		projectInfo = new ProjectInfoForm();
		projectInfo.setBorder(BorderFactory.createTitledBorder(PROJECT_INFO_BORDER_TITLE));
		activityListPanel = new ActivityListPanel();
		
		uiLayout();
		uiEvents();
	}
	
	private void uiLayout() {
		JPanel form = new JPanel();
		form.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.NONE;

		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		form.add(projectInfo, constraints);
		projectInfo.setOpaque(false);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		
		// Add filler
		constraints.gridy++;
		constraints.ipady = 10;
		constraints.fill = GridBagConstraints.BOTH;
		form.add(Box.createVerticalBox(), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.ipady = 0;
		
		activityListContainer.setLayout(new BoxLayout(activityListContainer, BoxLayout.Y_AXIS));
		activityListContainer.add(activityListPanel);
		activityListPanel.setOpaque(false);
		activityListContainer.add(Box.createVerticalStrut(10));
		
		JPanel activityButtonPanel = new JPanel();
		activityButtonPanel.setLayout(new BoxLayout(activityButtonPanel, BoxLayout.X_AXIS));
		activityButtonPanel.add(removeActivityButton);
		removeActivityButton.setOpaque(false);
		activityButtonPanel.add(Box.createHorizontalGlue());
		activityButtonPanel.add(addActivityButton);
		addActivityButton.setOpaque(false);
		activityButtonPanel.add(Box.createHorizontalStrut(5));
		activityButtonPanel.add(editActivityButton);
		editActivityButton.setOpaque(false);
		
		activityListContainer.add(activityButtonPanel);
		activityButtonPanel.setOpaque(false);
		
		constraints.gridy++;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		form.add(activityListContainer, constraints);
		activityListContainer.setOpaque(false);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		
		// Add filler
		constraints.gridy++;
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.BOTH;
		form.add(Box.createVerticalBox(), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weighty = 0.0;
		
		constraints.gridy++;
		constraints.anchor = GridBagConstraints.WEST;
		form.add(statusLabel, constraints);
		constraints.anchor = GridBagConstraints.CENTER;
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		buttonPanel.add(saveButton);
		saveButton.setOpaque(false);
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(cancelButton);
		cancelButton.setOpaque(false);

		constraints.gridy++;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		form.add(buttonPanel, constraints);
		buttonPanel.setOpaque(false);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		
		super.setupFormPanel(form);
	}
	
	private void uiEvents() {
		saveButton.addActionListener((e) -> {
			String projectId;

			if (createNewProject) {
				if (projectInfo.getProjectName().isEmpty()) {
					showErrorMessage(EMPTY_NAME_ERROR, null);
					return;
				}
				
				try {
					projectId = facade.createProject(projectInfo.getProjectName());
				} catch (PermissionDeniedException | ProjectIdGenerationException e1) {
					showErrorMessage(UNABLE_TO_CREATE_ERROR, e1);
					return;
				}
			} else {
				projectId = projectInfo.getProjectId();
			}
			
			boolean error = false;
			
			String projectManager = projectInfo.getProjectManager();
			if (projectManager != null) {
				facade.selectProject(projectId);
				try {
					facade.setProjectManager(projectManager);

					if (allowEmptyManager)
						prepareProjectEdit(facade.getProjectInfoFromId(projectId));
				} catch (InvalidEmployeeException | PermissionDeniedException e1) {
					showErrorMessage(SET_MANAGER_ERROR, e1);
					error = true;
				}
				facade.unselect();
			} else if (createNewProject) {
				prepareProjectEdit(facade.getProjectInfoFromId(projectId));
			}
			
			if (!error)
				showSuccessMessage(PROJECT_SAVED_SUCCESS);
		});

		cancelButton.addActionListener((e) -> {
			display.setActiveCard(Display.DASHBOARD_CARD_ID);
		});
		
		activityEvents();
	}
	
	private void activityEvents() {
		removeActivityButton.addActionListener((e) -> {
			if (projectId != null) {
				facade.selectProject(projectId);
				ProjectActivityInfo info = activityListPanel.getSelectedActivityInfo(facade);
				if (info != null) {
					try {
						facade.removeActivityInProject(info.getName());
						updateActivityList();
						
						showSuccessMessage(REMOVE_ACTIVITY_SUCCESS);
					} catch (PermissionDeniedException | NameUnavailableException e1) {
						showErrorMessage(REMOVE_ACTIVITY_ERROR, e1);
					}
				}
				facade.unselect();
			}
		});
		
		addActivityButton.addActionListener((e) -> {
			if (projectId != null) {
				display.getActivityEditorPanel().prepareNewActivity(projectId);
				display.setActiveCard(Display.ACTIVITY_EDITOR_CARD_ID);
			}
		});
		
		editActivityButton.addActionListener((e) -> {
			if (projectId != null) {
				facade.selectProject(projectId);
				ProjectActivityInfo info = activityListPanel.getSelectedActivityInfo(facade);
				facade.unselect();
				
				if (info != null) {
					ActivityEditorPanel activityEditor = display.getActivityEditorPanel();
					activityEditor.prepareActivityEdit(projectId, info);
					display.setActiveCard(Display.ACTIVITY_EDITOR_CARD_ID);
				} else {
					showErrorMessage(ACTIVITY_LOAD_ERROR, null);
				}
			}
		});
	}
	
	private void showErrorMessage(String msg, Exception e) {
		statusLabel.setForeground(GUIHelper.ERROR_COLOR);
		
		if (e != null) {
			statusLabel.setText(msg + e.getMessage());
		} else {
			statusLabel.setText(msg);
		}
	}
	
	private void showSuccessMessage(String msg) {
		statusLabel.setForeground(GUIHelper.SUCCESS_COLOR);
		statusLabel.setText(msg);
	}
	
	@Override
	public void onEnter() {
		updateActivityList();
	}

	@Override
	public void onExit() {
	}
	
	private void updateActivityList() {
		if (projectId != null) {
			activityListPanel.updateCells(facade, projectId);
		} else {
			activityListPanel.clearCells();
		}
	}

	private void updateLabels() {
		if (createNewProject) {
			setTitleText(CREATE_NEW_TITLE);
	
			saveButton.setText(CREATE_LABEL);
			cancelButton.setText(CANCEL_LABEL);
		} else {
			setTitleText(EDIT_PROJECT_TITLE);
			
			saveButton.setText(SAVE_LABEL);
			cancelButton.setText(BACK_LABEL);
		}

		setActivityEnabled(!createNewProject);
		
		statusLabel.setText("");
	}
	
	public void prepareNewProject() {
		createNewProject = true;
		projectId = null;
		
		projectInfo.setProjectNameEditable(true);
		projectInfo.setProjectName("");
		projectInfo.setProjectId(EMPTY_FIELD);
		
		updateAvailableEmployees(true);
		projectInfo.setSelectedManager(null);
		
		updateLabels();
	}
	
	private void updateAvailableEmployees(boolean allowEmpty) {
		List<String> employees = facade.getAllEmployeeIds();
		projectInfo.setAvailableProjectManagers(employees.toArray(new String[employees.size()]), allowEmpty);
	
		allowEmptyManager = allowEmpty;
	}

	public void prepareProjectEdit(ProjectInfo info) {
		createNewProject = false;
		projectId = info.getProjectId();
		
		projectInfo.setProjectNameEditable(false);
		projectInfo.setProjectName(info.getName());
		projectInfo.setProjectId(info.getProjectId());
		
		updateAvailableEmployees(info.getProjectManagerId() == null);
		projectInfo.setSelectedManager(info.getProjectManagerId());
		
		updateLabels();
	}
	
	private void setActivityEnabled(boolean state) {
		activityListContainer.setEnabled(state);
		addActivityButton.setEnabled(state);
		editActivityButton.setEnabled(state);
		activityListPanel.setEnabled(state);
		removeActivityButton.setEnabled(state);
	}
}
