package com.softwarehuset.projectmanager.ui.panel.project;

import java.util.Collection;

import javax.swing.table.DefaultTableModel;

import com.softwarehuset.projectmanager.app.facade.ProjectActivityInfo;
import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.ui.GUIHelper;
import com.softwarehuset.projectmanager.ui.component.TableListPanel;

//Gustav Gr�nvold s184217
@SuppressWarnings("serial")
public class ActivityListPanel extends TableListPanel {

	private static final int NAME_COLUMN = 0;
	private static final int START_DATE_COLUMN = 1;
	private static final int END_DATE_COLUMN = 2;
	private static final int EXPECTED_HOURS_COLUMN = 3;
	private static final int ASSIGNED_NUM_COLUMN = 4;
	
	private static final String[] ACTIVITY_COLUMNS = new String[] {
		"Name",
		"Start week",
		"End week",
		"Expected hours",
		"Number of assigned"
	};
	
	public ActivityListPanel() {
		super(ACTIVITY_COLUMNS);
	}
	
	public void updateCells(ProjectManagerFacade facade, String projectId) {
		Collection<ProjectActivityInfo> infos = facade.getAllProjectActivityInfos(projectId);

		DefaultTableModel tableModel = getModel();
		
		int i = 0;
		tableModel.setRowCount(infos.size());
		for (ProjectActivityInfo info : infos) {
			tableModel.setValueAt(info.getName(), i, NAME_COLUMN);
			tableModel.setValueAt(GUIHelper.formatActivitySpan(info.getStartYear(), info.getStartWeek()), i, START_DATE_COLUMN);
			tableModel.setValueAt(GUIHelper.formatActivitySpan(info.getEndYear(), info.getEndWeek()), i, END_DATE_COLUMN);
			tableModel.setValueAt(GUIHelper.formatExpectedHours(info.getExpectedHours()), i, EXPECTED_HOURS_COLUMN);
			tableModel.setValueAt(Integer.toString(info.getNumberAssigned()), i, ASSIGNED_NUM_COLUMN);
			
			i++;
		}
		
		if (tableModel.getRowCount() != 0)
			getTable().getSelectionModel().setSelectionInterval(0, 0);
	}

	public ProjectActivityInfo getSelectedActivityInfo(ProjectManagerFacade facade) {
		DefaultTableModel tableModel = getModel();
		if (tableModel.getRowCount() <= 0)
			return null;
		
		int index = getTable().getSelectedRow();
		String activityName = (String)tableModel.getValueAt(index, NAME_COLUMN);
		return facade.getProjectActivityInfo(activityName);
	}
}
