package com.softwarehuset.projectmanager.ui.panel.project;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.softwarehuset.projectmanager.ui.component.HintetTextField;

//Christian M. Fuglsang s184222
@SuppressWarnings("serial")
public class ProjectInfoForm extends JPanel {

	private static final String PROJECT_NAME_LABEL = "Project name:";
	private static final String PROJECT_ID_LABEL = "Project ID:";
	private static final String PROJECT_MANAGER_LABEL = "Project manager:";

	private static final String NAME_HINT = "Insert project name...";
	
	private final HintetTextField nameField;
	private final JLabel projectIdField;
	private final JComboBox<String> managerSelector;
	
	public ProjectInfoForm() {
		nameField = new HintetTextField();
		nameField.setHint(NAME_HINT);
		managerSelector = new JComboBox<String>();
		projectIdField = new JLabel();
	
		uiLayout();
	}
	
	private void uiLayout() {
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.NONE;

		constraints.gridy = 0;
		addFieldRow(constraints, new JLabel(PROJECT_NAME_LABEL), nameField);

		constraints.gridy++;
		addFieldRow(constraints, new JLabel(PROJECT_ID_LABEL), projectIdField);
		
		constraints.gridy++;
		addFieldRow(constraints, new JLabel(PROJECT_MANAGER_LABEL), managerSelector);
	}
	
	private void addFieldRow(GridBagConstraints constraints, JLabel label, JComponent field) {
		constraints.gridx = 0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(label, constraints);
		constraints.fill = GridBagConstraints.NONE;
		
		constraints.gridx++;
		
		constraints.anchor = GridBagConstraints.WEST;
		constraints.weightx = 1.0;
		constraints.ipadx = 200;
		add(field, constraints);
		constraints.ipadx = 0;
		constraints.weightx = 0.0;
		constraints.anchor = GridBagConstraints.CENTER;
	}
	
	public void setProjectNameEditable(boolean state) {
		nameField.setEditable(state);
	}
	
	public void setProjectName(String projectName) {
		nameField.setText(projectName);
	}
	
	public String getProjectName() {
		return nameField.getText();
	}

	public void setProjectId(String id) {
		projectIdField.setText(id);
	}

	public String getProjectId() {
		return projectIdField.getText();
	}
	
	public void setAvailableProjectManagers(String[] projectManagers, boolean allowEmpty) {
		managerSelector.setModel(new DefaultComboBoxModel<String>(projectManagers));
		if (allowEmpty)
			managerSelector.insertItemAt("", 0);
	}

	public void setSelectedManager(String projectManagerId) {
		if (projectManagerId == null) {
			managerSelector.setSelectedIndex(0);
			return;
		}
		
		managerSelector.setSelectedItem(projectManagerId);
	}
	
	public String getProjectManager() {
		String selectedItem = (String)managerSelector.getSelectedItem();
		if (selectedItem.isEmpty())
			return null;
		return selectedItem;
	}
}
