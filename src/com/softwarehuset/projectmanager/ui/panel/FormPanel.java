package com.softwarehuset.projectmanager.ui.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.ui.Display;
import com.softwarehuset.projectmanager.ui.component.TitleLabel;

//Christian M. Fuglsang s184222
@SuppressWarnings("serial")
public abstract class FormPanel extends ManagerPanel {

	private static final Color FORM_BORDER_COLOR = new Color(217, 217, 217);
	
	private final TitleLabel title;

	public FormPanel(ProjectManagerFacade facade, Display display) {
		super(facade, display);
	
		title = new TitleLabel("NO TITLE");
	}
	
	protected void setupFormPanel(JPanel form) {
		JPanel formContainer = new JPanel();
		formContainer.setLayout(new BorderLayout());
		formContainer.setBackground(Color.WHITE);
		formContainer.setBorder(BorderFactory.createLineBorder(FORM_BORDER_COLOR));

		form.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		formContainer.add(form, BorderLayout.CENTER);
		form.setOpaque(false);
		
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(6, 5, 6, 5);
		constraints.weightx = 1.0;
		constraints.weighty = 0.0;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(title, constraints);
		
		constraints.gridy++;
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.BOTH;
		add(formContainer, constraints);
	}
	
	protected void setTitleText(String titleText) {
		title.setText(titleText);
	}
}
