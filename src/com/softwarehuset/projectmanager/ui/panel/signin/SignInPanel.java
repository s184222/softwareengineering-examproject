package com.softwarehuset.projectmanager.ui.panel.signin;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.exceptions.InvalidEmployeeException;
import com.softwarehuset.projectmanager.ui.Display;
import com.softwarehuset.projectmanager.ui.panel.ManagerPanel;

//Gustav Gr�nvold s184217
@SuppressWarnings("serial")
public class SignInPanel extends ManagerPanel {

	private static final String NO_SELECTION_ERROR = "No user selected";
	private static final String SIGN_IN_ERROR = "Unable to sign in";

	private static final String SELECT_USER_LABEL = "Select user:";
	private static final String SIGN_IN_LABEL = "Sign in";
	
	private static final Color SIGN_IN_CONTENT_BACKGROUND = Color.WHITE;
	
	private final JComboBox<String> userSelect;
	private final JButton signInButton;
	private final JLabel errorLabel;
	
	public SignInPanel(ProjectManagerFacade facade, Display display) {
		super(facade, display);
		
		userSelect = new JComboBox<String>(getAvailableUsers());
		signInButton = new JButton(SIGN_IN_LABEL);
		errorLabel = new JLabel("ERROR");
		errorLabel.setForeground(Color.RED);
		
		uiLayout();
		uiEvents();
	}
	
	private String[] getAvailableUsers() {
		List<String> userIds = facade.getAllUserIds();
		return userIds.toArray(new String[userIds.size()]);
	}

	private void uiLayout() {
		setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		
		JPanel content = new JPanel();
		content.setBackground(SIGN_IN_CONTENT_BACKGROUND);
		content.setBorder(createContentBorder());
		content.setLayout(new GridBagLayout());
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 1.0;
		constraints.anchor = GridBagConstraints.WEST;
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.insets.right = 30;
		content.add(new JLabel(SELECT_USER_LABEL), constraints);
		constraints.insets.right = 5;
		
		constraints.gridx++;
		userSelect.setOpaque(false);
		content.add(userSelect, constraints);
		
		constraints.gridx = 0;
		constraints.gridy++;
		constraints.gridwidth = 2;
		constraints.anchor = GridBagConstraints.CENTER;
		errorLabel.setOpaque(false);
		content.add(errorLabel, constraints);
		
		constraints.gridy++;

		constraints.insets.top = 10;
		signInButton.setOpaque(false);
		content.add(signInButton, constraints);
		constraints.insets.top = 5;
		
		setLayout(new GridBagLayout());

		constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.CENTER;
		add(content, constraints);
	}
	
	private Border createContentBorder() {
		Border contentBorder = BorderFactory.createEtchedBorder(EtchedBorder.RAISED);
		Border emptyBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);
		return BorderFactory.createCompoundBorder(contentBorder, emptyBorder);
	}
	
	private void uiEvents() {
		signInButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String selectedUser = (String)userSelect.getSelectedItem();
				if (selectedUser == null) {
					showError(NO_SELECTION_ERROR);
				} else {
					try {
						facade.signIn(selectedUser);
					} catch (InvalidEmployeeException e1) {
						showError(SIGN_IN_ERROR);
						return;
					}
					
					errorLabel.setVisible(false);
					display.setActiveCard(Display.DASHBOARD_CARD_ID);
				}
			}
		});
	}
	
	private void showError(String errorMessage) {
		errorLabel.setText(errorMessage);
		errorLabel.setVisible(true);
	}

	@Override
	public void onEnter() {
		errorLabel.setVisible(false);
		userSelect.setSelectedIndex(0);
	}

	@Override
	public void onExit() {
	}
}
