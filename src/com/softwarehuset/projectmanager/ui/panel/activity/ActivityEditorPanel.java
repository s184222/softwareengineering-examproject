package com.softwarehuset.projectmanager.ui.panel.activity;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.softwarehuset.projectmanager.app.facade.ProjectActivityInfo;
import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.exceptions.InvalidHourException;
import com.softwarehuset.projectmanager.exceptions.InvalidSpanException;
import com.softwarehuset.projectmanager.exceptions.NameUnavailableException;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.projectmanager.ui.Display;
import com.softwarehuset.projectmanager.ui.GUIHelper;
import com.softwarehuset.projectmanager.ui.panel.FormPanel;

//Gustav Gr�nvold s184217
@SuppressWarnings("serial")
public class ActivityEditorPanel extends FormPanel {

	private static final String ACTIVITY_INFO_BORDER_TITLE = "Activity information";
	private static final String ASSIGNED_EMPLOYEE_PANEL_TITLE = "Assigned employees";

	private static final String CREATE_ACTIVITY_TITLE = "Create new activity";
	private static final String EDIT_ACTIVITY_TITLE = "Edit activity";
	
	private static final String CANCEL_LABEL = "Cancel";
	private static final String BACK_LABEL = "Back";
	private static final String CREATE_LABEL = "Create activity";
	private static final String SAVE_LABEL = "Save activity";
	
	private static final String EMPTY_ACTIVITY_NAME_ERROR = "Activity name field is empty";
	private static final String INVALID_ACTIVITY_SPAN_ERROR = "Invalid or empty activity span";
	private static final String CREATE_ERROR = "Unable to create activity: ";
	private static final String WEEKSPAN_ERROR = "Unable to edit weekspan: ";
	private static final String EXPECTED_HOUR_ERROR = "Unable to set expected hours: ";
	
	private static final String ACTIVITY_SAVED_SUCCESS = "Activity saved successfully";
	
	private final JLabel statusLabel;
	
	private final JButton saveButton;
	private final JButton cancelButton;
	
	private final ActivityInfoForm activityInfo;
	private final AssignedEmployeePanel employeePanel;
	
	private String projectId;
	private boolean createNewActivity;
	
	public ActivityEditorPanel(ProjectManagerFacade facade, Display display) {
		super(facade, display);
	
		statusLabel = new JLabel();
		
		saveButton = new JButton();
		cancelButton = new JButton(CANCEL_LABEL);

		activityInfo = new ActivityInfoForm(display, true);
		activityInfo.setBorder(BorderFactory.createTitledBorder(ACTIVITY_INFO_BORDER_TITLE));
		employeePanel = new AssignedEmployeePanel(this, facade);
		employeePanel.setBorder(BorderFactory.createTitledBorder(ASSIGNED_EMPLOYEE_PANEL_TITLE));
		
		uiLayout();
		uiEvents();
	}
	
	private void uiLayout() {
		JPanel form = new JPanel();
		form.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.NONE;

		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		form.add(activityInfo, constraints);
		activityInfo.setOpaque(false);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;

		// Add filler
		constraints.gridy++;
		constraints.ipady = 20;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		form.add(Box.createVerticalBox(), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.ipady = 0;

		constraints.gridy++;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		form.add(employeePanel, constraints);
		employeePanel.setOpaque(false);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		
		// Add filler
		constraints.gridy++;
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.BOTH;
		form.add(Box.createVerticalBox(), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weighty = 0.0;
		
		constraints.gridy++;
		constraints.anchor = GridBagConstraints.WEST;
		form.add(statusLabel, constraints);
		constraints.anchor = GridBagConstraints.CENTER;

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		buttonPanel.add(saveButton);
		saveButton.setOpaque(false);
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(cancelButton);
		cancelButton.setOpaque(false);

		constraints.gridy++;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		form.add(buttonPanel, constraints);
		buttonPanel.setOpaque(false);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		
		super.setupFormPanel(form);
	}
	
	private void uiEvents() {
		saveButton.addActionListener((e) -> {
			if (activityInfo.getActivityName().isEmpty()) {
				showErrorMessage(EMPTY_ACTIVITY_NAME_ERROR, null);
				return;
			}
			
			if (!activityInfo.isValidActivitySpan()) {
				showErrorMessage(INVALID_ACTIVITY_SPAN_ERROR, null);
				return;
			}
				
			String activityName = activityInfo.getActivityName();
			
			facade.selectProject(projectId);
			if (createNewActivity) {
				try {
					facade.createActivity(activityName);
				} catch (NameUnavailableException | PermissionDeniedException e1) {
					showErrorMessage(CREATE_ERROR, e1);
					facade.unselect();
					return;
				}
			}
			
			boolean error = false;
			
			if (activityInfo.isValidActivitySpan()) {
				facade.selectActivity(activityName);
				try {
					facade.setActivityStart(activityInfo.getStartWeek(), activityInfo.getStartYear());
					facade.setActivityEnd(activityInfo.getEndWeek(), activityInfo.getEndYear());
				} catch (PermissionDeniedException | InvalidSpanException e1) {
					showErrorMessage(WEEKSPAN_ERROR, e1);
					error = true;
				}

				try {
					facade.setExpectedHours(activityInfo.getExpectedHours());
				} catch (PermissionDeniedException | InvalidHourException e1) {
					showErrorMessage(EXPECTED_HOUR_ERROR, e1);
					error = true;
				}
			}

			if (createNewActivity)
				prepareActivityEdit(projectId, facade.getProjectActivityInfo(activityName));

			// Unselect both activity and project
			facade.unselect();
			
			if (!error)
				showSuccessMessage(ACTIVITY_SAVED_SUCCESS);
		});

		cancelButton.addActionListener((e) -> {
			display.setActiveCard(Display.PROJECT_EDITOR_CARD_ID);
		});
	}

	void showErrorMessage(String msg, Exception e) {
		statusLabel.setForeground(GUIHelper.ERROR_COLOR);
		
		if (e != null) {
			statusLabel.setText(msg + e.getMessage());
		} else {
			statusLabel.setText(msg);
		}
	}
	
	void showSuccessMessage(String msg) {
		statusLabel.setForeground(GUIHelper.SUCCESS_COLOR);
		statusLabel.setText(msg);
	}
	
	@Override
	public void onEnter() {
	}

	@Override
	public void onExit() {
	}

	private void updateLabels() {
		if (createNewActivity) {
			setTitleText(CREATE_ACTIVITY_TITLE);
			cancelButton.setText(CANCEL_LABEL);
			saveButton.setText(CREATE_LABEL);
		} else {
			setTitleText(EDIT_ACTIVITY_TITLE);
			cancelButton.setText(BACK_LABEL);
			saveButton.setText(SAVE_LABEL);
		}
		
		statusLabel.setText("");
	}
	
	public void prepareNewActivity(String projectId) {
		createNewActivity = true;
		this.projectId = projectId;

		activityInfo.setActivityName("");
		activityInfo.setActivityNameEditable(true);
		activityInfo.invalidateActivitySpan();
		activityInfo.setExpectedHours(0);
		
		employeePanel.setEnabled(false);
		employeePanel.clear();
		
		updateLabels();
	}
	
	public void prepareActivityEdit(String projectId, ProjectActivityInfo info) {
		createNewActivity = false;
		this.projectId = projectId;
		
		activityInfo.setActivityName(info.getName());
		activityInfo.setActivityNameEditable(false);
		activityInfo.setActivityStartWeek(info.getStartYear(), info.getStartWeek());
		activityInfo.setActivityEndWeek(info.getEndYear(), info.getEndWeek());
		activityInfo.setExpectedHours(info.getExpectedHours());

		employeePanel.setEnabled(true);
		employeePanel.prepareEdit(projectId, info.getName());
	
		updateLabels();
	}
}
