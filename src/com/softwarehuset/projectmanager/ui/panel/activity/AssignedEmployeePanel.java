package com.softwarehuset.projectmanager.ui.panel.activity;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.exceptions.AssignableException;
import com.softwarehuset.projectmanager.exceptions.InvalidEmployeeException;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;

//Rasmus M. Larsen s184190
@SuppressWarnings("serial")
public class AssignedEmployeePanel extends JPanel {

	private static final String EMPLOYEES_LABEL = "Assigned to activity";
	private static final String EMPLOYEE_TO_ASSIGN_LABEL = "Employee to assign:";
	private static final String ASSIGN_BUTTON_LABEL = "Assign selected";
	
	private static final String UNABLE_TO_ASSIGN_ERROR = "Unable to assign employee: ";
	private static final String NO_SELECTION_ERROR = "No employee selected";
	
	private static final String ASSIGNED_SUCCESS = "Assigned employee successfully";
	
	private final ActivityEditorPanel editor;
	private final ProjectManagerFacade facade;
	
	private final JLabel employeesLabel;
	private final JList<String> employeeList;
	private final JScrollPane employeeScrollPane;
	
	private final JLabel assignLabel;
	private final JComboBox<String> assignSelection;
	private final JButton assignButton;
	
	private String projectId;
	private String activityName;
	
	public AssignedEmployeePanel(ActivityEditorPanel editor, ProjectManagerFacade facade) {
		this.editor = editor;
		this.facade = facade;
		
		employeesLabel = new JLabel(EMPLOYEES_LABEL);
		employeeList = new JList<String>() {
			
			@Override
			public Dimension getPreferredScrollableViewportSize() {
				return employeeList.getPreferredSize();
			}
		};
		employeeScrollPane = new JScrollPane(employeeList);
		
		assignLabel = new JLabel(EMPLOYEE_TO_ASSIGN_LABEL);
		assignSelection = new JComboBox<String>();
		assignButton = new JButton(ASSIGN_BUTTON_LABEL);
		
		uiLayout();
		uiEvents();
	}
	
	private void uiLayout() {
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.NONE;

		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.ipadx = 60;
		add(employeesLabel, constraints);
		employeesLabel.setHorizontalAlignment(JLabel.CENTER);
		constraints.ipadx = 0;
		
		constraints.gridy++;
		constraints.anchor = GridBagConstraints.WEST;
		constraints.ipadx = 60;
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.BOTH;
		add(employeeScrollPane, constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weighty = 0.0;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.ipadx = 0;
		
		JPanel assignContainer = createAssignContainer();
		
		constraints.gridx++;
		constraints.gridy = 0;
		constraints.gridheight = 2;
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.VERTICAL;
		constraints.insets = new Insets(30, 30, 30, 30);
		add(assignContainer, constraints);
		constraints.insets = new Insets(5, 5, 5, 5);
		assignContainer.setOpaque(false);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weighty = 0.0;
		constraints.gridheight = 1;
		
		constraints.gridx++;
		constraints.gridheight = 2;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.BOTH;
		add(Box.createGlue(), constraints);
	}
	
	private JPanel createAssignContainer() {
		JPanel assignContainer = new JPanel();
		assignContainer.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.anchor = GridBagConstraints.WEST;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.fill = GridBagConstraints.NONE;
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		assignContainer.add(assignLabel, constraints);
		
		constraints.gridy++;
		assignContainer.add(assignSelection, constraints);
		assignSelection.setOpaque(false);
		Dimension preferredSize = assignSelection.getPreferredSize();
		preferredSize.width = 200;
		assignSelection.setPreferredSize(preferredSize);
		assignSelection.setMaximumSize(preferredSize);

		constraints.gridy++;
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.VERTICAL;
		assignContainer.add(Box.createVerticalGlue(), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weighty = 0.0;

		constraints.gridy++;
		assignContainer.add(assignButton, constraints);
		assignButton.setOpaque(false);
		
		return assignContainer;
	}
	
	private void uiEvents() {
		assignButton.addActionListener((e) -> {
			if (assignSelection.getModel().getSize() == 0) {
				editor.showErrorMessage(NO_SELECTION_ERROR, null);
				return;
			}
			
			facade.selectProject(projectId);
			facade.selectActivity(activityName);
			
			try {
				facade.assignEmployee((String)assignSelection.getSelectedItem());
			} catch(PermissionDeniedException | AssignableException | InvalidEmployeeException e1) {
				editor.showErrorMessage(UNABLE_TO_ASSIGN_ERROR, e1);
				return;
			} finally {
				facade.unselect();
			}
			
			// Update lists
			prepareEdit(projectId, activityName);
			
			editor.showSuccessMessage(ASSIGNED_SUCCESS);
		});
	}
	
	@Override
    public void setEnabled(boolean enabled) {
    	super.setEnabled(enabled);
    	
    	employeeList.setEnabled(enabled);
    	employeesLabel.setEnabled(enabled);
    	employeeScrollPane.setEnabled(enabled);
    	
    	assignLabel.setEnabled(enabled);
    	assignSelection.setEnabled(enabled);
    	assignButton.setEnabled(enabled);
	}

	public void clear() {
		projectId = null;
		activityName = null;
		
		assignSelection.setModel(new DefaultComboBoxModel<String>());
		employeeList.setModel(new DefaultComboBoxModel<String>());
	}

	public void prepareEdit(String projectId, String activityName) {
		this.projectId = projectId;
		this.activityName = activityName;
		
		List<String> employeeIds = new ArrayList<String>(facade.getAllEmployeeIds());
		facade.selectProject(projectId);
		facade.selectActivity(activityName);
		List<String> assignedIds = facade.getAssignedEmployees();
		facade.unselect();
		
		employeeIds.removeAll(assignedIds);
		
		String[] employees = employeeIds.toArray(new String[employeeIds.size()]);
		assignSelection.setModel(new DefaultComboBoxModel<String>(employees));
	
		DefaultListModel<String> model = new DefaultListModel<String>();
		model.setSize(assignedIds.size());
		int i = 0;
		for (String id : assignedIds)
			model.set(i++, id);
		employeeList.setModel(model);
	}
}
