package com.softwarehuset.projectmanager.ui.panel.activity;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import com.softwarehuset.projectmanager.ui.Display;
import com.softwarehuset.projectmanager.ui.GUIHelper;
import com.softwarehuset.projectmanager.ui.component.HintetTextField;
import com.softwarehuset.projectmanager.ui.popup.PopupManager;
import com.softwarehuset.projectmanager.ui.popup.date.Period;
import com.softwarehuset.projectmanager.ui.popup.date.PeriodDate;

//Christian M. Fuglsang s184222
@SuppressWarnings("serial")
public class ActivityInfoForm extends JPanel {

	private static final String EMPTY_FIELD = "-";

	private static final String ACTIVITY_NAME_LABEL = "Activity name:";
	private static final String NAME_HINT = "Insert activity name...";

	private static final String START_WEEK_LABEL = "Start week:";
	private static final String END_WEEK_LABEL = "End week:";
	private static final String BROWSE_SPAN_LABEL = "Browse activity span";
	private static final String EXPECTED_HOURS_LABEL = "Expected hours:";
	
	private final Display display;
	
	private final HintetTextField nameField;

	private final JLabel startWeekLabel;
	private final JLabel endWeekLabel;
	private final JButton browseSpan;
	
	private final JSpinner expectedHoursField;

	private final List<JComponent> formComponents;
	
	private int startYear;
	private int startWeek;
	private int endYear;
	private int endWeek;
	
	public ActivityInfoForm(Display display, boolean hasExpectedHours) {
		this.display = display;
		
		nameField = new HintetTextField();
		nameField.setHint(NAME_HINT);
	
		startWeekLabel = new JLabel(EMPTY_FIELD);
		endWeekLabel = new JLabel(EMPTY_FIELD);
		browseSpan = new JButton(BROWSE_SPAN_LABEL);
	
		formComponents = new ArrayList<JComponent>();
		
		if (hasExpectedHours) {
			expectedHoursField = new JSpinner(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
			expectedHoursField.setEditor(new JSpinner.NumberEditor(expectedHoursField, "#"));
		} else {
			expectedHoursField = null;
		}
		
		uiLayout();
		uiEvents();
	}
	
	private void uiLayout() {
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.NONE;

		constraints.gridy = 0;
		addFieldRow(constraints, new JLabel(ACTIVITY_NAME_LABEL), nameField);
		constraints.gridy++;
		addFieldRow(constraints, new JLabel(START_WEEK_LABEL), startWeekLabel);
		constraints.gridy++;
		addFieldRow(constraints, new JLabel(END_WEEK_LABEL), endWeekLabel);
		
		JPanel browsePanel = new JPanel();
		formComponents.add(browsePanel);
		browsePanel.setLayout(new BoxLayout(browsePanel, BoxLayout.X_AXIS));
		formComponents.add(browseSpan);
		browsePanel.add(browseSpan);
		browseSpan.setOpaque(false);
		browsePanel.add(Box.createHorizontalGlue());
		
		constraints.gridx = 0;
		constraints.gridy++;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.gridwidth = 2;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(browsePanel, constraints);
		browsePanel.setOpaque(false);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.gridwidth = 1;

		if (expectedHoursField != null) {
			constraints.gridy++;
			addFieldRow(constraints, new JLabel(EXPECTED_HOURS_LABEL), expectedHoursField);
		}
	}
	
	private void addFieldRow(GridBagConstraints constraints, JLabel label, JComponent field) {
		constraints.gridx = 0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(label, constraints);
		constraints.fill = GridBagConstraints.NONE;
		
		constraints.gridx++;
		
		constraints.anchor = GridBagConstraints.WEST;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.VERTICAL;
		add(field, constraints);
		field.setPreferredSize(new Dimension(200, field.getPreferredSize().height));
		field.setOpaque(false);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.anchor = GridBagConstraints.CENTER;

		formComponents.add(label);
		formComponents.add(field);
	}
	
	private void uiEvents() {
		browseSpan.addActionListener((e) -> {
			Period span = PopupManager.popupCalendar(display);
			if (span != null) {
				PeriodDate startDate = span.getStartDate();
				PeriodDate endDate = span.getEndDate();
				setActivityStartWeek(startDate.getYear(), startDate.getWeekOfYear());
				setActivityEndWeek(endDate.getYear(), endDate.getWeekOfYear());
			}
		});
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		
		for (JComponent comp : formComponents)
			comp.setEnabled(enabled);
	}
	
	public void setActivityNameEditable(boolean state) {
		nameField.setEditable(state);
	}
	
	public void setActivityName(String projectName) {
		nameField.setText(projectName);
	}
	
	public void setActivityStartWeek(int year, int week) {
		startWeekLabel.setText(GUIHelper.formatActivitySpan(year, week));
		startYear = year;
		startWeek = week;
	}

	public void setActivityEndWeek(int year, int week) {
		endWeekLabel.setText(GUIHelper.formatActivitySpan(year, week));
		endYear = year;
		endWeek = week;
	}
	
	public int getStartYear() {
		return startYear;
	}

	public int getStartWeek() {
		return startWeek;
	}

	public int getEndYear() {
		return endYear;
	}

	public int getEndWeek() {
		return endWeek;
	}
	
	public String getActivityName() {
		return nameField.getText();
	}
	
	public boolean isValidActivitySpan() {
		return startYear != -1 && startWeek != -1 &&
		       endYear != -1 && endWeek != -1;
	}

	public void invalidateActivitySpan() {
		startYear = startWeek = -1;
		endYear = endWeek = -1;
	
		startWeekLabel.setText(EMPTY_FIELD);
		endWeekLabel.setText(EMPTY_FIELD);
	}
	
	public void setExpectedHours(int hours) {
		if (expectedHoursField != null)
			expectedHoursField.setValue(Integer.valueOf(hours));
	}

	public int getExpectedHours() {
		if (expectedHoursField == null)
			throw new IllegalStateException("Expected hours is disabled");
			
		return ((Integer)expectedHoursField.getValue()).intValue();
	}
	
	public void clear() {
		setActivityName("");
		invalidateActivitySpan();
		setExpectedHours(0);
	}
}
