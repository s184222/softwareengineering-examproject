package com.softwarehuset.projectmanager.ui.panel.dashboard;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.text.InternationalFormatter;

import com.softwarehuset.projectmanager.app.facade.ProjectActivityInfo;
import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.exceptions.InvalidEmployeeException;
import com.softwarehuset.projectmanager.exceptions.InvalidHourException;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.projectmanager.ui.GUIHelper;
import com.softwarehuset.projectmanager.ui.component.TitleLabel;

//Christian M. Fuglsang s184222
@SuppressWarnings("serial")
public class MyActivitiesTab extends DashboardTab {

	private static final String SELECT_TITLE = "Select Project and Activity";
	private static final String REGISTER_REQUEST_TITLE = "Register and Request";

	private static final String SELECTED_PROJECT_BORDER_TITLE = "Selected project information";
	private static final String SELECTED_ACTIVITY_BORDER_TITLE = "Selected activity information";
	private static final String REGISTER_BORDER_TITLE = "Register work-hours";
	private static final String REQUEST_BORDER_TITLE = "Request help from employee";

	private static final String SELECT_PROJECT_LABEL = "Select project:";
	private static final String SELECT_ACTIVITY_LABEL = "Select activity:";
	
	private static final String DATE_INPUT_FORMAT = "dd-MM-yyyy";
	private static final String DATE_LABEL = "Specify date (" + DATE_INPUT_FORMAT.toUpperCase() + "):";
	private static final String HOURS_LABEL = "Specify hours (0-24):";
	private static final String REGISTER_HOURS_LABEL = "Register hours";
	
	private static final String SELECT_EMPLOYEE_LABEL = "Select employee:";
	private static final String REQUEST_HELP_LABEL = "Request help";
	
	private static final String EMPTY_FIELD_ERROR = "One or more empty fields";
	private static final String REGISTER_ERROR = "Unable to register: ";
	private static final String NO_SELECTED_EMPLOYEE_ERROR = "No selected employee";
	private static final String REQUEST_ERROR = "Unable to request: ";
	
	private static final String REGISTER_SUCCESS = "Registered hours successfully";
	private static final String REQUEST_SUCCESS = "Requested help successfully";
	
	private static final DateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_INPUT_FORMAT);
	
	private final JComboBox<String> projectIdSelection;
	private final JComboBox<String> activityNameSelection;
	
	private final JFormattedTextField dateField;
	private final JFormattedTextField hoursField;
	private final JLabel registerStatus;
	private final JButton registerHours;
	
	private final JComboBox<String> employeeSelection;
	private final JLabel requestStatus;
	private final JButton requestHelp;
	
	private final ProjectInfoPanel projectInfoPanel;
	private final ActivityInfoPanel activityInfoPanel;
	private final WorkHoursListPanel workHoursListPanel;
	
	private final List<JComponent> registerComponents;
	
	private String projectId;
	private String activityName;
	
	public MyActivitiesTab(ProjectManagerFacade facade, DashboardPanel dashboard) {
		super(facade, dashboard);
		
		projectIdSelection = new JComboBox<String>();
		activityNameSelection = new JComboBox<String>();
		
		dateField = new JFormattedTextField(DATE_FORMATTER);
		
		NumberFormat format = DecimalFormat.getInstance();
		format.setMinimumFractionDigits(2);
		format.setMaximumFractionDigits(2);
		format.setRoundingMode(RoundingMode.HALF_UP);
		InternationalFormatter formatter = new InternationalFormatter(format);
		formatter.setMinimum(0.0);
		formatter.setMaximum(24.0);
		formatter.setValueClass(Double.class);
		hoursField = new JFormattedTextField(formatter);
		
		registerStatus = new JLabel();
		registerHours = new JButton(REGISTER_HOURS_LABEL);
		
		employeeSelection = new JComboBox<String>();
		
		projectInfoPanel = new ProjectInfoPanel();
		projectInfoPanel.setBorder(BorderFactory.createTitledBorder(SELECTED_PROJECT_BORDER_TITLE));
		activityInfoPanel = new ActivityInfoPanel();
		activityInfoPanel.setBorder(BorderFactory.createTitledBorder(SELECTED_ACTIVITY_BORDER_TITLE));
		workHoursListPanel = new WorkHoursListPanel();
		workHoursListPanel.setRowSelectionAllowed(false);
		
		registerComponents = new ArrayList<JComponent>();
		requestStatus = new JLabel();
		requestHelp = new JButton(REQUEST_HELP_LABEL);
		
		setOpaque(false);
		
		uiLayout();
		uiEvents();
	}
	
	private void uiLayout() {
		setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.fill = GridBagConstraints.NONE;
	
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 2;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		JLabel projectsTitle = new TitleLabel(SELECT_TITLE, true);
		projectsTitle.setHorizontalAlignment(JLabel.CENTER);
		add(projectsTitle, constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.gridwidth = 1;

		constraints.gridy++;
		constraints.gridwidth = 2;
		constraints.anchor = GridBagConstraints.SOUTH;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(new JSeparator(JSeparator.HORIZONTAL), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.gridwidth = 1;
		
		constraints.gridy = 0;
		constraints.gridx = 2;
		constraints.gridheight = 2;
		constraints.fill = GridBagConstraints.VERTICAL;
		add(new JSeparator(JSeparator.VERTICAL), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.gridheight = 1;

		constraints.gridy = 0;
		constraints.gridx++;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		JLabel registerRequestTitle = new TitleLabel(REGISTER_REQUEST_TITLE, true);
		registerRequestTitle.setHorizontalAlignment(JLabel.CENTER);
		add(registerRequestTitle, constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;

		constraints.gridy++;
		constraints.anchor = GridBagConstraints.SOUTH;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(new JSeparator(JSeparator.HORIZONTAL), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.anchor = GridBagConstraints.CENTER;

		// Insets for selectors (fixed later)
		constraints.insets.top = 30;
		constraints.insets.bottom = 15;
		
		constraints.gridx = 0;
		constraints.gridy++;
		constraints.anchor = GridBagConstraints.WEST;
		add(new JLabel(SELECT_PROJECT_LABEL), constraints);
		constraints.anchor = GridBagConstraints.CENTER;

		constraints.gridx++;
		constraints.insets.left = 20;
		constraints.anchor = GridBagConstraints.WEST;
		add(projectIdSelection, constraints);
		projectIdSelection.setOpaque(false);
		setPreferredWidth(projectIdSelection, 200);
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.insets.left = 5;

		// Only 30 on first row
		constraints.insets.top = 15;

		constraints.gridx = 0;
		constraints.gridy++;
		constraints.anchor = GridBagConstraints.WEST;
		add(new JLabel(SELECT_ACTIVITY_LABEL), constraints);
		constraints.anchor = GridBagConstraints.CENTER;
		
		constraints.gridx++;
		constraints.insets.left = 20;
		constraints.anchor = GridBagConstraints.WEST;
		add(activityNameSelection, constraints);
		activityNameSelection.setOpaque(false);
		setPreferredWidth(activityNameSelection, 200);
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.insets.left = 5;

		// Fixed. Told ya.
		constraints.insets.top = 5;
		constraints.insets.bottom = 5;
		
		constraints.gridx = 0;
		constraints.gridy = 4;
		constraints.insets.top = 20;
		constraints.gridwidth = 2;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(projectInfoPanel, constraints);
		projectInfoPanel.setOpaque(false);
		constraints.fill = GridBagConstraints.NONE;
		constraints.gridwidth = 1;
		constraints.insets.top = 5;

		constraints.gridy++;
		constraints.insets.top = 20;
		constraints.gridwidth = 2;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(activityInfoPanel, constraints);
		activityInfoPanel.setOpaque(false);
		constraints.fill = GridBagConstraints.NONE;
		constraints.gridwidth = 1;
		constraints.insets.top = 5;

		// Add filler bottom
		constraints.gridy++;
		constraints.gridwidth = 2;
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.BOTH;
		add(Box.createGlue(), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weighty = 0.0;
		constraints.gridwidth = 1;

		JPanel registerRequestPanel = createRegisterRequestPanel();
		
		constraints.gridx = 3;
		constraints.gridy = 2; // below right title
		constraints.gridheight = 5;
		constraints.insets.top = 20;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(registerRequestPanel, constraints);
		registerRequestPanel.setOpaque(false);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.insets.top = 5;
		constraints.gridheight = 1;
	}
	
	private JPanel createRegisterRequestPanel() {
		JPanel registerWorkPanel = new JPanel();
		registerComponents.add(registerWorkPanel);
		
		registerWorkPanel.setLayout(new BoxLayout(registerWorkPanel, BoxLayout.Y_AXIS));
		registerWorkPanel.setBorder(BorderFactory.createTitledBorder(REGISTER_BORDER_TITLE));
		
		registerComponents.add(workHoursListPanel);
		registerWorkPanel.add(workHoursListPanel);
		registerWorkPanel.add(Box.createVerticalStrut(10));
		
		JPanel formPanel = new JPanel();
		registerComponents.add(formPanel);
		
		formPanel.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.fill = GridBagConstraints.NONE;
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.anchor = GridBagConstraints.WEST;
		JLabel dateLabel = new JLabel(DATE_LABEL);
		registerComponents.add(dateLabel);
		formPanel.add(dateLabel, constraints);
		constraints.anchor = GridBagConstraints.CENTER;
		
		constraints.gridx++;
		constraints.insets.left = 20;
		constraints.anchor = GridBagConstraints.WEST;
		constraints.weightx = 1.0;
		registerComponents.add(dateField);
		formPanel.add(dateField, constraints);
		setPreferredWidth(dateField, 200);
		dateField.setOpaque(false);
		constraints.weightx = 0.0;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.insets.left = 5;

		constraints.gridx = 0;
		constraints.gridy++;
		constraints.anchor = GridBagConstraints.WEST;
		JLabel hoursLabel = new JLabel(HOURS_LABEL);
		registerComponents.add(hoursLabel);
		formPanel.add(hoursLabel, constraints);
		constraints.anchor = GridBagConstraints.CENTER;
		
		constraints.gridx++;
		constraints.insets.left = 20;
		constraints.anchor = GridBagConstraints.WEST;
		constraints.weightx = 1.0;
		registerComponents.add(hoursField);
		formPanel.add(hoursField, constraints);
		setPreferredWidth(hoursField, 200);
		hoursField.setOpaque(false);
		constraints.weightx = 0.0;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.insets.left = 5;

		constraints.gridx = 0;
		constraints.gridy++;
		constraints.gridwidth = 2;
		constraints.anchor = GridBagConstraints.WEST;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		registerComponents.add(registerStatus);
		formPanel.add(registerStatus, constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.gridwidth = 1;
		
		constraints.gridy++;
		constraints.gridwidth = 2;
		constraints.anchor = GridBagConstraints.WEST;
		registerComponents.add(registerHours);
		formPanel.add(registerHours, constraints);
		registerHours.setOpaque(false);
		
		registerWorkPanel.add(formPanel);
		formPanel.setOpaque(false);
		
		JPanel requestPanel = createRequestHelpPanel();
		
		JPanel container = new JPanel();
		registerComponents.add(container);
		container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
		
		container.add(registerWorkPanel);
		registerWorkPanel.setOpaque(false);
		container.add(Box.createVerticalStrut(20));
		container.add(requestPanel);
		requestPanel.setOpaque(false);
		
		return container;
	}

	private JPanel createRequestHelpPanel() {
		JPanel requestPanel = new JPanel();
		registerComponents.add(requestPanel);
		requestPanel.setBorder(BorderFactory.createTitledBorder(REQUEST_BORDER_TITLE));
		
		requestPanel.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.fill = GridBagConstraints.NONE;
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.anchor = GridBagConstraints.WEST;
		JLabel requestLabel = new JLabel(SELECT_EMPLOYEE_LABEL);
		registerComponents.add(requestLabel);
		requestPanel.add(requestLabel, constraints);
		constraints.anchor = GridBagConstraints.CENTER;
		
		constraints.gridx++;
		constraints.insets.left = 20;
		constraints.anchor = GridBagConstraints.WEST;
		constraints.weightx = 1.0;
		registerComponents.add(employeeSelection);
		requestPanel.add(employeeSelection, constraints);
		setPreferredWidth(employeeSelection, 200);
		employeeSelection.setOpaque(false);
		constraints.weightx = 0.0;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.insets.left = 5;
		
		constraints.gridx = 0;
		constraints.gridy++;
		constraints.gridwidth = 2;
		constraints.anchor = GridBagConstraints.WEST;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		registerComponents.add(requestStatus);
		requestPanel.add(requestStatus, constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.gridwidth = 1;
		
		constraints.gridy++;
		constraints.gridwidth = 2;
		constraints.anchor = GridBagConstraints.WEST;
		registerComponents.add(requestHelp);
		requestPanel.add(requestHelp, constraints);
		requestHelp.setOpaque(false);
		
		return requestPanel;
	}

	private void setPreferredWidth(JComponent comp, int prefWidth) {
		Dimension preferred = comp.getPreferredSize();
		preferred.width = prefWidth;
		comp.setPreferredSize(preferred);
	}
	
	private void uiEvents() {
		projectIdSelection.addActionListener((e) -> {
			String projectId = (String)projectIdSelection.getSelectedItem();
			if (projectId.isEmpty()) {
				this.projectId = null;
				facade.unselect();
			} else {
				this.projectId = projectId;
				facade.selectProject(projectId);
			}
			
			updateActivitySelection();
			updateProjectInfo();
			
			// We may have unselected the project
			updateActivityInfo();
		});
		
		activityNameSelection.addActionListener((e) -> {
			String activityName = (String)activityNameSelection.getSelectedItem();
			if (activityName.isEmpty()) {
				this.activityName = null;
				facade.unselect();
				
				// Make sure the project is still selected
				if (projectId != null)
					facade.selectProject(projectId);
			} else {
				this.activityName = activityName;
				facade.selectActivity(activityName);
			}

			updateActivityInfo();
		});
		
		registerHours.addActionListener((e) -> {
			Date date = (Date)dateField.getValue();
			Double hours = (Double)hoursField.getValue();
			
			if (date == null || hours == null) {
				showRegisterError(EMPTY_FIELD_ERROR, null);
				return;
			}
			
			try {
				facade.registerWork(hours.doubleValue(), DATE_FORMATTER.format(date));
				workHoursListPanel.updateCells(facade);
			} catch (PermissionDeniedException | InvalidHourException e1) {
				showRegisterError(REGISTER_ERROR, e1);
				return;
			}
			
			showRegisterSuccess(REGISTER_SUCCESS);
		});
		
		requestHelp.addActionListener((e) -> {
			String employeeID = (String)employeeSelection.getSelectedItem();
			if (employeeID == null) {
				showRequestError(NO_SELECTED_EMPLOYEE_ERROR, null);
				return;
			}
			
			try {
				facade.requestHelp(employeeID);
			} catch (InvalidEmployeeException | PermissionDeniedException | InvalidHourException e1) {
				showRequestError(REQUEST_ERROR, e1);
				return;
			}

			showRequestSuccess(REQUEST_SUCCESS);
		});
	}

	private void showRegisterError(String msg, Exception e) {
		showError(registerStatus, msg, e);
	}

	private void showRequestError(String msg, Exception e) {
		showError(requestStatus, msg, e);
	}

	private void showRegisterSuccess(String msg) {
		showSuccess(registerStatus, msg);
	}
	
	private void showRequestSuccess(String msg) {
		showSuccess(requestStatus, msg);
	}
	
	private void showError(JLabel label, String msg, Exception e) {
		label.setForeground(GUIHelper.ERROR_COLOR);
		
		if (e != null) {
			label.setText(msg + e.getMessage());
		} else {
			label.setText(msg);
		}
	}

	private void showSuccess(JLabel label, String msg) {
		label.setForeground(GUIHelper.SUCCESS_COLOR);
		label.setText(msg);
	}
	
	private void updateActivityInfo() {
		if (projectId == null || activityName == null) {
			activityInfoPanel.clear();
		
			workHoursListPanel.clearCells();
			clearErrors();
			setWorkHoursEnabled(false);
			return;
		}
		
		activityInfoPanel.setActivityInfo(facade.getProjectActivityInfo(activityName));
		workHoursListPanel.updateCells(facade);
		setWorkHoursEnabled(true);
	}
	
	private void setWorkHoursEnabled(boolean enabled) {
		for (JComponent comp : registerComponents)
			comp.setEnabled(enabled);
	}
	
	private void updateProjectInfo() {
		if (projectId == null) {
			projectInfoPanel.clear();
			return;
		}
		
		projectInfoPanel.setProjectInfo(facade.getProjectInfoFromId(projectId));
	}
	
	private void updateActivitySelection() {
		activityName = null;

		if (projectId == null) {
			activityNameSelection.setModel(new DefaultComboBoxModel<String>());
			return;
		}
		
		List<ProjectActivityInfo> activityInfos = facade.getAllProjectActivityInfos(projectId);
		String[] activityNames = new String[activityInfos.size() + 1];
		int i = 0;
		activityNames[i++] = "";
		for (ProjectActivityInfo info : activityInfos)
			activityNames[i++] = info.getName();
	
		activityNameSelection.setModel(new DefaultComboBoxModel<String>(activityNames));
		activityNameSelection.setSelectedIndex(0);
	}
	
	private void clearErrors() {
		registerStatus.setText("");
		requestStatus.setText("");
	}
	
	@Override
	public void onEnter() {
		List<String> projectIds = facade.getAllProjectIds();
		projectIdSelection.setModel(new DefaultComboBoxModel<String>(createStringArray(projectIds, true)));
		projectIdSelection.setSelectedIndex(0);

		projectId = null;
		activityName = null;
		facade.unselect();
		updateActivitySelection();
		updateProjectInfo();
		updateActivityInfo();
		
		dateField.setValue(new Date());
		hoursField.setValue(Double.valueOf(1.0));
		clearErrors();
		
		Collection<String> avail = facade.getAvailableEmployeeIdsToday();
		employeeSelection.setModel(new DefaultComboBoxModel<String>(avail.toArray(new String[avail.size()])));
	}
	
	private String[] createStringArray(List<String> ids, boolean leadingEmpty) {
		int length = ids.size();
		if (leadingEmpty)
			length++;
		
		String[] stringArray = new String[length];

		int i = 0;
		if (leadingEmpty)
			stringArray[i++] = "";
		for (String id : ids)
			stringArray[i++] = id;
		
		return stringArray;
	}

	@Override
	public void onExit() {
		facade.unselect();
	}
}
