package com.softwarehuset.projectmanager.ui.panel.dashboard;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.softwarehuset.projectmanager.app.facade.ProjectActivityInfo;
import com.softwarehuset.projectmanager.ui.GUIHelper;

//Tobias H. M�ller s184217
@SuppressWarnings("serial")
public class ActivityInfoPanel extends JPanel {

	private static final String ACTIVITY_NAME_LABEL = "Activity name:";
	private static final String START_WEEK_LABEL = "Start week:";
	private static final String END_WEEK_LABEL = "End week:";
	private static final String EXPECTED_HOURS_LABEL = "Expected hours:";

	private static final String EMPTY_FIELD = "-";
	
	private final JLabel nameField;
	private final JLabel startWeekField;
	private final JLabel endWeekField;
	private final JLabel expectedHoursField;
	
	public ActivityInfoPanel() {
		this.nameField = new JLabel();
		this.startWeekField = new JLabel();
		this.endWeekField = new JLabel();
		this.expectedHoursField = new JLabel();
		
		uiLayout();
	}
	
	private void uiLayout() {
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.NONE;

		constraints.gridy = 0;
		addFieldRow(constraints, new JLabel(ACTIVITY_NAME_LABEL), nameField);

		constraints.gridy++;
		addFieldRow(constraints, new JLabel(START_WEEK_LABEL), startWeekField);

		constraints.gridy++;
		addFieldRow(constraints, new JLabel(END_WEEK_LABEL), endWeekField);
		
		constraints.gridy++;
		addFieldRow(constraints, new JLabel(EXPECTED_HOURS_LABEL), expectedHoursField);
	}
	
	private void addFieldRow(GridBagConstraints constraints, JLabel label, JComponent field) {
		constraints.gridx = 0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(label, constraints);
		constraints.fill = GridBagConstraints.NONE;
		
		constraints.gridx++;
		
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(field, constraints);
		field.setPreferredSize(new Dimension(200, 30));
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
	}
	
	public void setActivityName(String activityName) {
		nameField.setText(activityName);
	}
	
	public String getActivityName() {
		return nameField.getText();
	}

	public void setStartWeek(String startWeek) {
		startWeekField.setText(startWeek);
	}

	public String getStartWeek() {
		return startWeekField.getText();
	}

	public void setEndWeek(String endWeek) {
		endWeekField.setText(endWeek);
	}
	
	public String getEndWeek() {
		return endWeekField.getText();
	}
	
	public void setExpectedHours(String expectedHours) {
		expectedHoursField.setText(expectedHours);
	}
	
	public String getExpectedHours() {
		return expectedHoursField.getText();
	}

	public void setActivityInfo(ProjectActivityInfo info) {
		setActivityName(info.getName());
		setStartWeek(GUIHelper.formatActivitySpan(info.getStartYear(), info.getStartWeek()));
		setEndWeek(GUIHelper.formatActivitySpan(info.getEndYear(), info.getEndWeek()));
		setExpectedHours(GUIHelper.formatExpectedHours(info.getExpectedHours()));
	}
	
	public void clear() {
		setActivityName(EMPTY_FIELD);
		setStartWeek(EMPTY_FIELD);
		setEndWeek(EMPTY_FIELD);
		setExpectedHours(EMPTY_FIELD);
	}
}
