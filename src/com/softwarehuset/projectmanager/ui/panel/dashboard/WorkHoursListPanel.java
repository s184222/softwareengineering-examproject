package com.softwarehuset.projectmanager.ui.panel.dashboard;

import java.util.Collection;

import javax.swing.table.DefaultTableModel;

import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.app.facade.WorkInfo;
import com.softwarehuset.projectmanager.ui.GUIHelper;
import com.softwarehuset.projectmanager.ui.component.TableListPanel;

//Christian M. Fuglsang s184222
@SuppressWarnings("serial")
public class WorkHoursListPanel extends TableListPanel {

	private static final int EMPLOYEE_ID_COLUMN = 0;
	private static final int DATE_COLUMN = 1;
	private static final int NUM_OF_HOURS_COLUMN = 2;
	
	private static final String[] WORK_COLUMNS = new String[] {
		"Employee ID",
		"Date",
		"Number of hours"
	};
	
	public WorkHoursListPanel() {
		super(WORK_COLUMNS);
	}
	
	public void updateCells(ProjectManagerFacade facade) {
		Collection<WorkInfo> workInfos = facade.getWorkInfos();
		
		DefaultTableModel tableModel = getModel();
		
		int r = 0;
		tableModel.setRowCount(workInfos.size());
		for (WorkInfo info : workInfos) {
			tableModel.setValueAt(info.getEmployeeId(), r, EMPLOYEE_ID_COLUMN);
			tableModel.setValueAt(info.getDate(), r, DATE_COLUMN);
			tableModel.setValueAt(GUIHelper.formatWorkHours(info.getHours()), r, NUM_OF_HOURS_COLUMN);

			r++;
		}
		
		if (tableModel.getRowCount() != 0)
			getTable().getSelectionModel().setSelectionInterval(0, 0);
	}
}
