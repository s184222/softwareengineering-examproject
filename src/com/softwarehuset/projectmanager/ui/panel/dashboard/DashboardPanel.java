package com.softwarehuset.projectmanager.ui.panel.dashboard;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;

import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.ui.Display;
import com.softwarehuset.projectmanager.ui.component.TitleLabel;
import com.softwarehuset.projectmanager.ui.panel.ManagerPanel;

//Christian M. Fuglsang s184222
@SuppressWarnings("serial")
public class DashboardPanel extends ManagerPanel {

	private static final String DASHBOARD_TITLE = "Dashboard";
	private static final String LOGGED_IN_AS = "Logged in as:";
	
	private static final String GENERAL_TAB_TITLE = "General";
	private static final String PROJECT_LIST_TAB_TITLE = "Projects";
	private static final String MY_ACTIVITIES_TAB_TITLE = "Activities";
	private static final String JOURNAL_TAB_TITLE = "Journal";
	
	private static final int[] TAB_MNEMONICS = {
		KeyEvent.VK_1, KeyEvent.VK_2, KeyEvent.VK_3,
		KeyEvent.VK_4, KeyEvent.VK_5, KeyEvent.VK_6,
		KeyEvent.VK_7, KeyEvent.VK_8, KeyEvent.VK_9,
		KeyEvent.VK_0
	};
	
	private final JLabel loggedInAs;
	private final JTabbedPane tabbedContent;
	
	private final GeneralTab generalTab;
	private final ProjectListTab projectListTab;
	private final MyActivitiesTab myActivitiesTab;
	private final JournalTab journalTab;
	
	private int currentSelectedIndex;
	
	public DashboardPanel(ProjectManagerFacade facade, Display display) {
		super(facade, display);
		
		loggedInAs = new JLabel("No user");
		loggedInAs.setFont(loggedInAs.getFont().deriveFont(Font.BOLD));
		tabbedContent = new JTabbedPane();
		
		generalTab = new GeneralTab(facade, this);
		projectListTab = new ProjectListTab(facade, this);
		myActivitiesTab = new MyActivitiesTab(facade, this);
		journalTab = new JournalTab(facade, this);
		
		uiLayout();
		uiEvents();
	}
	
	private void uiLayout() {
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		// Constraint defaults. When changed, change back.
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.NONE;
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.anchor = GridBagConstraints.WEST;
		add(new TitleLabel(DASHBOARD_TITLE), constraints);

		// Add filler
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(Box.createHorizontalGlue(), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		
		constraints.gridx++;
		constraints.anchor = GridBagConstraints.EAST;
		add(new JLabel(LOGGED_IN_AS), constraints);
		
		constraints.gridx++;
		constraints.anchor = GridBagConstraints.EAST;
		add(loggedInAs, constraints);
		
		constraints.gridx = 0;
		constraints.gridy++;
		constraints.anchor = GridBagConstraints.SOUTH;
		constraints.gridwidth = 3;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.BOTH;
		add(tabbedContent, constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weighty = 0.0;
		constraints.weightx = 0.0;
		constraints.gridwidth = 1;
	
		setupTabs();
	}
	
	private void setupTabs() {
		addTab(GENERAL_TAB_TITLE, generalTab);
		addTab(PROJECT_LIST_TAB_TITLE, projectListTab);
		addTab(MY_ACTIVITIES_TAB_TITLE, myActivitiesTab);
		addTab(JOURNAL_TAB_TITLE, journalTab);
	}
	
	private void addTab(String title, DashboardTab tabPanel) {
		tabbedContent.addTab(title, tabPanel);
		
		int index = tabbedContent.getTabCount() - 1;
		tabbedContent.setMnemonicAt(index, getCurrentTabMnemonic());
	}

	public int getCurrentTabMnemonic() {
		int tabCount = tabbedContent.getTabCount();
		if (tabCount > 0 && tabCount <= TAB_MNEMONICS.length)
			return TAB_MNEMONICS[tabCount - 1];
		return -1;
	}

	private void uiEvents() {
		tabbedContent.addChangeListener((e) -> {
			if (currentSelectedIndex >= 0 && currentSelectedIndex < tabbedContent.getTabCount())
				((DashboardTab)tabbedContent.getComponentAt(currentSelectedIndex)).onExit();
				
			currentSelectedIndex = tabbedContent.getSelectedIndex();
			
			((DashboardTab)tabbedContent.getSelectedComponent()).onEnter();
		});
	}
	
	/*
	public boolean closeTab(DashboardTab tabPanel) {
		if (tabPanel.shouldClose()) {
			int index = getIndex(tabPanel);
			if (index != -1)
				tabbedContent.removeTabAt(index);
			return true;
		}
		return false;
	}*/
	
	public void setSelectedTab(DashboardTab tabPanel) {
		int index = getIndex(tabPanel);
		if (index != -1)
			tabbedContent.setSelectedIndex(index);
	}
	
	public int getIndex(DashboardTab tabPanel) {
		int i = tabbedContent.getTabCount() - 1;
		while (i >= 0) {
			if (tabbedContent.getComponentAt(i).equals(tabPanel))
				return i;
			i--;
		}
		// If it doesn't exist this
		// returns -1.
		return i;
	}
	
	public void setTabEnabled(DashboardTab tabPanel, boolean enabled) {
		int index = getIndex(tabPanel);
		if (index != -1)
			tabbedContent.setEnabledAt(index, enabled);
	}
	
	@Override
	public void onEnter() {
		String userId = facade.getUserId();
		if (userId == null) {
			// We've not selected any user..
			display.setActiveCard(Display.SIGN_IN_CARD_ID);
			return;
		}
		
		loggedInAs.setText(userId);
		
		currentSelectedIndex = tabbedContent.getSelectedIndex();
		((DashboardTab)tabbedContent.getSelectedComponent()).onEnter();
		
		setTabEnabled(journalTab, !facade.isAdmin());
	}

	@Override
	public void onExit() {
		((DashboardTab)tabbedContent.getSelectedComponent()).onExit();
	}

	public Display getDisplay() {
		return display;
	}
}
