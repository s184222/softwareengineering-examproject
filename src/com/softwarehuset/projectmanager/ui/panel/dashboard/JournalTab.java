package com.softwarehuset.projectmanager.ui.panel.dashboard;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.TitledBorder;

import com.softwarehuset.projectmanager.app.facade.PersonalActivityInfo;
import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.exceptions.InvalidSpanException;
import com.softwarehuset.projectmanager.exceptions.NameUnavailableException;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.projectmanager.ui.GUIHelper;
import com.softwarehuset.projectmanager.ui.component.TitleLabel;
import com.softwarehuset.projectmanager.ui.panel.activity.ActivityInfoForm;

//Christian M. Fuglsang s184222
@SuppressWarnings("serial")
public class JournalTab extends DashboardTab {

	private static final String JOURNAL_TITLE = "My Journal";
	
	private static final String ACTIVITY_LIST_BORDER_TITLE = "Personal activities";
	
	private static final String ACTIVITY_EDIT_FORM_BORDER_TITLE = "Edit personal activity";
	private static final String ACTIVITY_ADD_FORM_BORDER_TITLE = "Add personal activity";
	
	private static final String ADD_ACTIVITY_LABEL = "Add activity";
	private static final String EDIT_ACTIVITY_LABEL = "Edit activity";
	private static final String DONE_LABEL = "Done";
	private static final String CANCEL_LABEL = "Cancel";
	
	private static final String NO_SELECTION_ERROR = "No selection";
	private static final String ALREADY_EDITING_ERROR = "Already editing activity";
	
	private static final String EMPTY_ACTIVITY_NAME_ERROR = "Activity name field is empty";
	private static final String INVALID_ACTIVITY_SPAN_ERROR = "Invalid or empty activity span";
	private static final String CREATE_ERROR = "Unable to create activity: ";
	private static final String WEEKSPAN_ERROR = "Unable to edit weekspan: ";
	
	private static final String ACTIVITY_SAVED_SUCCESS = "Activity saved successfully";
	
	private final JButton addActivity;
	private final JButton editActivity;
	private final JLabel activityListStatus;
	
	private final JButton done;
	private final JButton cancel;
	private final TitledBorder formBorder;
	private final JLabel formStatus;
	
	private final JournalActivityListPanel activityListPanel;
	private final ActivityInfoForm activityForm;
	
	private final List<JComponent> formComponents;
	
	private boolean createNewActivity;
	private boolean editing;
	
	public JournalTab(ProjectManagerFacade facade, DashboardPanel dashboard) {
		super(facade, dashboard);
	
		setOpaque(false);
		
		activityListStatus = new JLabel();
		formStatus = new JLabel();
		
		addActivity = new JButton(ADD_ACTIVITY_LABEL);
		editActivity = new JButton(EDIT_ACTIVITY_LABEL);
		
		done = new JButton(DONE_LABEL);
		cancel = new JButton(CANCEL_LABEL);
		formBorder = BorderFactory.createTitledBorder(ACTIVITY_ADD_FORM_BORDER_TITLE);
		
		activityListPanel = new JournalActivityListPanel();
		activityForm = new ActivityInfoForm(dashboard.getDisplay(), false);
		
		formComponents = new ArrayList<JComponent>();
		
		uiLayout();
		uiEvents();
	}
	
	private void uiLayout() {
		setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.fill = GridBagConstraints.NONE;
	
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		JLabel projectsTitle = new TitleLabel(JOURNAL_TITLE, true);
		projectsTitle.setHorizontalAlignment(JLabel.CENTER);
		add(projectsTitle, constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
	
		constraints.gridy++;
		constraints.anchor = GridBagConstraints.SOUTH;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(new JSeparator(JSeparator.HORIZONTAL), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.anchor = GridBagConstraints.CENTER;
	
		JPanel activityListContainer = createActivityListContainer();
		activityListContainer.setBorder(BorderFactory.createTitledBorder(ACTIVITY_LIST_BORDER_TITLE));
		
		constraints.gridy++;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(activityListContainer, constraints);
		activityListContainer.setOpaque(false);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;

		JPanel activityFormContainer = createActivityFormContainer();
		activityFormContainer.setBorder(formBorder);
		
		constraints.gridy++;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(activityFormContainer, constraints);
		activityFormContainer.setOpaque(false);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		
		constraints.gridy++;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.BOTH;
		add(Box.createGlue(), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weighty = 0.0;
		constraints.weightx = 0.0;
	}

	private JPanel createActivityListContainer() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

		buttonPanel.add(activityListStatus);
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(addActivity);
		addActivity.setOpaque(false);
		buttonPanel.add(Box.createHorizontalStrut(5));
		buttonPanel.add(editActivity);
		editActivity.setOpaque(false);
		
		JPanel activityContainer = new JPanel();
		activityContainer.setLayout(new BoxLayout(activityContainer, BoxLayout.Y_AXIS));
		
		activityContainer.add(activityListPanel);
		activityListPanel.setOpaque(false);
		activityContainer.add(Box.createVerticalStrut(10));
		activityContainer.add(buttonPanel);
		buttonPanel.setOpaque(false);
	
		return activityContainer;
	}
	
	private JPanel createActivityFormContainer() {
		JPanel buttonPanel = new JPanel();
		formComponents.add(buttonPanel);
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		
		buttonPanel.add(formStatus);
		buttonPanel.add(Box.createHorizontalGlue());
		formComponents.add(done);
		buttonPanel.add(done);
		done.setOpaque(false);
		buttonPanel.add(Box.createHorizontalStrut(5));
		formComponents.add(cancel);
		buttonPanel.add(cancel);
		cancel.setOpaque(false);
		
		JPanel activityContainer = new JPanel();
		formComponents.add(activityContainer);
		activityContainer.setLayout(new BoxLayout(activityContainer, BoxLayout.Y_AXIS));
		
		formComponents.add(activityForm);
		activityContainer.add(activityForm);
		activityForm.setOpaque(false);
		activityContainer.add(Box.createVerticalStrut(10));
		activityContainer.add(buttonPanel);
		buttonPanel.setOpaque(false);
	
		return activityContainer;
	}
	
	private void uiEvents() {
		editActivity.addActionListener((e) -> {
			if (editing) {
				showAlreadyEditingError();
				return;
			}
			
			PersonalActivityInfo info = activityListPanel.getSelectedActivityInfo(facade);
			if (info == null) {
				showActivityError(NO_SELECTION_ERROR);
				return;
			} else {
				activityListStatus.setText("");
			}
			
			activityForm.setActivityName(info.getName());
			activityForm.setActivityNameEditable(false);
			activityForm.setActivityStartWeek(info.getStartYear(), info.getStartWeek());
			activityForm.setActivityEndWeek(info.getEndYear(), info.getEndWeek());

			editing = true;
			createNewActivity = false;
			setActivityFormEnabled(true);
			
			formBorder.setTitle(ACTIVITY_EDIT_FORM_BORDER_TITLE);
		});

		addActivity.addActionListener((e) -> {
			if (editing) {
				showAlreadyEditingError();
				return;
			}
			
			activityListStatus.setText("");
			
			activityForm.setActivityNameEditable(true);

			editing = true;
			createNewActivity = true;
			setActivityFormEnabled(true);

			formBorder.setTitle(ACTIVITY_ADD_FORM_BORDER_TITLE);
		});
		
		done.addActionListener((e) -> {
			String name = activityForm.getActivityName();
			if (name.isEmpty()) {
				showFormError(EMPTY_ACTIVITY_NAME_ERROR, null);
				return;
			}
			
			if (!activityForm.isValidActivitySpan()) {
				showFormError(INVALID_ACTIVITY_SPAN_ERROR, null);
				return;
			}
			
			if (createNewActivity) {
				try {
					facade.createActivity(name);
				} catch (NameUnavailableException | PermissionDeniedException e1) {
					showFormError(CREATE_ERROR, e1);
					return;
				}
				
				createNewActivity = false;
			}
			
			boolean error = false;
			if (activityForm.isValidActivitySpan()) {
				facade.selectActivity(name);
				
				try {
					facade.setActivityStart(activityForm.getStartWeek(), activityForm.getStartYear());
					facade.setActivityEnd(activityForm.getEndWeek(), activityForm.getEndYear());
				} catch (PermissionDeniedException | InvalidSpanException e1) {
					showFormError(WEEKSPAN_ERROR, e1);
					formBorder.setTitle(ACTIVITY_EDIT_FORM_BORDER_TITLE);
					activityForm.setActivityNameEditable(false);
					error = true;
				} finally {
					facade.unselect();
					
					// Make sure to reselect journal
					facade.selectJournal();
				}
			}
			
			if (!error) {
				showActivitySuccess(ACTIVITY_SAVED_SUCCESS);
				formStatus.setText("");
				
				activityListPanel.updateCells(facade);

				stopEditing();
			}
		});

		cancel.addActionListener((e) -> {
			activityListStatus.setText("");
			formStatus.setText("");

			stopEditing();
		});
	}
	
	private void showFormError(String msg, Exception e) {
		formStatus.setForeground(GUIHelper.ERROR_COLOR);
		
		if (e != null) {
			formStatus.setText(msg + e.getMessage());
		} else {
			formStatus.setText(msg);
		}
	}
	
	private void showActivityError(String msg) {
		activityListStatus.setForeground(GUIHelper.ERROR_COLOR);
		activityListStatus.setText(msg);
	}

	private void showActivitySuccess(String msg) {
		activityListStatus.setForeground(GUIHelper.SUCCESS_COLOR);
		activityListStatus.setText(msg);
	}
	
	private void stopEditing() {
		createNewActivity = false;
		editing = false;
		
		activityForm.clear();
		setActivityFormEnabled(false);
		formBorder.setTitle(ACTIVITY_ADD_FORM_BORDER_TITLE);
	}
	
	private void showAlreadyEditingError() {
		showActivityError(ALREADY_EDITING_ERROR);
	}

	private void setActivityFormEnabled(boolean enabled) {
		for (JComponent comp : formComponents)
			comp.setEnabled(enabled);
	}
	
	@Override
	public void onEnter() {
		facade.selectJournal();
		activityListPanel.updateCells(facade);
		activityListStatus.setText("");
		formStatus.setText("");
		
		if (editing)
			stopEditing();
		
		activityForm.clear();
		setActivityFormEnabled(false);
	}

	@Override
	public void onExit() {
		facade.unselect();
	}
}
