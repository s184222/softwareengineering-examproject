package com.softwarehuset.projectmanager.ui.panel.dashboard;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import com.softwarehuset.projectmanager.app.facade.ProjectInfo;
import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.ui.Display;
import com.softwarehuset.projectmanager.ui.component.TitleLabel;
import com.softwarehuset.projectmanager.ui.panel.project.ActivityListPanel;

//Rasmus M. Larsen s184190
@SuppressWarnings("serial")
public class ProjectListTab extends DashboardTab {

	private static final String PROJECTS_TITLE = "Projects";
	private static final String NO_PROJECT_SELECTED_TITLE = "No selected project";
	
	private static final String ADD_NEW_LABEL = "New project";
	private static final String EDIT_LABEL = "Edit project";
	
	private static final String PROJECT_LIST_BORDER_TITLE = "Project list";
	private static final String PROJECT_INFO_BORDER_TITLE = "Project information";
	private static final String ACTIVITY_LIST_BORDER_TITLE = "Project activities";
	
	private final JLabel selectedProjectTitle;
	private final JButton addNewButton;
	private final JButton editButton;
	
	private final ProjectListPanel projectListPanel;
	private final ProjectInfoPanel projectInfoPanel;
	private final ActivityListPanel activityListPanel;
	
	public ProjectListTab(ProjectManagerFacade facade, DashboardPanel dashboard) {
		super(facade, dashboard);
		
		setOpaque(false);
	
		selectedProjectTitle = new TitleLabel(NO_PROJECT_SELECTED_TITLE, true);
		addNewButton = new JButton(ADD_NEW_LABEL);
		editButton = new JButton(EDIT_LABEL);
		
		projectListPanel = new ProjectListPanel();
		projectListPanel.setBorder(BorderFactory.createTitledBorder(PROJECT_LIST_BORDER_TITLE));
		projectInfoPanel = new ProjectInfoPanel();
		projectInfoPanel.setBorder(BorderFactory.createTitledBorder(PROJECT_INFO_BORDER_TITLE));
		activityListPanel = new ActivityListPanel();
		activityListPanel.setBorder(BorderFactory.createTitledBorder(ACTIVITY_LIST_BORDER_TITLE));
		activityListPanel.setRowSelectionAllowed(false);
		
		uiLayout();
		uiEvents();
	}
	
	private void uiLayout() {
		setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.CENTER;
	
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		JLabel projectsTitle = new TitleLabel(PROJECTS_TITLE, true);
		projectsTitle.setHorizontalAlignment(JLabel.CENTER);
		add(projectsTitle, constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;

		constraints.gridy++;
		constraints.anchor = GridBagConstraints.SOUTH;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(new JSeparator(JSeparator.HORIZONTAL), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.anchor = GridBagConstraints.CENTER;
		
		constraints.gridy = 0;
		constraints.gridx++;
		constraints.gridheight = 2;
		constraints.fill = GridBagConstraints.VERTICAL;
		add(new JSeparator(JSeparator.VERTICAL), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.gridheight = 1;

		constraints.gridy = 0;
		constraints.gridx++;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		selectedProjectTitle.setHorizontalAlignment(JLabel.CENTER);
		add(selectedProjectTitle, constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;

		constraints.gridy++;
		constraints.anchor = GridBagConstraints.SOUTH;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(new JSeparator(JSeparator.HORIZONTAL), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.anchor = GridBagConstraints.CENTER;
		
		constraints.gridx = 0;
		
		constraints.gridy++;
		constraints.ipadx = 20;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.insets.top = 20;
		add(projectListPanel, constraints);
		constraints.insets.top = 5;
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.ipadx = 0;
		
		constraints.gridy++;
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.VERTICAL;
		add(Box.createVerticalGlue(), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weighty = 0.0;
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		buttonPanel.add(addNewButton);
		addNewButton.setOpaque(false);
		buttonPanel.add(Box.createHorizontalStrut(5));
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(editButton);
		editButton.setOpaque(false);
		
		constraints.gridy++;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(buttonPanel, constraints);
		buttonPanel.setOpaque(false);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		
		JPanel selectedPanel = new JPanel();
		selectedPanel.setLayout(new BoxLayout(selectedPanel, BoxLayout.Y_AXIS));
		selectedPanel.add(projectInfoPanel);
		projectInfoPanel.setOpaque(false);
		selectedPanel.add(Box.createVerticalStrut(20));
		selectedPanel.add(activityListPanel);
		activityListPanel.setOpaque(false);
		selectedPanel.add(Box.createVerticalGlue());
		
		constraints.gridy = 2;
		constraints.gridx = 2;
		constraints.gridheight = 3;
		constraints.ipadx = 20;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.insets.top = 20;
		add(selectedPanel, constraints);
		selectedPanel.setOpaque(false);
		constraints.insets.top = 5;
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.ipadx = 0;
		constraints.gridheight = 1;
	}
	
	private void uiEvents() {
		addNewButton.addActionListener((e) -> {
			Display display = dashboard.getDisplay();
			display.getProjectEditorPanel().prepareNewProject();
			display.setActiveCard(Display.PROJECT_EDITOR_CARD_ID);
		});
		
		editButton.addActionListener((e) -> {
			ProjectInfo info = projectListPanel.getSelectedProjectInfo(facade);
			Display display = dashboard.getDisplay();
			display.getProjectEditorPanel().prepareProjectEdit(info);
			display.setActiveCard(Display.PROJECT_EDITOR_CARD_ID);
		});
		
		projectListPanel.addSelectionListener((e) -> {
			updateProjectSelection();
		});
	}

	private void updateProjectSelection() {
		ProjectInfo info = projectListPanel.getSelectedProjectInfo(facade);
		if (info != null) {
			projectInfoPanel.setProjectInfo(info);
			selectedProjectTitle.setText(info.getName());
			
			activityListPanel.updateCells(facade, info.getProjectId());
		} else {
			selectedProjectTitle.setText(NO_PROJECT_SELECTED_TITLE);
		}
	}
	
	@Override
	public void onEnter() {
		projectListPanel.updateCells(facade);
		updateProjectSelection();
	}

	@Override
	public void onExit() {
		
	}
}
