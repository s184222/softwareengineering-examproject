package com.softwarehuset.projectmanager.ui.panel.dashboard;

import java.util.Collection;

import javax.swing.table.DefaultTableModel;

import com.softwarehuset.projectmanager.app.facade.ProjectInfo;
import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.ui.component.TableListPanel;

//Tobias H. M�ller s184217
@SuppressWarnings("serial")
public class ProjectListPanel extends TableListPanel {

	private static final int NAME_COLUMN = 0;
	private static final int ID_COLUMN = 1;
	private static final int PROJECT_MANAGER_COLUMN = 2;
	private static final int NUM_OF_ACTIVITIES_COLUMN = 3;
	
	private static final String[] PROJECT_COLUMNS = new String[] {
		"Name",
		"ID",
		"Project manager",
		"Number of activities"
	};
	
	public ProjectListPanel() {
		super(PROJECT_COLUMNS);
	}
	
	public void updateCells(ProjectManagerFacade facade) {
		Collection<ProjectInfo> projects = facade.getAllProjectInfos();
		
		DefaultTableModel tableModel = getModel();
		
		int r = 0;
		tableModel.setRowCount(projects.size());
		for (ProjectInfo project : projects) {
			tableModel.setValueAt(project.getName(), r, NAME_COLUMN);
			tableModel.setValueAt(project.getProjectId(), r, ID_COLUMN);
			tableModel.setValueAt(project.getNumberOfActivities(), r, NUM_OF_ACTIVITIES_COLUMN);

			String projectManager = project.getProjectManagerId();
			tableModel.setValueAt(projectManager != null ? projectManager : "-", r, PROJECT_MANAGER_COLUMN);
			
			r++;
		}
		
		if (tableModel.getRowCount() != 0)
			getTable().getSelectionModel().setSelectionInterval(0, 0);
	}
	
	public ProjectInfo getSelectedProjectInfo(ProjectManagerFacade facade) {
		DefaultTableModel tableModel = getModel();
		if (tableModel.getRowCount() <= 0)
			return null;
		
		int index = getTable().getSelectedRow();
		String projectId = (String)tableModel.getValueAt(index, ID_COLUMN);
		return facade.getProjectInfoFromId(projectId);
	}
}
