package com.softwarehuset.projectmanager.ui.panel.dashboard;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.softwarehuset.projectmanager.app.facade.ProjectInfo;

//Gustav Gr�nvold s184217
@SuppressWarnings("serial")
public class ProjectInfoPanel extends JPanel {

	private static final String ACTIVITY_NAME_LABEL = "Project name:";
	private static final String PROJECT_ID_LABEL = "Project ID:";
	private static final String PROJECT_MANAGER_LABEL = "Project manager:";
	private static final String NUM_ACTIVITIES_LABEL = "Number of activities:";
	
	private static final String EMPTY_FIELD = "-";
	
	private final JLabel nameField;
	private final JLabel projectIdField;
	private final JLabel managerField;
	private final JLabel numActivitiesField;
	
	public ProjectInfoPanel() {
		this.nameField = new JLabel();
		this.managerField = new JLabel();
		this.projectIdField = new JLabel();
		this.numActivitiesField = new JLabel();
		
		uiLayout();
	}
	
	private void uiLayout() {
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.NONE;

		constraints.gridy = 0;
		addFieldRow(constraints, new JLabel(ACTIVITY_NAME_LABEL), nameField);

		constraints.gridy++;
		addFieldRow(constraints, new JLabel(PROJECT_ID_LABEL), projectIdField);
		
		constraints.gridy++;
		addFieldRow(constraints, new JLabel(PROJECT_MANAGER_LABEL), managerField);

		constraints.gridy++;
		addFieldRow(constraints, new JLabel(NUM_ACTIVITIES_LABEL), numActivitiesField);
	}
	
	private void addFieldRow(GridBagConstraints constraints, JLabel label, JComponent field) {
		constraints.gridx = 0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(label, constraints);
		constraints.fill = GridBagConstraints.NONE;
		
		constraints.gridx++;
		
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(field, constraints);
		field.setPreferredSize(new Dimension(200, 30));
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
	}
	
	public void setProjectName(String projectName) {
		nameField.setText(projectName);
	}
	
	public String getProjectName() {
		return nameField.getText();
	}

	public void setProjectId(String id) {
		projectIdField.setText(id);
	}

	public String getProjectId() {
		return projectIdField.getText();
	}
	
	public void setProjectManager(String projectManager) {
		managerField.setText(projectManager);
	}
	
	public String getProjectManager() {
		return managerField.getText();
	}

	public void setNumActivities(String numActivities) {
		numActivitiesField.setText(numActivities);
	}
	
	public String setNumActivities() {
		return numActivitiesField.getText();
	}
	
	public void setProjectInfo(ProjectInfo info) {
		setProjectName(info.getName());
		setProjectId(info.getProjectId());
		setProjectManager(info.getProjectManagerId() == null ? EMPTY_FIELD : info.getProjectManagerId());
		setNumActivities(Integer.toString(info.getNumberOfActivities()));
	}
	
	public void clear() {
		setProjectName(EMPTY_FIELD);
		setProjectId(EMPTY_FIELD);
		setProjectManager(EMPTY_FIELD);
		setNumActivities(EMPTY_FIELD);
	}
}
