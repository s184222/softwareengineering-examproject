package com.softwarehuset.projectmanager.ui.panel.dashboard;

import javax.swing.JPanel;

import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;

//Christian M. Fuglsang s184222
@SuppressWarnings("serial")
public abstract class DashboardTab extends JPanel {

	protected final ProjectManagerFacade facade;
	protected final DashboardPanel dashboard;
	
	public DashboardTab(ProjectManagerFacade facade, DashboardPanel dashboard) {
		this.facade = facade;
		this.dashboard = dashboard;
	}

	public abstract void onEnter();

	public abstract void onExit();

}
