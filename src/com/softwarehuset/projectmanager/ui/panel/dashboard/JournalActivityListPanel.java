package com.softwarehuset.projectmanager.ui.panel.dashboard;

import java.util.Collection;

import javax.swing.table.DefaultTableModel;

import com.softwarehuset.projectmanager.app.facade.PersonalActivityInfo;
import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.ui.GUIHelper;
import com.softwarehuset.projectmanager.ui.component.TableListPanel;

//Christian M. Fuglsang s184222
@SuppressWarnings("serial")
public class JournalActivityListPanel extends TableListPanel {

	private static final int NAME_COLUMN = 0;
	private static final int START_DATE_COLUMN = 1;
	private static final int END_DATE_COLUMN = 2;
	
	private static final String[] JOURNAL_ACTIVITY_COLUMNS = new String[] {
		"Name",
		"Start week",
		"End week"
	};
	
	public JournalActivityListPanel() {
		super(JOURNAL_ACTIVITY_COLUMNS);
	}
	
	public void updateCells(ProjectManagerFacade facade) {
		Collection<PersonalActivityInfo> infos = facade.getAllPersonalActivityInfos();

		DefaultTableModel tableModel = getModel();
		
		int i = 0;
		tableModel.setRowCount(infos.size());
		for (PersonalActivityInfo info : infos) {
			tableModel.setValueAt(info.getName(), i, NAME_COLUMN);
			tableModel.setValueAt(GUIHelper.formatActivitySpan(info.getStartYear(), info.getStartWeek()), i, START_DATE_COLUMN);
			tableModel.setValueAt(GUIHelper.formatActivitySpan(info.getEndYear(), info.getEndWeek()), i, END_DATE_COLUMN);
			
			i++;
		}
		
		if (tableModel.getRowCount() != 0)
			getTable().getSelectionModel().setSelectionInterval(0, 0);
	}

	public PersonalActivityInfo getSelectedActivityInfo(ProjectManagerFacade facade) {
		DefaultTableModel tableModel = getModel();
		if (tableModel.getRowCount() <= 0)
			return null;
		
		int index = getTable().getSelectedRow();
		String activityName = (String)tableModel.getValueAt(index, NAME_COLUMN);
		return facade.getPersonalActivityInfo(activityName);
	}
}
