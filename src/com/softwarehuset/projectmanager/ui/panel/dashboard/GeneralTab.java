package com.softwarehuset.projectmanager.ui.panel.dashboard;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.ui.Display;
import com.softwarehuset.projectmanager.ui.component.TitleLabel;

//Rasmus M. Larsen s184190
@SuppressWarnings("serial")
public class GeneralTab extends DashboardTab {

	private static final String GENERAL_TITLE = "General";
	
	private static final String LOGOUT_BUTTON_TEXT = "Sign out";
	
	private static final String HELLO_TITLE = "Hello ";
	private static final String MESSAGE_LABEL = "<html><p>"
			+ "Welcome to Project Manager.<br> This platform makes it possible to register, "
			+ "personal activites such as illness and vacations.<br>"
			+ "Furthemore you can see all the projects and project activies that Softwarehuset A/S<br>"
			+ "currently is working on. And much much more...</p></html>";
	
	private final JButton signOutButton;
	private final JLabel nameLabel;
	
	public GeneralTab(ProjectManagerFacade facade, DashboardPanel dashboard) {
		super(facade, dashboard);

		setOpaque(false);
		
		signOutButton = new JButton(LOGOUT_BUTTON_TEXT);
		nameLabel = new TitleLabel(true);
		
		uiLayout();
		uiEvents();
	}
	
	private void uiLayout() {
		setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.fill = GridBagConstraints.NONE;
	
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		JLabel projectsTitle = new TitleLabel(GENERAL_TITLE, true);
		projectsTitle.setHorizontalAlignment(JLabel.CENTER);
		add(projectsTitle, constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
	
		constraints.gridy++;
		constraints.anchor = GridBagConstraints.SOUTH;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(new JSeparator(JSeparator.HORIZONTAL), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.anchor = GridBagConstraints.CENTER;
		
		constraints.gridy++;
		constraints.anchor = GridBagConstraints.WEST;
		constraints.insets.top = 20;
		add(getMessagePanel(), constraints);
		constraints.insets.top = 5;
		constraints.anchor = GridBagConstraints.CENTER;
		
		constraints.gridy++;

		// Add filler
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.BOTH;
		add(Box.createVerticalGlue(), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weighty = 0.0;
		
		constraints.gridy++;

		JPanel buttonPanel = new JPanel();
		buttonPanel.setOpaque(false);
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(signOutButton);
		signOutButton.setOpaque(false);
		
		constraints.gridwidth = 2;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(buttonPanel, constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.gridwidth = 1;
	}
	
	private JPanel getMessagePanel() {
		JPanel messagePanel = new JPanel();
		messagePanel.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.fill = GridBagConstraints.NONE;
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.anchor = GridBagConstraints.WEST;
		messagePanel.add(new TitleLabel(HELLO_TITLE, true), constraints);
		constraints.anchor = GridBagConstraints.CENTER;
		
		constraints.gridx++;
		constraints.anchor = GridBagConstraints.WEST;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		messagePanel.add(nameLabel, constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.anchor = GridBagConstraints.CENTER;

		constraints.gridx = 0;
		constraints.gridy++;
		constraints.gridwidth = 2;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.insets.top = 10;
		JLabel messageLabel = new JLabel(MESSAGE_LABEL);
		messageLabel.setFont(messageLabel.getFont().deriveFont(12.0f));
		messagePanel.add(messageLabel, constraints);

		messagePanel.setOpaque(false);

		return messagePanel;
	}
	
	private void uiEvents() {
		signOutButton.addActionListener((e) -> {
			facade.signOut();
			dashboard.getDisplay().setActiveCard(Display.SIGN_IN_CARD_ID);
		});
	}

	@Override
	public void onEnter() {
		String userId = facade.getUserId();
		if (userId != null)
			nameLabel.setText(userId);
	}

	@Override
	public void onExit() {
	}
}
