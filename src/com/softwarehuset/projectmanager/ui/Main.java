package com.softwarehuset.projectmanager.ui;

import com.softwarehuset.projectmanager.app.ProjectManagerApp;
import com.softwarehuset.projectmanager.app.facade.DefaultProjectManagerFacade;
import com.softwarehuset.projectmanager.showcase.ShowcaseProjectManagerAppConfiguration;

//Rasmus M. Larsen s184190
public class Main {
	
	private static final String DISPLAY_TITLE = "ProjectManager";
	
	public static void main(String[] args) {
		ProjectManagerApp app = new ProjectManagerApp(new ShowcaseProjectManagerAppConfiguration());
		Display display = new Display(new DefaultProjectManagerFacade(app));

		// This will also set visible to
		// true and hence start the GUI.
		display.create(DISPLAY_TITLE);
	}
}
