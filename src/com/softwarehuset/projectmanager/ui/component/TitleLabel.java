package com.softwarehuset.projectmanager.ui.component;

import javax.swing.JLabel;

//Tobias H. M�ller s184217
@SuppressWarnings("serial")
public class TitleLabel extends JLabel {

	private static final float TITLE_FONT_SIZE = 20.0f;
	private static final float SUB_TITLE_FONT_SIZE = 18.0f;
	
	public TitleLabel() {
		this(false);
	}

	public TitleLabel(boolean subtitle) {
		this("", subtitle);
	}

	public TitleLabel(String msg) {
		this(msg, false);
	}
	
	public TitleLabel(String msg, boolean subtitle) {
		super(msg);
		
		setFont(getFont().deriveFont(subtitle ? SUB_TITLE_FONT_SIZE : TITLE_FONT_SIZE));
	}
}
