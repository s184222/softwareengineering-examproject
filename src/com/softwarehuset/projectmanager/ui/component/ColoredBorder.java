package com.softwarehuset.projectmanager.ui.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.border.Border;

//Gustav Gr�nvold s184217
public class ColoredBorder implements Border {

	private final Insets insets;
	private Color color;

	public ColoredBorder(Color color, Insets insets) {
		this.color = color;
		this.insets = new Insets(0, 0, 0, 0);
		if (insets != null)
			this.setInsets(insets);
	}

	public ColoredBorder(Color color, int top, int left, int bottom, int right) {
		this(color, new Insets(top, left, bottom, right));
	}

	public ColoredBorder(Color color) {
		this(color, null);
	}

	public ColoredBorder() {
		this(null, null);
	}

	public void setInsets(Insets insets) {
		this.setInsets(insets.top, insets.left, insets.bottom, insets.right);
	}

	public void setInsets(int top, int left, int bottom, int right) {
		this.insets.set(top, left, bottom, right);
	}

	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
		if (color == null)
			return;
		Color oldColor = g.getColor();
		g.setColor(this.color);
		g.fillRect(x, y, width, insets.top);
		g.fillRect(x, y, insets.left, height);
		g.fillRect(x, height - insets.bottom, width, height);
		g.fillRect(width - insets.right, y, width, height);
		g.setColor(oldColor);
	}

	@Override
	public Insets getBorderInsets(Component c) {
		return this.insets;
	}

	@Override
	public boolean isBorderOpaque() {
		return true;
	}

}
