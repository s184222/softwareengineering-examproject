package com.softwarehuset.projectmanager.ui.component;

import javax.swing.table.DefaultTableModel;

//Tobias H. M�ller s184217
@SuppressWarnings("serial")
public class UneditableTableModel extends DefaultTableModel {

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}
}
