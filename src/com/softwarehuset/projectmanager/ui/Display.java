package com.softwarehuset.projectmanager.ui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.ui.panel.ManagerPanel;
import com.softwarehuset.projectmanager.ui.panel.activity.ActivityEditorPanel;
import com.softwarehuset.projectmanager.ui.panel.dashboard.DashboardPanel;
import com.softwarehuset.projectmanager.ui.panel.project.ProjectEditorPanel;
import com.softwarehuset.projectmanager.ui.panel.signin.SignInPanel;

//Christian M. Fuglsang s184222
public class Display {

	public static final String SIGN_IN_CARD_ID = "SIGN_IN";
	public static final String DASHBOARD_CARD_ID = "DASHBOARD";
	public static final String PROJECT_EDITOR_CARD_ID = "PROJECT_EDITOR";
	public static final String ACTIVITY_EDITOR_CARD_ID = "ACTIVITY_EDITOR";
	
	private static final int MINIMUM_PADDING = 30;
	
	private final ProjectManagerFacade facade;

	private JFrame frame;
	private SignInPanel loginPanel;
	private DashboardPanel dashboardPanel;
	private ProjectEditorPanel projectEditor;
	private ActivityEditorPanel activityEditor;

	private JPanel cardsPanel;
	private CardLayout cardLayout;
	private String currentCardId;
	private Map<String, ManagerPanel> cards;
	
	public Display(ProjectManagerFacade facade) {
		this.facade = facade;
		cards = new HashMap<String, ManagerPanel>();

		setSystemLookAndFeel();
	}
	
	private void setSystemLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
		}
	}
	
	public void create(String title) {
		if (frame != null)
			throw new IllegalStateException("Display already initialized!");
		
		frame = new JFrame(title);
		loginPanel = new SignInPanel(facade, this);
		dashboardPanel = new DashboardPanel(facade, this);
		projectEditor = new ProjectEditorPanel(facade, this);
		activityEditor = new ActivityEditorPanel(facade, this);
		
		uiLayout();
		uiEvents();
		
		setActiveCard(SIGN_IN_CARD_ID);
	}
	
	private void uiLayout() {
		createCardsPanel();
		
		frame.setLayout(new BorderLayout());
		frame.add(cardsPanel, BorderLayout.CENTER);

		frame.setResizable(true);
		frame.pack();
		
		frame.setMinimumSize(getMinimumSize());

		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setVisible(true);
	}
	
	private Dimension getMinimumSize() {
		Dimension minimumSize = cardsPanel.getMinimumSize();
		minimumSize.width += MINIMUM_PADDING;
		minimumSize.height += MINIMUM_PADDING;
		return minimumSize;
	}
	
	private JPanel createCardsPanel() {
		cardsPanel = new JPanel();
		cardLayout = new CardLayout();
		cardsPanel.setLayout(cardLayout);
		
		initCards();
		
		return cardsPanel;
	}

	private void initCards() {
		addCard(loginPanel, SIGN_IN_CARD_ID);
		addCard(dashboardPanel, DASHBOARD_CARD_ID);
		addCard(projectEditor, PROJECT_EDITOR_CARD_ID);
		addCard(activityEditor, ACTIVITY_EDITOR_CARD_ID);
	}

	private void addCard(ManagerPanel panel, String id) {
		cardsPanel.add(panel, id);
		cards.put(id, panel);
	}
	
	private void closeFrame() {
		frame.dispose();
		System.exit(0);
	}
	
	private void uiEvents() {
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				closeFrame();
			}
		});
	}

	public void setActiveCard(String cardId) {
		if (!cardId.equals(currentCardId)) {
			ManagerPanel panel = cards.get(currentCardId);
			if (panel != null)
				panel.onExit();
			
			cardLayout.show(cardsPanel, cardId);
			
			panel = cards.get(cardId);
			if (panel != null)
				panel.onEnter();
		}
	}
	
	public ProjectManagerFacade getFacade() {
		return facade;
	}

	public ProjectEditorPanel getProjectEditorPanel() {
		return projectEditor;
	}

	public ActivityEditorPanel getActivityEditorPanel() {
		return activityEditor;
	}

	public JFrame getFrame() {
		return frame;
	}
}
