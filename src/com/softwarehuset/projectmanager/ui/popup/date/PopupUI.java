package com.softwarehuset.projectmanager.ui.popup.date;

import java.awt.Window;
import java.awt.event.WindowEvent;

import javax.swing.JPanel;

//Christian M. Fuglsang s184222
@SuppressWarnings("serial")
public class PopupUI extends JPanel {

	protected void closeWindow() {
		Window window = (Window)this.getTopLevelAncestor();
		window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
	}
}
