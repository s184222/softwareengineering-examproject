package com.softwarehuset.projectmanager.ui.popup.date;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.plaf.basic.BasicLabelUI;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import com.softwarehuset.projectmanager.ui.component.ColoredBorder;
import com.softwarehuset.projectmanager.ui.component.UneditableTableModel;

//Christian M. Fuglsang s184222
public class SimpleFormCalendar extends JScrollPane {
	private static final long serialVersionUID = -107678331484659310L;

	private static final String[] WEEK_DESC = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };
	private static final Period EMPTY_SELECTION = new Period();

	private final JTable calendar;
	private final DefaultTableModel calendarModel;
	private final DateCellRenderer calendarRenderer;

	private int currentYear;
	private int currentMonth;

	private int realYear;
	private int realMonth;

	private int cellWidth;
	private int cellHeight;

	private WeekPanel weeks;

	private int selectedRow;
	private int selectedColumn;
	private final Period selection;

	private final List<Period> unavailable;
	private final List<Integer> available;
	private final List<Integer> clickable;

	private int periodLength;

	private boolean lost;
	private boolean canChangeDate;
	
	@SuppressWarnings("serial")
	public SimpleFormCalendar(List<Period> unavailable) {
		this.unavailable = new ArrayList<Period>(unavailable);

		// Initialize components
		calendar = new JTable() {
			@Override
			public Dimension getPreferredScrollableViewportSize() {
				return this.getPreferredSize();
			}
		};
		calendarModel = new UneditableTableModel();
		calendarRenderer = new DateCellRenderer();
		weeks = new WeekPanel();

		available = new ArrayList<Integer>();
		clickable = new ArrayList<Integer>();

		selection = new Period();

		// Settings
		cellWidth = 38;
		cellHeight = 38;
		calendar.setModel(this.calendarModel);
		addHeaders();
		calendar.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		calendar.setRowHeight(38);
		calendar.getTableHeader().setResizingAllowed(false);
		calendar.getTableHeader().setReorderingAllowed(false);
		calendar.setCellSelectionEnabled(true);
		calendar.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		calendar.setRowHeight(this.cellHeight);
		calendar.setShowGrid(false);
		calendarModel.setColumnCount(7);
		calendarModel.setRowCount(6);
		TableColumn column = null;
		for (int i = 0; i < this.calendar.getColumnCount(); i++) {
			column = this.calendar.getColumnModel().getColumn(i);
			column.setPreferredWidth(this.cellWidth);
		}
		calendar.setDefaultRenderer(this.calendar.getColumnClass(0), this.calendarRenderer);
		calendar.setGridColor(calendar.getBackground());
		setBorder(BorderFactory.createEmptyBorder());

		// This is simply to fix an exception
		// where row would be -1 when selecting the first time.
		calendar.setRowSelectionInterval(0, 0);
		calendar.setColumnSelectionInterval(0, 0);

		uiLayout();
		uiEvents();
	}

	private void addHeaders() {
		calendarModel.setColumnIdentifiers(WEEK_DESC);
	}

	private void uiLayout() {
		setViewportView(calendar);
	}

	private void uiEvents() {
		calendar.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				selectionChanged();
			}
		});
		calendar.getColumnModel().addColumnModelListener(new TableColumnModelListener() {
			@Override
			public void columnSelectionChanged(ListSelectionEvent e) {
				selectionChanged();
			}

			public void columnRemoved(TableColumnModelEvent e) {
			}

			public void columnMoved(TableColumnModelEvent e) {
			}

			public void columnMarginChanged(ChangeEvent e) {
			}

			public void columnAdded(TableColumnModelEvent e) {
			}
		});
		calendar.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				lost = true;
			}

			@Override
			public void focusGained(FocusEvent e) {
				if (lost) {
					lost = false;
					int row = calendar.getSelectedRow();
					int column = calendar.getSelectedColumn();

					if (row == selectedRow && column == selectedColumn) {
						selectionChanged();
					}
				}
			}
		});
	}

	public void refreshCalendar() {
		// Empty calendar
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 7; j++) {
				calendar.setValueAt(null, i, j);
			}
		}

		available.clear();
		clickable.clear();

		// Fill in dates
		Calendar calendar = new GregorianCalendar(this.currentYear, this.currentMonth, 1);
		int nod = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		int som = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		if (som <= 1)
			som += 7;

		for (int i = 0; i < nod; i++) {
			int row = (i + som - 1) / 7;
			int col = (i + som - 1) % 7;
			int date = i + 1;
			this.calendar.setValueAt((date), row, col);
			if (isAvailable(date)) {
				available.add(date);
				if (isClickable(date)) {
					clickable.add(date);
				}
			}
		}

		calendar.add(Calendar.DATE, -som + 1);

		int row = this.calendar.getRowCount();

		weeks.setLength(row);

		for (int i = 0; i < row; i++) {
			weeks.set(i, calendar.get(Calendar.WEEK_OF_YEAR));
			calendar.add(Calendar.DATE, 7);
		}

		weeks.generate();
	}

	public WeekPanel getWeeks() {
		return weeks;
	}

	public boolean isAvailable(int date) {
		for (Period period : unavailable) {
			if (period.isSelected(currentYear, currentMonth, date)) {
				return false;
			}
		}

		return true;
	}

	public boolean isClickable(int date) {
		Calendar cal = new GregorianCalendar(currentYear, currentMonth, date);

		// Only allow selections when the week begins.
		if (cal.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY)
			return false;
			
		cal.add(Calendar.DATE, periodLength - 1);

		int yearEnd = cal.get(Calendar.YEAR);
		int monthEnd = cal.get(Calendar.MONTH);
		int dayEnd = cal.get(Calendar.DAY_OF_MONTH);

		PeriodDate date1 = new PeriodDate(currentYear, currentMonth, date);
		PeriodDate date2 = new PeriodDate(yearEnd, monthEnd, dayEnd);
		Period needsAvailable = new Period(date1, date2);

		for (Period period : unavailable) {
			if (period.intersects(needsAvailable)) {
				return false;
			}
		}

		return canChangeDate;
	}
	
	public void setClickable(boolean clickable) {
		canChangeDate = clickable;
	}

	public void selectionChanged() {
		int row = calendar.getSelectedRow();
		int column = calendar.getSelectedColumn();

		Object value = calendar.getValueAt(row, column);

		if (value != null) {
			int date = Integer.parseInt(value.toString());

			if (!clickable.contains(date))
				return;
		}

		selectedRow = row;
		selectedColumn = column;
		ActionEvent e = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, Action.ACTION_COMMAND_KEY);
		fireActionEvent(e);
	}

	public void setDate(int year, int month) {
		currentYear = year;
		currentMonth = month;
	}

	public void setDefaultDate(int year, int month, int day) {
		realYear = year;
		realMonth = month;
		setToDefault();
	}

	public void setSelection(Period selection) {
		if (selection == null)
			selection = EMPTY_SELECTION;
		this.selection.set(selection);
		repaint();
	}

	public int getSelectedRow() {
		return selectedRow;
	}

	public int getSelectedColumn() {
		return selectedColumn;
	}

	public Object getItemAt(int row, int column) {
		return calendar.getValueAt(row, column);
	}

	public int getCurrentYear() {
		return currentYear;
	}

	public int getCurrentMonth() {
		return currentMonth;
	}

	public void setToDefault() {
		setDate(realYear, realMonth);
	}

	public void setPeriodLength(int length) {
		periodLength = length;
	}

	public void fireActionEvent(ActionEvent e) {
		for (ActionListener al : getActionListeners()) {
			al.actionPerformed(e);
		}
	}

	public void addActionListener(ActionListener actionListener) {
		listenerList.add(ActionListener.class, actionListener);
	}

	public void removeActionListener(ActionListener l) {
		listenerList.remove(ActionListener.class, l);
	}

	public ActionListener[] getActionListeners() {
		return listenerList.getListeners(ActionListener.class);
	}

	private class DateCellRenderer extends DefaultTableCellRenderer {
		private static final long serialVersionUID = 7012462397920202676L;

		private final Color availableColor = new Color(204, 255, 204);
		private final Color textColor = new Color(0, 0, 0);
		private final Color clickableColor = new Color(0, 128, 0);
		private final Color unavailableColor = new Color(170, 170, 170);
		private final Color selectionColor = new Color(103, 189, 103);

		private Object value;

		private DateCellRenderer() {
			setHorizontalAlignment(CENTER);
			setVerticalAlignment(CENTER);
			this.setUI(new BasicLabelUI() {
				public void paint(Graphics g, JComponent c) {
					if (c.isOpaque()) {
						if (value != null) {
							int day = Integer.parseInt(value.toString());
							g.setColor(selectionColor);
							if (selection.isStart(currentYear, currentMonth, day)) {
								g.fillRect(cellWidth / 2 - 5, 0, cellWidth / 2 + 10, cellHeight);
							} else if (selection.isEnd(currentYear, currentMonth, day)) {
								g.fillRect(0, 0, cellWidth / 2 + 5, cellHeight);
							} else if (selection.isSelected(currentYear, currentMonth, day)) {
								g.fillRect(0, 0, cellWidth, cellHeight);
							}
						}
					}
					super.paint(g, c);
				}
			});
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {
			super.getTableCellRendererComponent(table, value, selected, focused, row, column);

			this.value = value;

			setOpaque(true);

			if (value == null) {
				setBackground(table.getBackground());
				setBorder(BorderFactory.createEmptyBorder());
			} else {
				int date = Integer.parseInt(value.toString());

				if (available.contains(date)) {
					setBackground(availableColor);
				} else {
					setBackground(unavailableColor);
				}

				if (clickable.contains(date)) {
					setBorder(new ColoredBorder(clickableColor, 4, 4, 4, 4));
				} else {
					setBorder(BorderFactory.createEmptyBorder());
				}

				setForeground(textColor);
			}

			return this;
		}
	}

	public static class WeekPanel extends JPanel {
		private static final long serialVersionUID = 8650785308713090872L;

		private static final int VERTICAL = 0;
		private static final int HORIZONTAL = 1;

		private JLabel[] weeks;
		private boolean changed;
		private int length;
		private int orientation;

		public WeekPanel(int length, int orientation) {
			setLength(length);
			this.orientation = validateOrientation(orientation);
		}

		public WeekPanel(int length) {
			this(length, VERTICAL);
		}

		public WeekPanel() {
			this(0);
		}

		private int validateOrientation(int orientation) {
			switch (orientation) {
			case WeekPanel.VERTICAL:
				return orientation;
			case WeekPanel.HORIZONTAL:
				return orientation;
			default:
				return WeekPanel.VERTICAL;
			}
		}

		public void setLength(int length) {
			if (length == this.length)
				return;
			this.length = length;
			weeks = new JLabel[length];
			for (int i = 0; i < length; i++) {
				weeks[i] = new JLabel();
			}
			changed = true;
		}

		public void set(int index, int week) {
			if (index < 0)
				throw new IndexOutOfBoundsException("index < 0");
			if (index >= length)
				throw new IndexOutOfBoundsException("index >= length");

			weeks[index].setText(Integer.toString(week));
		}

		public void generate() {
			if (!changed)
				return;

			GridLayout layout = null;

			if (this.orientation == VERTICAL) {
				layout = new GridLayout(length, 1);
			} else if (this.orientation == HORIZONTAL) {
				layout = new GridLayout(1, length);
			}

			setLayout(layout);

			for (int i = 0; i < this.length; i++) {
				add(this.weeks[i]);
				weeks[i].setHorizontalAlignment(JLabel.CENTER);
				weeks[i].setVerticalAlignment(JLabel.CENTER);
			}

			changed = false;
		}
	}
}
