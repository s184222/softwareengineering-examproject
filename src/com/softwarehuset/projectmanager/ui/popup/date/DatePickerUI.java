package com.softwarehuset.projectmanager.ui.popup.date;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicArrowButton;

import com.softwarehuset.projectmanager.ui.Display;
import com.softwarehuset.projectmanager.ui.popup.PopupManager;

//Christian M. Fuglsang s184222
public class DatePickerUI extends PopupUI {
	private static final long serialVersionUID = -6803913691021901292L;

	private static final String WEEK_LABEL = "Week";
	private static final String FINISH_LABEL = "Done";
	private static final String RESET_LABEL = "Reset";
	private static final String SELECTION_LABEL = "Go to selection";
	private static final String CANCEL_LABEL = "Cancel";
	private static final String PERIOD_LENGTH_LABEL = "Period (weeks):";
	private static final String CHOOSE_MONTH_LABEL = "Month:";
	
	private static final String YEAR_LABEL = "Year";
	
	private static final String JAN_LABEL = "January";
	private static final String FEB_LABEL = "February";
	private static final String MAR_LABEL = "Marts";
	private static final String APR_LABEL = "April";
	private static final String MAY_LABEL = "May";
	private static final String JUN_LABEL = "June";
	private static final String JUL_LABEL = "July";
	private static final String AUG_LABEL = "August";
	private static final String SEP_LABEL = "September";
	private static final String OCT_LABEL = "October";
	private static final String NOV_LABEL = "November";
	private static final String DEC_LABEL = "December";
	
	private static final String NO_SELECTION_ERROR = "No selection";
	
	private static final int DAYS_PER_WEEK = 7;
	
	private final Display display;
	
	private final JLabel mon1;
	private final JLabel mon2;
	private final JLabel week1;
	private final JLabel week2;
	private final JLabel year;
	private final JLabel monthLabel;
	private final JLabel periodLabel;
	private final JButton next;
	private final JButton prev;
	private final JButton next_year;
	private final JButton prev_year;
	private final JButton finish;
	private final JButton cancel;
	private final JButton reset;
	private final JButton gotoSelection;
	
	private final SimpleFormCalendar cal1;
	private final SimpleFormCalendar cal2;
	private final JSpinner periodLength;
	private final JComboBox<String> month;

	private Period period;

	private int realDay;
	private int realMonth;
	private int realYear;
	private int maxMonths;
	private int minimumMonths;

	private int currentMonth;
	private int currentYear;

	private boolean canceled;

	private boolean firstSelection;
	private int periodTime;

	private int oldRow;
	private int oldColumn;
	private int oldYear;
	private int oldMonth;
	private Object oldValue;

	public DatePickerUI(Display display, List<Period> unavailable) {
		this.display = display;
		
		// Initialize components
		mon1 = new JLabel();
		mon2 = new JLabel();
		week1 = new JLabel(WEEK_LABEL);
		week2 = new JLabel(WEEK_LABEL);
		next = new BasicArrowButton(JButton.EAST);
		prev = new BasicArrowButton(JButton.WEST);
		next_year = new BasicArrowButton(JButton.EAST);
		year = new JLabel();
		prev_year = new BasicArrowButton(JButton.WEST);
		finish = new JButton(FINISH_LABEL);
		reset = new JButton(RESET_LABEL);
		gotoSelection = new JButton(SELECTION_LABEL);
		cancel = new JButton(CANCEL_LABEL);

		cal1 = new SimpleFormCalendar(unavailable);
		cal1.setClickable(true);
		cal2 = new SimpleFormCalendar(unavailable);

		periodLabel = new JLabel(PERIOD_LENGTH_LABEL);
		periodLength = new JSpinner(new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1));
		periodLength.setEditor(new JSpinner.NumberEditor(periodLength, "#"));
		
		periodLength.setValue(Integer.valueOf(1));
		
		monthLabel = new JLabel(CHOOSE_MONTH_LABEL);
		month = new JComboBox<String>();

		// Settings
		firstSelection = true;
		setBorder(new EmptyBorder(5, 5, 5, 5));

		mon1.setHorizontalAlignment(JLabel.CENTER);
		mon2.setHorizontalAlignment(JLabel.CENTER);
		year.setHorizontalAlignment(JLabel.CENTER);
		getDate();

		String[] months = new String[this.maxMonths + 1];

		for (int i = 0; i <= this.maxMonths; i++) {
			months[i] = getMonthLabel(i);
		}

		month.setModel(new DefaultComboBoxModel<String>(months));

		setToday();
		updatePeriodTime();
		updateCalendar();

		// Create UI
		uiLayout();
		uiEvents();
	}

	private void getDate() {
		Calendar cal = Calendar.getInstance();

		this.realDay = cal.get(Calendar.DAY_OF_MONTH);
		this.realMonth = cal.get(Calendar.MONTH);
		this.realYear = cal.get(Calendar.YEAR);
		this.maxMonths = cal.getMaximum(Calendar.MONTH);
		this.minimumMonths = cal.getMinimum(Calendar.MONTH);

		this.cal1.setDefaultDate(this.realYear, this.realMonth, this.realDay);
		this.cal2.setDefaultDate(this.realYear, this.realMonth, this.realDay);
	}

	private void uiLayout() {
		this.setLayout(new GridBagLayout());

		// Constraints first hand operations
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridy = 0;
		constraints.insets = new Insets(2, 2, 2, 2);
		constraints.fill = GridBagConstraints.NONE;

		// Top bar
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.X_AXIS));

		topPanel.add(periodLabel);
		topPanel.add(Box.createHorizontalStrut(4));
		topPanel.add(periodLength);

		constraints.gridx = 0;
		constraints.gridwidth = 2;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.WEST;
		add(topPanel, constraints);

		topPanel = new JPanel();
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.X_AXIS));

		topPanel.add(monthLabel);
		topPanel.add(Box.createHorizontalStrut(4));
		topPanel.add(month);

		constraints.gridx = 3;
		constraints.gridwidth = 2;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.EAST;
		add(topPanel, constraints);

		// Calendar titles start
		constraints.gridy++;
		constraints.gridx = 0;
		constraints.weightx = 0;
		constraints.gridwidth = 1;
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.EAST;
		add(prev, constraints);

		constraints.gridx++;
		constraints.weightx = 1;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.anchor = GridBagConstraints.CENTER;
		add(mon1, constraints);

		constraints.gridx++;
		constraints.weightx = 0;
		constraints.fill = GridBagConstraints.BOTH;
		add(new JSeparator(JSeparator.VERTICAL), constraints);

		constraints.gridx++;
		constraints.weightx = 1;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(mon2, constraints);

		constraints.gridx++;
		constraints.weightx = 0;
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.WEST;
		add(next, constraints);
		// Calendar titles end

		// Title splitter
		constraints.gridwidth = GridBagConstraints.RELATIVE;
		constraints.gridx = 1;
		constraints.gridy++;
		constraints.fill = GridBagConstraints.BOTH;
		add(new JSeparator(JSeparator.HORIZONTAL), constraints);
		constraints.gridwidth = 1;

		// Week label
		constraints.gridy++;
		constraints.gridx = 0;
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0;
		constraints.anchor = GridBagConstraints.CENTER;
		add(week1, constraints);

		constraints.gridy++;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = 0;
		add(cal1.getWeeks(), constraints);

		// Month1 table
		constraints.gridy--;
		constraints.gridx++;
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.gridheight = 2;
		add(cal1, constraints);

		// Month2 table
		constraints.gridx += 2;
		add(cal2, constraints);

		// Week label
		constraints.gridx++;
		constraints.anchor = GridBagConstraints.EAST;
		constraints.weightx = 0;
		constraints.weighty = 0;
		constraints.gridheight = 1;
		constraints.anchor = GridBagConstraints.CENTER;
		add(week2, constraints);

		constraints.gridy++;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = 0;
		add(cal2.getWeeks(), constraints);

		// Button panel
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(1, 3));

		JPanel leftButtons = new JPanel();
		leftButtons.setLayout(new BoxLayout(leftButtons, BoxLayout.X_AXIS));

		leftButtons.add(Box.createHorizontalStrut(2));
		leftButtons.add(finish);
		leftButtons.add(Box.createHorizontalStrut(4));
		leftButtons.add(reset);
		leftButtons.add(Box.createGlue());

		buttonPanel.add(leftButtons);

		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridBagLayout());

		GridBagConstraints buttonConstraints = new GridBagConstraints();
		buttonConstraints.insets = new Insets(2, 2, 2, 2);
		buttonConstraints.gridy = 0;
		buttonConstraints.fill = GridBagConstraints.NONE;

		buttonConstraints.gridx = 0;
		buttonConstraints.weightx = 1;
		buttonConstraints.anchor = GridBagConstraints.EAST;
		centerPanel.add(prev_year, buttonConstraints);

		buttonConstraints.gridx++;
		buttonConstraints.weightx = 0.3;
		buttonConstraints.anchor = GridBagConstraints.CENTER;
		buttonConstraints.fill = GridBagConstraints.BOTH;
		centerPanel.add(year, buttonConstraints);
		buttonConstraints.fill = GridBagConstraints.NONE;

		buttonConstraints.gridx++;
		buttonConstraints.weightx = 1;
		buttonConstraints.anchor = GridBagConstraints.WEST;
		centerPanel.add(next_year, buttonConstraints);

		buttonPanel.add(centerPanel);

		JPanel rightButtons = new JPanel();
		rightButtons.setLayout(new BoxLayout(rightButtons, BoxLayout.X_AXIS));

		rightButtons.add(Box.createGlue());
		rightButtons.add(gotoSelection);
		rightButtons.add(Box.createHorizontalStrut(4));
		rightButtons.add(cancel);
		rightButtons.add(Box.createHorizontalStrut(2));

		buttonPanel.add(rightButtons);

		constraints.gridx = 0;
		constraints.gridy++;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		constraints.anchor = GridBagConstraints.SOUTH;
		constraints.weightx = 1;
		constraints.weighty = 0;
		add(buttonPanel, constraints);
		constraints.gridwidth = 1;
	}

	private void uiEvents() {
		next.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				currentMonth++;
				verify();
				updateCalendar();
			}
		});
		prev.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				currentMonth--;
				verify();
				updateCalendar();
			}
		});
		cancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancel();
				closeWindow();
			}
		});
		prev_year.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				currentYear--;
				updateCalendar();
			}
		});
		next_year.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				currentYear++;
				updateCalendar();
			}
		});
		reset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clearSelection();
				setToday();
				updateCalendar();
			}
		});
		cal1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateSelection(cal1);
			}
		});
		cal2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateSelection(cal2);
			}
		});
		periodLength.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				updatePeriodTime();
			}
		});
		gotoSelection.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				gotoSelection();
			}
		});
		month.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateMonth();
			}
		});
		finish.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				finish();
			}
		});
	}

	private void finish() {
		if (canceled)
			return;
		
		if (period == null) {
			PopupManager.popupError(display, NO_SELECTION_ERROR);
			return;
		}

		closeWindow();
	}
	
	private void updateMonth() {
		int month = this.month.getSelectedIndex();

		if (month != currentMonth) {
			currentMonth = month;
			updateCalendar();
		}
	}

	private void gotoSelection() {
		if (period == null)
			return;
		PeriodDate date = period.getStartDate();

		int year = date.getYear();
		int month = date.getMonth();

		if (currentYear == year && currentMonth == month)
			return;
		currentYear = year;
		currentMonth = month;

		updateCalendar();
	}

	private void clearSelection() {
		period = null;
		cal1.setSelection(null);
		cal2.setSelection(null);
	}

	private void updateSelection(SimpleFormCalendar owner) {
		if (owner == null && firstSelection)
			return;

		firstSelection = false;

		int selectedRow = oldRow;
		int selectedColumn = oldColumn;
		int year = oldYear;
		int month = oldMonth;

		Object value = oldValue;

		if (owner != null) {
			selectedRow = owner.getSelectedRow();
			selectedColumn = owner.getSelectedColumn();

			value = owner.getItemAt(selectedRow, selectedColumn);
			year = owner.getCurrentYear();
			month = owner.getCurrentMonth();
		}
		
		if (value != null) {
			oldRow = selectedRow;
			oldColumn = selectedColumn;
			oldYear = year;
			oldMonth = month;
			oldValue = value;

			int day = Integer.parseInt(value.toString());

			SimpleFormCalendar calendar = null;

			// Check which calendar is currently selected
			if (this.cal1.getCurrentMonth() == month) {
				calendar = this.cal1;
			} else {
				calendar = this.cal2;
			}

			if (calendar != null) {
				if (!calendar.isClickable(day)) {
					return;
				}
			}

			Calendar cal = new GregorianCalendar(year, month, day);
			cal.add(Calendar.DATE, periodTime - 1);

			int yearEnd = cal.get(Calendar.YEAR);
			int monthEnd = cal.get(Calendar.MONTH);
			int dayEnd = cal.get(Calendar.DAY_OF_MONTH);

			PeriodDate date1 = new PeriodDate(year, month, day);
			PeriodDate date2 = new PeriodDate(yearEnd, monthEnd, dayEnd);
			period = new Period(date1, date2);

			cal1.setSelection(this.period);
			cal2.setSelection(this.period);
		}
		
		updateCalendar();
	}

	private void updatePeriodTime() {
		int weeks = ((Integer)periodLength.getValue()).intValue();
		periodTime = weeks * DAYS_PER_WEEK;

		cal1.setPeriodLength(periodTime);
		cal2.setPeriodLength(periodTime);

		if (period == null) {
			updateSelection(null);
			updateCalendar();
			return;
		}

		SimpleFormCalendar calendar = null;

		// Check which calendar is currently selected
		if (this.cal1.getCurrentMonth() == period.getStartDate().getMonth()) {
			calendar = cal1;
		} else {
			calendar = cal2;
		}

		if (calendar.isClickable(period.getStartDate().getDay())) {
			updateSelection(null);
			updateCalendar();
		} else {
			clearSelection();
			updateCalendar();
		}
	}

	private void verify() {
		while (currentMonth > maxMonths) {
			currentMonth = minimumMonths;
			currentYear++;
		}
		while (currentMonth < minimumMonths) {
			currentMonth = maxMonths;
			currentYear--;
		}
	}

	private void setToday() {
		currentMonth = realMonth;
		currentYear = realYear;
	}

	private void updateCalendar() {
		int mon1 = currentMonth;
		int year1 = currentYear;

		int mon2 = -1;
		int year2 = -1;
		if (period != null) {
			PeriodDate startDate = period.getStartDate();
			PeriodDate endDate = period.getEndDate();
			if (endDate.getMonth() > startDate.getMonth() || endDate.getYear() > startDate.getYear()) {
				Calendar cal = new GregorianCalendar(endDate.getYear(), endDate.getMonth(), endDate.getDay());
				cal.add(Calendar.MONTH, currentMonth - startDate.getMonth());
				cal.add(Calendar.YEAR, currentYear - startDate.getYear());
				mon2 = cal.get(Calendar.MONTH);
				year2 = cal.get(Calendar.YEAR);
			}
		}
		
		if (mon2 == -1 || year2 == -1) {
			mon2 = currentMonth + 1;
			year2 = currentYear;

			if (mon2 > maxMonths) { // Month 2 is in a new year
				mon2 = minimumMonths;
				year2++;
			}
		}


		// Change month titles, to the correct one
		String month1 = getMonthLabel(mon1);
		String month2 = getMonthLabel(mon2);
		this.mon1.setText(month1 + " " + year1);
		this.mon2.setText(month2 + " " + year2);

		// Change year label
		String year = YEAR_LABEL;
		if (year1 != year2) {
			year += " " + year1 + "-" + year2;
		} else {
			year += " " + year1;
		}
		this.year.setText(year);

		// Change month combo
		this.month.setSelectedIndex(this.currentMonth);

		cal1.setDate(year1, mon1);
		cal1.setPeriodLength(periodTime);
		cal1.refreshCalendar();

		cal2.setDate(year2, mon2);
		cal2.setPeriodLength(periodTime);
		cal2.refreshCalendar();
	}

	private String getMonthLabel(int mon) {
		switch (mon) {
		case Calendar.JANUARY:
			return JAN_LABEL;
		case Calendar.FEBRUARY:
			return FEB_LABEL;
		case Calendar.MARCH:
			return MAR_LABEL;
		case Calendar.APRIL:
			return APR_LABEL;
		case Calendar.MAY:
			return MAY_LABEL;
		case Calendar.JUNE:
			return JUN_LABEL;
		case Calendar.JULY:
			return JUL_LABEL;
		case Calendar.AUGUST:
			return AUG_LABEL;
		case Calendar.SEPTEMBER:
			return SEP_LABEL;
		case Calendar.OCTOBER:
			return OCT_LABEL;
		case Calendar.NOVEMBER:
			return NOV_LABEL;
		case Calendar.DECEMBER:
			return DEC_LABEL;
		default:
			// Should never happen
			return "Invalid month";
		}
	}

	public void cancel() {
		this.canceled = true;
	}

	public Period getPeriod() {
		return this.canceled ? null : this.period;
	}
}
