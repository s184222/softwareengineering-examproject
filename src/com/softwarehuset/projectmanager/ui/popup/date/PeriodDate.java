package com.softwarehuset.projectmanager.ui.popup.date;

import java.util.Calendar;
import java.util.GregorianCalendar;

//Gustav Gr�nvold s184217
public class PeriodDate {

	private int year;
	private int month;
	private int day;

	public PeriodDate(int year, int month, int day) {
		this.set(year, month, day);
	}

	public PeriodDate() {
		this(-1, -1, -1);
	}

	public PeriodDate(PeriodDate date) {
		this(date.year, date.month, date.day);
	}

	public void set(int year, int month, int day) {
		this.year = year;
		this.month = month;
		this.day = day;
	}

	public void set(PeriodDate date) {
		this.set(date.year, date.month, date.day);
	}

	public int getYear() {
		return this.year;
	}

	public int getMonth() {
		return this.month;
	}

	public int getDay() {
		return this.day;
	}

	public long getValue() {
		return (long) this.day + this.month * 100L + this.year * 10000L;
	}

	public void fromValue(long value) {
		int year = (int) (value / 10000L);
		int month = (int) (value % 10000L / 100L);
		int day = (int) (value % 100);

		this.set(year, month, day);
	}

	public boolean isReal() {
		return this.year >= 0 && this.month >= 0 && this.day >= 0;
	}

	public int getWeekOfYear() {
		Calendar cal = new GregorianCalendar(year, month, day);
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		return cal.get(Calendar.WEEK_OF_YEAR);
	}
	
	public boolean equals(PeriodDate other) {
		if (other == null)
			return false;

		return this.getValue() == other.getValue();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof PeriodDate))
			return false;

		return this.equals((PeriodDate) obj);
	}

	@Override
	public String toString() {
		return String.format("%s/%s - %s", this.day, this.month, this.year);
	}
}