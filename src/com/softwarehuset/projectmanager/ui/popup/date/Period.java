package com.softwarehuset.projectmanager.ui.popup.date;

//Tobias H. M�ller s184217
public class Period {

	private final PeriodDate date1;
	private final PeriodDate date2;

	public Period(PeriodDate date1, PeriodDate date2) {
		this.date1 = new PeriodDate();
		this.date2 = new PeriodDate();
		if (date1 != null)
			this.date1.set(date1);
		if (date2 != null)
			this.date2.set(date2);
	}

	public Period(Period period) {
		this(period.date1, period.date2);
	}

	public Period() {
		this(null, null);
	}
	
	public void set(PeriodDate date1, PeriodDate date2) {
		this.date1.set(date1);
		this.date2.set(date2);
	}

	public void set(Period period) {
		this.set(period.date1, period.date2);
	}

	public PeriodDate getStartDate() {
		return this.date1.getValue() < this.date2.getValue() ? this.date1 : this.date2;
	}

	public PeriodDate getEndDate() {
		return this.date1.getValue() < this.date2.getValue() ? this.date2 : this.date1;
	}

	public boolean isStart(int year, int month, int day) {
		PeriodDate date = new PeriodDate(year, month, day);
		return this.isStart(date);
	}

	public boolean isStart(PeriodDate date) {
		PeriodDate start = getStartDate();
		return start.equals(date);
	}

	public boolean isEnd(int year, int month, int day) {
		PeriodDate date = new PeriodDate(year, month, day);
		return this.isEnd(date);
	}

	public boolean isEnd(PeriodDate date) {
		PeriodDate end = getEndDate();
		return end.equals(date);
	}

	public boolean isSelected(int year, int month, int day) {
		PeriodDate date = new PeriodDate(year, month, day);
		return this.isSelected(date);
	}

	public boolean isSelected(PeriodDate date) {
		PeriodDate start = getStartDate();
		PeriodDate end = getEndDate();
		return date.getValue() >= start.getValue() && date.getValue() <= end.getValue();
	}

	public boolean intersects(Period period) {
		if (this.isSelected(period.getStartDate()) || this.isSelected(period.getEndDate())) {
			return true;
		}
		return period.isSelected(getStartDate()) || period.isSelected(getEndDate());
	}

	public boolean isReal() {
		return this.date1.isReal() && this.date2.isReal();
	}

	public boolean equals(Period other) {
		if (other == null)
			return false;

		if (!this.getStartDate().equals(other.getStartDate()))
			return false;

		if (!this.getEndDate().equals(other.getEndDate()))
			return false;

		return true;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Period))
			return false;

		return this.equals((Period) obj);
	}

	@Override
	public String toString() {
		return this.getStartDate().toString() + " - " + this.getEndDate().toString();
	}
}