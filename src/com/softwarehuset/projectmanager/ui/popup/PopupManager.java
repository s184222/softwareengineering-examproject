package com.softwarehuset.projectmanager.ui.popup;

import java.awt.Component;
import java.util.ArrayList;

import javax.swing.JDialog;
import javax.swing.JFrame;

import com.softwarehuset.projectmanager.ui.Display;
import com.softwarehuset.projectmanager.ui.popup.date.DatePickerUI;
import com.softwarehuset.projectmanager.ui.popup.date.Period;

//Gustav Gr�nvold s184217
public class PopupManager {

	public static void popupError(Display display, String errorMessage) {
		JFrame frame = display.getFrame();
		Component comp = new ErrorMessageUI(errorMessage);
		JDialog dialog = new JDialog(frame, comp.getName(), true);
		dialog.add(comp);
		dialog.pack();
		dialog.setResizable(false);
		dialog.setLocationRelativeTo(frame);
		dialog.setVisible(true);
	}
	
	public static Period popupCalendar(Display display) {
		JFrame frame = display.getFrame();
		DatePickerUI datePicker = new DatePickerUI(display, new ArrayList<Period>());
		JDialog dialog = new JDialog(frame, datePicker.getName(), true);
		dialog.add(datePicker);
		dialog.pack();
		dialog.setResizable(false);
		dialog.setLocationRelativeTo(frame);
		dialog.setVisible(true);
		return datePicker.getPeriod();
	}
}
