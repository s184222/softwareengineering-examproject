package com.softwarehuset.projectmanager.ui.popup;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.softwarehuset.projectmanager.ui.GUIHelper;
import com.softwarehuset.projectmanager.ui.popup.date.PopupUI;

//Tobias H. M�ller s184217
public class ErrorMessageUI extends PopupUI {
	private static final long serialVersionUID = 7808217635573011352L;

	private static final String ERROR_POPUP_TITLE = "An error occured";
	private static final String DONE_LABEL = "Ok";
	
	private final JLabel msg;
	private final JButton done;

	public ErrorMessageUI(String msg) {
		this.msg = new JLabel(msg);
		done = new JButton(DONE_LABEL);

		this.msg.setForeground(GUIHelper.ERROR_COLOR);
		this.msg.setAlignmentX(JLabel.CENTER_ALIGNMENT);

		setName(ERROR_POPUP_TITLE);

		uiLayout();
		uiEvents();
	}

	private void uiLayout() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBorder(new EmptyBorder(5, 5, 5, 5));

		add(this.msg);
		add(Box.createVerticalStrut(10));

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(done);
		buttonPanel.add(Box.createHorizontalGlue());

		add(buttonPanel);
	}

	private void uiEvents() {
		done.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				closeWindow();
			}
		});
	}
}
