package com.softwarehuset.projectmanager.ui;

import java.awt.Color;

//Christian M. Fuglsang s184222
public class GUIHelper {

	// ISO week format: https://en.wikipedia.org/wiki/ISO_week_date
	private static final String SPAN_FORMATTING_LABEL = "%d-W%d";
	private static final String EXPECTED_HOURS_FORMATTING_LABEL = "%d h";
	private static final String WORK_HOURS_FORMATTING_LABEL = "%.2f h";

	public static final Color ERROR_COLOR = new Color(0xFF, 0x00, 0x00);
	// https://www.colourlovers.com/color/4BB543/Success_Green
	public static final Color SUCCESS_COLOR = new Color(0x4B, 0xB5, 0x43).darker();
	
	public static String formatActivitySpan(int year, int week) {
		return String.format(SPAN_FORMATTING_LABEL, year, week);
	}

	public static String formatExpectedHours(int expectedHours) {
		return String.format(EXPECTED_HOURS_FORMATTING_LABEL, expectedHours);
	}

	public static String formatWorkHours(double hours) {
		return String.format(WORK_HOURS_FORMATTING_LABEL, hours);
	}
}
