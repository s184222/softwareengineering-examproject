package com.softwarehuset.projectmanager.showcase;

import java.util.LinkedHashMap;
import java.util.Map;

import com.softwarehuset.projectmanager.database.InitialData;
import com.softwarehuset.projectmanager.date.DateServer;
import com.softwarehuset.projectmanager.exceptions.AssignableException;
import com.softwarehuset.projectmanager.exceptions.InvalidHourException;
import com.softwarehuset.projectmanager.exceptions.NameUnavailableException;
import com.softwarehuset.projectmanager.exceptions.ProjectIdGenerationException;
import com.softwarehuset.projectmanager.project.Journal;
import com.softwarehuset.projectmanager.project.Project;
import com.softwarehuset.projectmanager.project.ProjectActivity;
import com.softwarehuset.projectmanager.project.ProjectIdGenerator;
import com.softwarehuset.projectmanager.user.Admin;
import com.softwarehuset.projectmanager.user.Employee;
import com.softwarehuset.projectmanager.user.User;
import com.softwarehuset.projectmanager.work.WorkUnit;

// Gustav Gr�nvold s184217
public class ShowcaseInitialData implements InitialData {

	private Map<String, User> users = new LinkedHashMap<String, User>();
	private Map<String, Project> projects = new LinkedHashMap<String, Project>();
	private Map<String, Journal> journals = new LinkedHashMap<String, Journal>();

	private ProjectIdGenerator projectIdGenerator;
	private DateServer dateServer;

	public ShowcaseInitialData (ProjectIdGenerator projectIdGenerator, DateServer dateServer) {
		this.projectIdGenerator = projectIdGenerator;
		this.dateServer = dateServer;

		createAdmin();
		createEmployee("AAAA");
		createEmployee("BBBB");
		createEmployee("ELON");
		createEmployee("CCCC");
		createEmployee("DDDD");

		try {
			createSkypeProject();
			createDisneyProject();
			createGithubProject();
			createSpaceXRocketProject();
			createSpaceXMarsProject();
			
		} catch (NameUnavailableException | ProjectIdGenerationException | InvalidHourException | AssignableException e) {
			System.err.println("broken!? should never happen!");
		}

	}

	private void createSpaceXMarsProject() throws ProjectIdGenerationException, NameUnavailableException, AssignableException, InvalidHourException {
		Project project = createProject("SpaceX - Mars conoizing", "ELON");

		ProjectActivity findArea = createProjectActivity(project, "find area", 10410, 2019, 3, 2020, 42, "DDDD", "BBBB");
		createWorkUnit(findArea, "AAAA", "08-10-2019", 8.0);
		createWorkUnit(findArea, "AAAA", "10-10-2019", 3.0);
		
		ProjectActivity suits = createProjectActivity(project, "radiation suits", 1000, 2017, 8, 20120, 13, "BBBB");
		createWorkUnit(suits, "BBBB", "08-10-2020", 8.0);
		createWorkUnit(suits, "AAAA", "10-10-2019", 3.0);
		
		ProjectActivity laws = createProjectActivity(project, "laws/philosophy", 1000, 2017, 8, 20120, 13, "DDDD", "AAAA", "BBBB");
		createWorkUnit(laws, "BBBB", "08-10-2020", 8.0);
		createWorkUnit(laws, "AAAA", "10-10-2019", 3.0);
	}

	private void createSpaceXRocketProject() throws ProjectIdGenerationException, NameUnavailableException, AssignableException, InvalidHourException {
		Project project = createProject("SpaceX - Falcon 9", "ELON");

		ProjectActivity engineDesign = createProjectActivity(project, "engine design", 10410, 2019, 3, 2020, 42, "DDDD", "BBBB");
		createWorkUnit(engineDesign, "AAAA", "08-10-2019", 8.0);
		createWorkUnit(engineDesign, "AAAA", "10-10-2019", 3.0);
		
		ProjectActivity cockPit = createProjectActivity(project, "cockpit", 1000, 2017, 8, 20120, 13, "BBBB");
		createWorkUnit(cockPit, "BBBB", "08-10-2020", 8.0);
		createWorkUnit(cockPit, "AAAA", "10-10-2019", 3.0);
		
		ProjectActivity landing = createProjectActivity(project, "landing", 1000, 2017, 8, 20120, 13, "DDDD", "AAAA", "BBBB");
		createWorkUnit(landing, "BBBB", "08-10-2020", 8.0);
		createWorkUnit(landing, "AAAA", "10-10-2019", 3.0);
		
		ProjectActivity suits = createProjectActivity(project, "suits", 1000, 2017, 8, 20120, 13, "DDDD", "AAAA", "BBBB");
		createWorkUnit(suits, "BBBB", "08-10-2020", 8.0);
		createWorkUnit(suits, "AAAA", "10-10-2019", 3.0);
		
		ProjectActivity foodRessources = createProjectActivity(project, "food ressouces", 1000, 2017, 8, 20120, 13, "DDDD", "AAAA", "BBBB");
		createWorkUnit(foodRessources, "BBBB", "08-10-2020", 8.0);
		createWorkUnit(foodRessources, "AAAA", "10-10-2019", 3.0);
	}

	private void createGithubProject() throws ProjectIdGenerationException, NameUnavailableException, AssignableException, InvalidHourException {
		Project project = createProject("Github", null);

		ProjectActivity gitImplementation = createProjectActivity(project, "git implementation", 10410, 2019, 3, 2020, 42, "AAAA", "BBBB");
		createWorkUnit(gitImplementation, "AAAA", "08-10-2019", 8.0);
		createWorkUnit(gitImplementation, "AAAA", "10-10-2019", 3.0);
		
		ProjectActivity showRepositories = createProjectActivity(project, "show repositories", 1000, 2017, 8, 20120, 13, "BBBB");
		createWorkUnit(showRepositories, "BBBB", "08-10-2020", 8.0);
		createWorkUnit(showRepositories, "AAAA", "10-10-2019", 3.0);
		
		ProjectActivity loginFeature = createProjectActivity(project, "login-feature", 1000, 2017, 8, 20120, 13, "DDDD", "AAAA", "BBBB");
		createWorkUnit(loginFeature, "BBBB", "08-10-2020", 8.0);
		createWorkUnit(loginFeature, "AAAA", "10-10-2019", 3.0);
	}

	private void createDisneyProject() throws ProjectIdGenerationException, NameUnavailableException, AssignableException, InvalidHourException {
		Project project = createProject("Disney", null);

		ProjectActivity animations = createProjectActivity(project, "animations", 2000, 2018, 10, 2020, 12, "AAAA", "BBBB");
		createWorkUnit(animations, "AAAA", "08-10-2019", 8.0);
		createWorkUnit(animations, "AAAA", "10-10-2019", 3.0);
		
		ProjectActivity sound = createProjectActivity(project, "gui", 1000, 2017, 8, 20120, 13, "BBBB");
		createWorkUnit(sound, "BBBB", "08-10-2020", 8.0);
		createWorkUnit(sound, "AAAA", "10-10-2019", 3.0);
		
		ProjectActivity voiceOver = createProjectActivity(project, "voice-over", 1000, 2017, 8, 20120, 13, "DDDD", "AAAA", "BBBB");
		createWorkUnit(voiceOver, "BBBB", "08-10-2020", 8.0);
		createWorkUnit(voiceOver, "AAAA", "10-10-2019", 3.0);
	}

	private void createSkypeProject() throws ProjectIdGenerationException, NameUnavailableException, AssignableException, InvalidHourException {
		Project project = createProject("Skype", "AAAA");

		ProjectActivity videoFeature = createProjectActivity(project, "video-feature", 2000, 2018, 10, 2020, 12, "AAAA", "BBBB");
		createWorkUnit(videoFeature, "AAAA", "08-10-2019", 8.0);
		createWorkUnit(videoFeature, "AAAA", "10-10-2019", 3.0);
		
		ProjectActivity gui = createProjectActivity(project, "gui", 1000, 2017, 8, 20120, 13, "BBBB");
		createWorkUnit(gui, "BBBB", "08-10-2020", 8.0);
		createWorkUnit(gui, "AAAA", "10-10-2019", 3.0);
		
		ProjectActivity backend = createProjectActivity(project, "backend", 1000, 2017, 8, 20120, 13, "DDDD", "AAAA", "BBBB");
		createWorkUnit(backend, "BBBB", "08-10-2020", 8.0);
		createWorkUnit(backend, "AAAA", "10-10-2019", 3.0);
	}

	private void createWorkUnit(ProjectActivity activity, String employeeId, String date, double hours) throws InvalidHourException {
		WorkUnit unit = new WorkUnit(employeeId, date);
		unit.setHours(hours);
		activity.getMainRecord().register(unit);
	}

	private ProjectActivity createProjectActivity(Project project, String activityName, int expectedHours, int startYear, int startWeek, int endYear, int endWeek, String... assignedEmployees) throws NameUnavailableException, AssignableException, InvalidHourException {
		project.addActivity(activityName);

		ProjectActivity activity = project.getActivity(activityName);
		activity.setExpectedHours(expectedHours);

		for (String id : assignedEmployees) {
			activity.assign(id);
		}

		return project.getActivity(activityName);
	}

	private Project createProject(String projectName, String projectManagerId) throws ProjectIdGenerationException {
		Project project = new Project(projectIdGenerator.generateProjectId(dateServer), projectName);
		project.setProjectManager(projectManagerId);
		projects.put(project.getId(), project);
		return project;
	}

	private void createAdmin() {
		Admin admin = new Admin();
		users.put(admin.getId(), admin);
	}

	private void createEmployee(String initials) {
		Employee user = new Employee(initials);
		users.put(user.getId(), user);

		Journal journal = new Journal();
		journals.put(user.getId(), journal);
	}

	@Override
	public Map<String, User> getUsers() {
		return users;
	}

	@Override
	public Map<String, Project> getProjects() {
		return projects;
	}

	@Override
	public Map<String, Journal> getJournals() {
		return journals;
	}

}
