package com.softwarehuset.projectmanager.showcase;

import com.softwarehuset.projectmanager.app.ProjectManagerAppConfiguration;
import com.softwarehuset.projectmanager.database.Database;
import com.softwarehuset.projectmanager.database.InMemoryDatabase;
import com.softwarehuset.projectmanager.date.DateServer;
import com.softwarehuset.projectmanager.project.DefaultProjectIdGenerator;
import com.softwarehuset.projectmanager.project.ProjectIdGenerator;
import com.softwarehuset.projectmanager.security.DefaultSecurityProtocol;
import com.softwarehuset.projectmanager.security.SecurityProtocol;
import com.softwarehuset.projectmanager.session.DefaultInitialRequirement;
import com.softwarehuset.projectmanager.session.InitialRequirement;
import com.softwarehuset.projectmanager.session.Session;
import com.softwarehuset.projectmanager.work.DefaultWorkUnitRetrieverPolicy;
import com.softwarehuset.projectmanager.work.WorkRegistry;
import com.softwarehuset.projectmanager.work.WorkUnitRetrieverPolicy;

// Gustav Gr�nvold s184217
public class ShowcaseProjectManagerAppConfiguration implements ProjectManagerAppConfiguration {

	private final DateServer dateServer = new DateServer();
	private final ProjectIdGenerator generator = new DefaultProjectIdGenerator();

	private final Database database = new InMemoryDatabase(new ShowcaseInitialData(generator, dateServer));
	private final SecurityProtocol security = new DefaultSecurityProtocol();

	private final InitialRequirement requirement = new DefaultInitialRequirement();
	private final Session session = new Session(database, requirement);

	private final WorkUnitRetrieverPolicy retrieverPolicy = new DefaultWorkUnitRetrieverPolicy(session);
	private final WorkRegistry registry = new WorkRegistry(retrieverPolicy);

	@Override
	public Database getDatabase() {
		return database;
	}

	@Override
	public SecurityProtocol getSecurityProtocol() {
		return security;
	}

	@Override
	public Session getSession() {
		return session;
	}

	@Override
	public WorkRegistry getWorkRegistry() {
		return registry;
	}

	@Override
	public ProjectIdGenerator getProjectIdGenerator() {
		return generator;
	}

	@Override
	public DateServer getDateServer() {
		return dateServer;
	}

}
