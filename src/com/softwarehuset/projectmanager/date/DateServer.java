package com.softwarehuset.projectmanager.date;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

// Christian M. Fuglsang s184222
public class DateServer {
	
	private SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	private Calendar calendar = new GregorianCalendar();

	public int getYear() {
		return calendar.get(Calendar.YEAR);
	}

	public int getPartialYear() {
		return getYear() % 100;
	}

	public String getDate() {
		return formatter.format(calendar.getTime());
	}

	public int getWeek() {
		return calendar.get(Calendar.WEEK_OF_YEAR);
	}
}
