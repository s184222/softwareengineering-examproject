package com.softwarehuset.projectmanager.session;

// Christian M. Fuglsang s184222
public interface InitialRequirement {
	public boolean fulfilled(String id);
}
