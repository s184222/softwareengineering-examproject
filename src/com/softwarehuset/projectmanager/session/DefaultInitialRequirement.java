package com.softwarehuset.projectmanager.session;

// Christian M. Fuglsang s184222
public class DefaultInitialRequirement implements InitialRequirement {

	private static final int EMPLOYEE_MAX_NUMBER_OF_INITIALS = 4;

	@Override
	public boolean fulfilled(String id) {
		if ("admin".equals(id)) return true;
		if (id.length() > EMPLOYEE_MAX_NUMBER_OF_INITIALS) return false;
		return true;
	}

}
