package com.softwarehuset.projectmanager.session;

import com.softwarehuset.projectmanager.database.Database;
import com.softwarehuset.projectmanager.exceptions.InvalidEmployeeException;
import com.softwarehuset.projectmanager.selection.SelectionUnion;
import com.softwarehuset.projectmanager.user.User;

// Gustav Gr�nvold s184217
public class Session {

	private Database database;
	private InitialRequirement requirement;

	private User user;

	private SelectionUnion selection;

	public Session (Database database, InitialRequirement requirement) {
		this.database = database;
		this.requirement = requirement;

	}

	/**
	 * Sets the currently selected user to the user with
	 * the given user id.
	 * 
	 * @param userId - The user id of the requested user.
	 * 
	 * @throws UserUnavailableException if the user is not
	 *                                  available
	 */
	public void signin(String id) throws InvalidEmployeeException {
		if (!requirement.fulfilled(id))
			throw new InvalidEmployeeException("Incorrect number of initials");
		if (database.fetchUser(id) == null) {
			throw new InvalidEmployeeException("Employee not available");
		}

		clearSelection();

		user = null;
		user = database.fetchUser(id);
	}

	public void signout() {
		user = null;
		clearSelection();
	}

	public User getUser() {
		return user;
	}

	public SelectionUnion getSelectionUnion() {
		return selection;
	}
	
	public void clearSelection() {
		selection = SelectionUnion.createNone();
	}

	public void setSeletionUnion(SelectionUnion selection) {
		this.selection = selection;
	}
}