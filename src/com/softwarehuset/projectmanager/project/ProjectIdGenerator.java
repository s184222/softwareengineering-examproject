package com.softwarehuset.projectmanager.project;

import com.softwarehuset.projectmanager.date.DateServer;
import com.softwarehuset.projectmanager.exceptions.ProjectIdGenerationException;

// Tobias H. M�ller s184217
public interface ProjectIdGenerator {

	/**
	 * @return A newly generated unique project id
	 *         or throws exception, if no project id is available. 
	 */
	public String generateProjectId(DateServer dateServer) throws ProjectIdGenerationException;

}
