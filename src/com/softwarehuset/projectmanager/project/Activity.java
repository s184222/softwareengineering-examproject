package com.softwarehuset.projectmanager.project;

// Rasmus M. Larsen s184190
public abstract class Activity {

	private final String name;
	private final WeekYearSpan span = new WeekYearSpan();

	public Activity (String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public WeekYearSpan getSpan() {
		return span;
	}
}
