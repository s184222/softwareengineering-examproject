package com.softwarehuset.projectmanager.project;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.softwarehuset.projectmanager.exceptions.AssignableException;
import com.softwarehuset.projectmanager.exceptions.InvalidHourException;
import com.softwarehuset.projectmanager.work.Record;

// Rasmus M. Larsen s184190
public class ProjectActivity extends Activity implements WorkRegistrable, EmployeeAssignable, ExpectedHoursRegistrable {

	private final Record workRecord = new Record();
	private final Record pendingForeignWorkRecord = new Record();

	private final List<String> employeeIds = new ArrayList<String>();

	private int expectedHours;

	public ProjectActivity (String name) {
		super(name);
	}

	@Override
	public Record getMainRecord() {
		return workRecord;
	}

	@Override
	public Record getPreRecord() {
		return pendingForeignWorkRecord;
	}

	@Override
	public boolean hasEmployee(String employeeId) {
		return employeeIds.contains(employeeId);
	}

	@Override
	public void assign(String id) throws AssignableException {
		if (employeeIds.contains(id))
			throw new AssignableException("employee already assigned");

		employeeIds.add(id);
	}

	@Override
	public void setExpectedHours(int hours) throws InvalidHourException {
		if(hours < 0) throw new InvalidHourException("invalid hours");
		this.expectedHours = hours;
	}

	@Override
	public int getExpectedHours() {
		return expectedHours;
	}

	@Override
	public List<String> getAssignedEmployees() {
		return Collections.unmodifiableList(employeeIds);
	}

	public int getNumberAssigned() {
		return employeeIds.size();
	}

}
