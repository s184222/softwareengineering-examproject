package com.softwarehuset.projectmanager.project;

import com.softwarehuset.projectmanager.date.DateServer;
import com.softwarehuset.projectmanager.exceptions.ProjectIdGenerationException;

// Rasmus M. Larsen s184190
public class DefaultProjectIdGenerator implements ProjectIdGenerator {

	private static final int RUNNING_NUMBER_START = 1;
	private static final int MAX_RUNNING_NUMBER = 9999;

	private int currentPartialYear;
	private int nextProjectNumber = RUNNING_NUMBER_START;

	/**
	 * Generates an unique project id in the format: "xxyyyy".
	 * xx is the last two digits of the current year.
	 * yyyy is a running number.
	 * 
	 * @return a unique project id
	 */
	@Override
	public String generateProjectId(DateServer dateServer) throws ProjectIdGenerationException {
		checkForNewYear(dateServer);
		return String.format("%02d%04d", currentPartialYear, getNextRunningNumber());
	}

	private void checkForNewYear(DateServer dateServer) {
		int newPartialYear = dateServer.getPartialYear();
		if (newPartialYear != currentPartialYear) nextProjectNumber = RUNNING_NUMBER_START;
		currentPartialYear = newPartialYear;
	}

	private int getNextRunningNumber() throws ProjectIdGenerationException {
		if (nextProjectNumber > MAX_RUNNING_NUMBER) throw new ProjectIdGenerationException("Project number overflow");
		return nextProjectNumber++;
	}
}
