package com.softwarehuset.projectmanager.project;

import com.softwarehuset.projectmanager.exceptions.InvalidHourException;
// Rasmus M. Larsen s184190
public interface ExpectedHoursRegistrable {
	
	public void setExpectedHours(int hours) throws InvalidHourException;
	public int getExpectedHours();
	

}
