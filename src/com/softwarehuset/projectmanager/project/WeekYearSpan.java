package com.softwarehuset.projectmanager.project;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.softwarehuset.projectmanager.exceptions.InvalidSpanException;

// Rasmus M. Larsen s184190
public class WeekYearSpan {

	private Calendar start = new GregorianCalendar();
	private Calendar end = new GregorianCalendar();
	
	public WeekYearSpan () {
		start.setFirstDayOfWeek(Calendar.MONDAY);
		end.setFirstDayOfWeek(Calendar.MONDAY);
	}

	public void setStart(int week, int year) throws InvalidSpanException {
		Calendar request = getTemporyCalender(week, year);
		if (week < 0 || request.getWeeksInWeekYear() < week) throw new InvalidSpanException("invalid start date");

		start.set(Calendar.YEAR, year);
		start.set(Calendar.WEEK_OF_YEAR, week);
	}

	public void setEnd(int week, int year) throws InvalidSpanException {
		Calendar request = getTemporyCalender(week, year);
		if (week < 0 || request.getWeeksInWeekYear() < week) throw new InvalidSpanException("invalid end date");

		end.set(Calendar.YEAR, year);
		end.set(Calendar.WEEK_OF_YEAR, week);
	}

	public int getWeekStart() {
		return start.get(Calendar.WEEK_OF_YEAR);
	}

	public int getWeekEnd() {
		return end.get(Calendar.WEEK_OF_YEAR);
	}

	public int getYearStart() {
		return start.get(Calendar.YEAR);
	}

	public int getYearEnd() {
		return end.get(Calendar.YEAR);
	}

	public boolean within(int week, int year) {
		Calendar between = getTemporyCalender(week, year);
		return start.before(between) && end.after(between);
	}

	private Calendar getTemporyCalender(int week, int year) {
		Calendar tempory = new GregorianCalendar();
		tempory.setFirstDayOfWeek(Calendar.MONDAY);
		tempory.set(Calendar.YEAR, year);
		tempory.set(Calendar.WEEK_OF_YEAR, week);
		return tempory;

	}

}
