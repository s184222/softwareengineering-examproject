package com.softwarehuset.projectmanager.project;

import java.util.List;

import com.softwarehuset.projectmanager.exceptions.AssignableException;

// Rasmus M. Larsen s184190
public interface EmployeeAssignable {

	public void assign(String id) throws AssignableException;

	public boolean hasEmployee(String id);
	
	public List<String> getAssignedEmployees();

}
