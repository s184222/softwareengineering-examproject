package com.softwarehuset.projectmanager.project;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.softwarehuset.projectmanager.exceptions.NameUnavailableException;

// Gustav Gr�nvold s184217
public class Project {

	private final String id;
	private final String name;

	private String projectManagerId;

	private final Map<String, ProjectActivity> activities = new HashMap<String, ProjectActivity>();

	public Project (String id, String name) {
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}

	public void setProjectManager(String id) {
		this.projectManagerId = id;
	}

	public String getProjectManagerId() {
		return projectManagerId;
	}

	public void addActivity(String name) throws NameUnavailableException {
		if (activities.containsKey(name))
			throw new NameUnavailableException("activity already exists in the selected project");
		activities.put(name, new ProjectActivity(name));
	}

	public ProjectActivity getActivity(String name) {
		return activities.get(name);
	}

	public Set<String> getActivityNames() {
		return activities.keySet();
	}

	public int getNumberOfActivities() {
		return activities.size();
	}
	
	public void removeActivity(String name) throws NameUnavailableException {
		if (!activities.containsKey(name))
			throw new NameUnavailableException("Activity does not exist in the selected project");

		activities.remove(name);
	}
}
