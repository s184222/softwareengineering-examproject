package com.softwarehuset.projectmanager.project;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.softwarehuset.projectmanager.exceptions.NameUnavailableException;

// Rasmus M. Larsen s184190
public class Journal {

	private final List<PersonalActivity> activities = new ArrayList<PersonalActivity>();

	public void register(PersonalActivity activity) throws NameUnavailableException {
		if(exists(activity.getName())) {
			throw new NameUnavailableException("activity already exists");
		}
		
		activities.add(activity);
	}
	
	public boolean exists(String name) {
		return activities.stream()
			.filter(a -> a.getName().equals(name))
			.findAny().isPresent();
	}

	public PersonalActivity getActivity(String name) {
		return activities.stream()
			.filter(a -> a.getName().equals(name))
			.findAny().get();
	}

	public Set<String> getActivityNames() {
		return activities.stream()
			.map(a -> a.getName())
			.collect(Collectors.toSet());
	}

	public boolean occupied(int week, int year) {
		return activities.stream().anyMatch(a -> a.getSpan().within(week, year));
	}
}
