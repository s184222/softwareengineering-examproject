package com.softwarehuset.projectmanager.project;

import com.softwarehuset.projectmanager.work.Record;
// Rasmus M. Larsen s184190
public interface RecordBased {
	public Record getRecord();

}
