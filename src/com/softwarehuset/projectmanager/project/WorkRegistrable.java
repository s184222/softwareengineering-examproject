package com.softwarehuset.projectmanager.project;

import com.softwarehuset.projectmanager.work.Record;
//Rasmus M. Larsen s184190
public interface WorkRegistrable {
	public Record getMainRecord();
	public Record getPreRecord();

}
