package com.softwarehuset.projectmanager.user;

// Christian M. Fuglsang s184222
public interface User {

	public String getId();

	public boolean isAdmin();

}
