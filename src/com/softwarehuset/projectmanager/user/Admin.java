package com.softwarehuset.projectmanager.user;

// Christian M. Fuglsang s184222
public class Admin implements User {

	private static final String ADMIN_USER_ID = "admin";

	@Override
	public String getId() {
		return ADMIN_USER_ID;
	}

	@Override
	public boolean isAdmin() {
		return true;
	}

}
