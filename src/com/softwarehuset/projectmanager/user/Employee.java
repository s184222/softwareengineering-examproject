package com.softwarehuset.projectmanager.user;

// Christian M. Fuglsang s184222
public class Employee implements User {

	private final String id;

	public Employee (String userId) {
		this.id = userId;
	}

	@Override
	public final String getId() {
		return id;
	}

	@Override
	public final boolean isAdmin() {
		return false;
	}

}
