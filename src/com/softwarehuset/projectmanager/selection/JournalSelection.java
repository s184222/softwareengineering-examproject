package com.softwarehuset.projectmanager.selection;

import com.softwarehuset.projectmanager.project.Journal;
import com.softwarehuset.projectmanager.project.PersonalActivity;

// Rasmus M. Larsen s184190
public class JournalSelection {

	private Journal journal;
	private PersonalActivity activity;

	public Journal getJournal() {
		return journal;
	}

	public void setJournal(Journal journal) {
		this.journal = journal;
	}

	public PersonalActivity getActivity() {
		return activity;
	}

	public void setActivity(PersonalActivity activity) {
		this.activity = activity;
	}

}
