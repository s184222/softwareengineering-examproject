package com.softwarehuset.projectmanager.selection;

// Rasmus M. Larsen s184190
public class SelectionUnion {

	/*
	 * the union acts the same way as a sum-type in other programming languages, e.g. C.
	 * The selection is _always_ in one of the following states:
	 * 	1. none    (nothing selected)
	 * 	2. journal (journal + personal activity selected)
	 *  3. project (project + project activity selected)
	 * 
	 * The purpose of the union is to give a simple way to get the current selection.

	 */

	private final Object selected;
	private final SelectionType type;

	private SelectionUnion (Object selected, SelectionType type) {
		this.type = type;
		this.selected = selected;
	}

	@SuppressWarnings("unchecked")
	public <T> T get() {
		return (T) selected;
	}

	public SelectionType getType() {
		return type;
	}

	public static SelectionUnion createJournal(JournalSelection object) {
		return new SelectionUnion(object, SelectionType.journal);
	}

	public static SelectionUnion createProject(ProjectSelection object) {
		return new SelectionUnion(object, SelectionType.project);
	}

	public static SelectionUnion createNone() {
		return new SelectionUnion(null, SelectionType.none);
	}

	// https://stackoverflow.com/questions/929021/what-are-static-factory-methods

}
