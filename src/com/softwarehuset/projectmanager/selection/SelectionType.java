package com.softwarehuset.projectmanager.selection;

// Rasmus M. Larsen s184190
public enum SelectionType {
	none,
	project,
	journal;
}
