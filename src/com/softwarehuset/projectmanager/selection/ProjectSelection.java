package com.softwarehuset.projectmanager.selection;

import com.softwarehuset.projectmanager.project.Project;
import com.softwarehuset.projectmanager.project.ProjectActivity;

// Rasmus M. Larsen s184190
public class ProjectSelection {

	private Project project;
	private ProjectActivity activity;

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public ProjectActivity getActivity() {
		return activity;
	}

	public void setActivity(ProjectActivity activity) {
		this.activity = activity;
	}

}
