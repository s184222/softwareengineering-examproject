package com.softwarehuset.projectmanager.exceptions;

// Rasmus M. Larsen s184190
@SuppressWarnings("serial")
public class InvalidSpanException extends Exception {

	public InvalidSpanException (String message) {
		super(message);
	}

}
