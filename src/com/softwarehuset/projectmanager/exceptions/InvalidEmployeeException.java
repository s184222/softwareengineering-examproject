package com.softwarehuset.projectmanager.exceptions;

// Gustav Gr�nvold s184217
@SuppressWarnings("serial")
public class InvalidEmployeeException extends Exception {

    public InvalidEmployeeException(String message) {
        super(message);
    }
}
