package com.softwarehuset.projectmanager.exceptions;

// Tobias H. M�ller s184217
@SuppressWarnings("serial")
public class InvalidHourException extends Exception {

	public InvalidHourException (String message) {
		super(message);
	}

}
