package com.softwarehuset.projectmanager.exceptions;

// Christian M. Fuglsang s184222
@SuppressWarnings("serial") 
public class NameUnavailableException extends Exception {
	
	public NameUnavailableException (String name) {
		super(name);
	}

}
