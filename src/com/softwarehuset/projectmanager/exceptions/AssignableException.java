package com.softwarehuset.projectmanager.exceptions;

// Gustav Gr�nvold s184217
@SuppressWarnings("serial")
public class AssignableException extends Exception {
	
	public AssignableException (String msg) {
		super(msg);
	}

}
