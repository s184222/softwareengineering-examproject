package com.softwarehuset.projectmanager.exceptions;

// Christian M. Fuglsang s184222
@SuppressWarnings("serial") 
public class ProjectIdGenerationException extends Exception {

	public ProjectIdGenerationException (String message) {
		super(message);
	}
}
