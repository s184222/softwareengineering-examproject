package com.softwarehuset.projectmanager.exceptions;

// Tobias H. M�ller s184217
@SuppressWarnings("serial") 
public class PermissionDeniedException extends Exception {
	
	public PermissionDeniedException (String message) {
		super(message);
	}
}
