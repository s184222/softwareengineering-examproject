package com.softwarehuset.projectmanager.work;

import com.softwarehuset.projectmanager.selection.ProjectSelection;
import com.softwarehuset.projectmanager.session.Session;

// Rasmus M. Larsen s184190
public class DefaultWorkUnitRetrieverPolicy implements WorkUnitRetrieverPolicy {

	private Session session;

	public DefaultWorkUnitRetrieverPolicy (Session session) {
		this.session = session;
	}

	@Override
	public WorkUnitRetriever pick() {
		if (isForeign()) {
			return new ForeignWorkUnitRetriever(getPreRecord(), getMainRecord());
		} else {
			return new AssignedEmployeeWorkUnitRetriever(getMainRecord());
		}
	}

	private boolean isForeign() {
		ProjectSelection selection = session.getSelectionUnion().get();
		return !selection.getActivity().hasEmployee(session.getUser().getId());
	}

	private Record getMainRecord() {
		ProjectSelection selection = session.getSelectionUnion().get();
		return selection.getActivity().getMainRecord();
	}

	private Record getPreRecord() {
		ProjectSelection selection = session.getSelectionUnion().get();
		return selection.getActivity().getPreRecord();
	}

}
