package com.softwarehuset.projectmanager.work;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

//Gustav Gr�nvold s184217
public class Record {

	private List<WorkUnit> record = new ArrayList<WorkUnit>();

	public void register(WorkUnit unit) {
		record.add(unit);
	}

	public Optional<WorkUnit> get(String userId, String date) {
		Optional<WorkUnit> unit = record.stream()
			.filter(u -> u.getEmployeeId().equals(userId) && u.getDate().equals(date))
			.findAny();
		return unit;
	}

	public Optional<WorkUnit> remove(String userId, String date) {
		Optional<WorkUnit> unit = record.stream()
			.filter(u -> u.getEmployeeId().equals(userId) && u.getDate().equals(date))
			.findAny();
		if (unit.isPresent()) record.remove(unit.get());
		return unit;
	}
	
	public Stream<WorkUnit> getStream() {
		return record.stream();
	}
}
