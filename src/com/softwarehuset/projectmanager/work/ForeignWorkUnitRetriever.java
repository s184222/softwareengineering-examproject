package com.softwarehuset.projectmanager.work;

import java.util.Optional;

// Rasmus M. Larsen s184190
public class ForeignWorkUnitRetriever implements WorkUnitRetriever {

	private Record preRecord;
	private Record mainRecord;

	public ForeignWorkUnitRetriever (Record preRecord, Record mainRecord) {
		this.preRecord = preRecord;
		this.mainRecord = mainRecord;
	}

	@Override
	public WorkUnit retrieve(String userId, String date) {

		Optional<WorkUnit> fresh = preRecord.get(userId, date);
		if (fresh.isPresent()) return fresh.get();

		Optional<WorkUnit> edit = mainRecord.get(userId, date);
		if (edit.isPresent()) return edit.get();

		/*
		 * We could just return edit.get() directly here instead (without the if-statement) and get 100% code coverage in this class.
		 * We intentionally don't do it, because if we were to refactor (and introduce a bug) and the system suddenly breaks
		 * then it would be easier for the programmer to understand how to fix the problem (and the intended behavior), due to the error message, 
		 * instead of a null-pointer exception
		 */
		throw new IllegalStateException("internal bug, should never happen!");
	}

	@Override
	public WorkUnit retrieveAndRemove(String userId, String date) {

		Optional<WorkUnit> fresh = preRecord.remove(userId, date);
		if (fresh.isPresent()) return fresh.get();

		Optional<WorkUnit> edit = mainRecord.remove(userId, date);
		if (edit.isPresent()) return edit.get();

		/*
		 * We could just return edit.get() directly here instead (without the if-statement) and get 100% code coverage in this class.
		 * We intentionally don't do it, because if we were to refactor (and introduce a bug) and the system suddenly breaks
		 * then it would be easier for the programmer to understand how to fix the problem (and the intended behavior), due to the error message, 
		 * instead of a null-pointer exception
		 */
		throw new IllegalStateException("internal bug, should never happen!");
	}

}
