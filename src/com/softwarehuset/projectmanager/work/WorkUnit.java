package com.softwarehuset.projectmanager.work;

import com.softwarehuset.projectmanager.exceptions.InvalidHourException;

// Gustav Gr�nvold s184217
public class WorkUnit {

	private static final int uninitializedHours = -1;

	private final String date;
	private final String employeeId;
	private double hours = uninitializedHours; // 9.5 hrs is ok!

	public WorkUnit (String employeeId, String date) {
		this.employeeId = employeeId;
		this.date = date;
	}

	public double getHours() {
		return hours;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setHours(double hours) throws InvalidHourException {
		if (hours < 0 || 24 < hours) throw new InvalidHourException("invalid number of hours");
		this.hours = hours;
	}

	public String getDate() {
		return date;
	}
}
