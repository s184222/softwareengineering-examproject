package com.softwarehuset.projectmanager.work;

import java.util.Optional;

import com.softwarehuset.projectmanager.exceptions.InvalidHourException;

// Rasmus M. Larsen s184190
public class WorkRegistry {

	private WorkUnitRetrieverPolicy policy;
	private WorkUnitRetriever workUnitRetriever;

	public WorkRegistry (WorkUnitRetrieverPolicy policy) {
		this.policy = policy;
	}

	public void queue(String date, String userId, Record record) {
		Optional<WorkUnit> unit = record.get(userId, date);	// no duplicates in the record
		if (!unit.isPresent()) record.register(new WorkUnit(userId, date));
	}

	public void register(double hours, String date, String userId, Record record) throws InvalidHourException {
		workUnitRetriever = policy.pick();

		// do not remove until we are sure that the hours are valid!
		WorkUnit unit = workUnitRetriever.retrieve(userId, date);
		unit.setHours(hours);
		workUnitRetriever.retrieveAndRemove(userId, date);

		record.register(unit);
	}

	public double getWorkHours(String id, String date, Record record) {
		Optional<WorkUnit> work = record.get(id, date);
		if (!work.isPresent()) return 0;
		return work.get().getHours();
	}

	public int getWorkRegistrations(String employeeId, Record record) {
		return (int) record.getStream()
			.filter(u -> u.getEmployeeId().equals(employeeId))
			.count();
	}

}