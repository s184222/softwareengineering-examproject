package com.softwarehuset.projectmanager.work;

// Rasmus M. Larsen s184190
public interface WorkUnitRetriever {

	public WorkUnit retrieve(String userId, String date);

	public WorkUnit retrieveAndRemove(String userId, String date);

}
