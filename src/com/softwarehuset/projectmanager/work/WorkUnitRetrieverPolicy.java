package com.softwarehuset.projectmanager.work;

// Rasmus M. Larsen s184190
public interface WorkUnitRetrieverPolicy {
	public WorkUnitRetriever pick();
}
