package com.softwarehuset.projectmanager.work;

import java.util.Optional;

// Rasmus M. Larsen s184190
public class AssignedEmployeeWorkUnitRetriever implements WorkUnitRetriever {

	private Record mainRecord;

	public AssignedEmployeeWorkUnitRetriever (Record mainRecord) {
		this.mainRecord = mainRecord;
	}

	@Override
	public WorkUnit retrieve(String userId, String date) {
		Optional<WorkUnit> unit = mainRecord.get(userId, date);

		if (unit.isPresent()) {
			return unit.get();
		}

		return new WorkUnit(userId, date);
	}

	@Override
	public WorkUnit retrieveAndRemove(String userId, String date) {
		Optional<WorkUnit> unit = mainRecord.remove(userId, date);

		if (unit.isPresent()) {
			return unit.get();
		}

		return new WorkUnit(userId, date);
	}

}
