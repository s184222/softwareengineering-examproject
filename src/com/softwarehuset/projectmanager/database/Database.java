package com.softwarehuset.projectmanager.database;

import java.util.stream.Stream;

import com.softwarehuset.projectmanager.project.Journal;
import com.softwarehuset.projectmanager.project.Project;
import com.softwarehuset.projectmanager.user.User;

// Gustav Gr�nvold s184217
public interface Database {

	public User fetchUser(String id);

	public void addUser(User user);

	// user is read-only, so no need to update it
	// public void updateUser(User user);

	public Stream<User> getAllUsers();

	public Project fetchProject(String id);

	public void addProject(Project project);

	public void updateProject(Project project);

	public Journal fetchJournal(String employeeId);

	public void addJournal(String employeeId, Journal journal);

	public void updateJournal(Journal journal);

	public Stream<Project> getAllProjects();

}
