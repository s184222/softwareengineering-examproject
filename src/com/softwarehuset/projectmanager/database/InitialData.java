package com.softwarehuset.projectmanager.database;

import java.util.Map;

import com.softwarehuset.projectmanager.project.Journal;
import com.softwarehuset.projectmanager.project.Project;
import com.softwarehuset.projectmanager.user.User;

// Gustav Gr�nvold s184217
public interface InitialData {

	public Map<String, User> getUsers();

	public Map<String, Project> getProjects();

	public Map<String, Journal> getJournals();
}
