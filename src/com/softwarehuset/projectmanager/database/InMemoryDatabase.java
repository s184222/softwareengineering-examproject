package com.softwarehuset.projectmanager.database;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Stream;

import com.softwarehuset.projectmanager.project.Journal;
import com.softwarehuset.projectmanager.project.Project;
import com.softwarehuset.projectmanager.user.User;

// Rasmus M. Larsen s184190
public class InMemoryDatabase implements Database {

	private Map<String, User> users = new LinkedHashMap<String, User>();
	private Map<String, Project> projects = new LinkedHashMap<String, Project>();
	private Map<String, Journal> journals = new LinkedHashMap<String, Journal>();

	public InMemoryDatabase (InitialData data) {
		users.putAll(data.getUsers());
		projects.putAll(data.getProjects());
		journals.putAll(data.getJournals());
	}

	@Override
	public User fetchUser(String id) {
		return users.get(id);
	}

	@Override
	public void addUser(User user) {
		users.put(user.getId(), user);
	}

	@Override
	public Stream<User> getAllUsers() {
		return users.values().stream();
	}

	@Override
	public Project fetchProject(String id) {
		return projects.get(id);
	}

	@Override
	public void addProject(Project project) {
		projects.put(project.getId(), project);
	}

	@Override
	public Journal fetchJournal(String userId) {
		return journals.get(userId);
	}

	@Override
	public void addJournal(String employeeId, Journal journal) {
		journals.put(employeeId, journal);
	}

	@Override
	public Stream<Project> getAllProjects() {
		return projects.values().stream();
	}

	@Override
	public void updateProject(Project project) {}

	@Override
	public void updateJournal(Journal journal) {}

}
