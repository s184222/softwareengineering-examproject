package com.softwarehuset.projectmanager.database;

import java.util.HashMap;
import java.util.Map;

import com.softwarehuset.projectmanager.project.Journal;
import com.softwarehuset.projectmanager.project.Project;
import com.softwarehuset.projectmanager.user.Admin;
import com.softwarehuset.projectmanager.user.User;

// Tobias H. M�ller s184217
public class DefaultInitialData implements InitialData {

	private Map<String, User> users = new HashMap<String, User>();
	private Map<String, Project> projects = new HashMap<String, Project>();
	private Map<String, Journal> journals = new HashMap<String, Journal>();

	public DefaultInitialData () {
		createAdmin();
	}

	private void createAdmin() {
		Admin admin = new Admin();
		users.put(admin.getId(), admin);
	}

	@Override
	public Map<String, User> getUsers() {
		return users;
	}

	@Override
	public Map<String, Project> getProjects() {
		return projects;
	}

	@Override
	public Map<String, Journal> getJournals() {
		return journals;
	}

}
