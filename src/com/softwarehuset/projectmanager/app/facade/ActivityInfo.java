package com.softwarehuset.projectmanager.app.facade;

// Tobias H. M�ller s184217
public abstract class ActivityInfo {

	private final String name;

	private final int startYear;
	private final int endYear;
	private final int startWeek;
	private final int endWeek;

	public ActivityInfo (String name, int startYear, int endYear, int startWeek, int endWeek) {
		this.name = name;

		this.startYear = startYear;
		this.endYear = endYear;
		this.startWeek = startWeek;
		this.endWeek = endWeek;
	}

	public String getName() {
		return name;
	}

	public int getStartYear() {
		return startYear;
	}

	public int getEndYear() {
		return endYear;
	}

	public int getStartWeek() {
		return startWeek;
	}

	public int getEndWeek() {
		return endWeek;
	}
}
