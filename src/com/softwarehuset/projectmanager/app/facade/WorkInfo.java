package com.softwarehuset.projectmanager.app.facade;

// Tobias H. M�ller s184217
public class WorkInfo {
	
	private final String date;
	private final String employeeId;
	private final double hours;
	
	public WorkInfo (String date, String employeeId, double hours) {
		this.date = date;
		this.employeeId = employeeId;
		this.hours = hours;
	}

	public String getDate() {
		return date;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public double getHours() {
		return hours;
	}

}
