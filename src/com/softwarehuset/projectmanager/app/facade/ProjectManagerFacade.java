package com.softwarehuset.projectmanager.app.facade;

import java.util.List;
import java.util.Set;

import com.softwarehuset.projectmanager.exceptions.AssignableException;
import com.softwarehuset.projectmanager.exceptions.InvalidEmployeeException;
import com.softwarehuset.projectmanager.exceptions.InvalidHourException;
import com.softwarehuset.projectmanager.exceptions.InvalidSpanException;
import com.softwarehuset.projectmanager.exceptions.NameUnavailableException;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.projectmanager.exceptions.ProjectIdGenerationException;

// Christian M. Fuglsang s184222
public interface ProjectManagerFacade {

	public void signIn(String userId) throws InvalidEmployeeException;

	public void signOut();

	public String createProject(String name) throws PermissionDeniedException, ProjectIdGenerationException;

	public void selectProject(String id);

	public void setProjectManager(String userId) throws InvalidEmployeeException, PermissionDeniedException;

	public void createActivity(String name) throws NameUnavailableException, PermissionDeniedException;

	public void removeActivityInProject(String name) throws PermissionDeniedException, NameUnavailableException;

	public void selectActivity(String name);

	public void assignEmployee(String employeeId) throws PermissionDeniedException, AssignableException, InvalidEmployeeException;

	public void setActivityStart(int week, int year) throws PermissionDeniedException, InvalidSpanException;

	public void setActivityEnd(int week, int year) throws PermissionDeniedException, InvalidSpanException;

	public void registerWork(double hours, String date) throws PermissionDeniedException, InvalidHourException;

	public void requestHelp(String employeeId) throws InvalidEmployeeException, PermissionDeniedException, InvalidHourException;

	public void selectJournal();

	public void unselect();

	public boolean isAdmin();

	public void setExpectedHours(int hours) throws PermissionDeniedException, InvalidHourException;

	public int getExpectedHours();

	public List<String> getAssignedEmployees();

	public boolean isActivitySelected();

	public Set<String> getActivityNamesOfSelection();

	public List<String> getAllProjectIds();

	public List<ProjectInfo> getAllProjectInfos();

	public ProjectInfo getProjectInfoFromId(String projectId);

	public List<String> getAllUserIds();

	public List<ProjectActivityInfo> getAllProjectActivityInfos(String projectId);

	public ProjectActivityInfo getProjectActivityInfo(String name);

	public String getUserId();

	public List<String> getAvailableEmployeeIdsToday();

	public List<String> getAllEmployeeIds();

	public PersonalActivityInfo getPersonalActivityInfo(String name);

	public List<PersonalActivityInfo> getAllPersonalActivityInfos();

	public WorkInfo getWorkInfo(String employeeId, String date);

	public List<WorkInfo> getWorkInfos();

}
