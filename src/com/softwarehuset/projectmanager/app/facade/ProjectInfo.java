package com.softwarehuset.projectmanager.app.facade;

// Christian M. Fuglsang s184222
public class ProjectInfo {
	
	private final String name;
	private final String projectId;
	private final String projectManagerId;
	
	private final int numberOfActivities;
	
	public ProjectInfo(String name, String projectId, String projectManagerId, int numberOfActivities) {
		this.name = name;
		this.projectId = projectId;
		this.projectManagerId = projectManagerId;
		this.numberOfActivities = numberOfActivities;
	}
	
	public String getName() {
		return name;
	}

	public String getProjectId() {
		return projectId;
	}
	
	public String getProjectManagerId() {
		return projectManagerId;
	}
	
	public int getNumberOfActivities() {
		return numberOfActivities;
	}
}
