package com.softwarehuset.projectmanager.app.facade;

// Tobias H. M�ller s184217
public class PersonalActivityInfo extends ActivityInfo {

	public PersonalActivityInfo (String name, int startYear, int endYear, int startWeek, int endWeek) {
		super(name, startYear, endYear, startWeek, endWeek);
	}
}
