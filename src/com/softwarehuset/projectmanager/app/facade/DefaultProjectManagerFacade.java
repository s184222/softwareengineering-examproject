package com.softwarehuset.projectmanager.app.facade;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.softwarehuset.projectmanager.app.ProjectManagerApp;
import com.softwarehuset.projectmanager.exceptions.AssignableException;
import com.softwarehuset.projectmanager.exceptions.InvalidEmployeeException;
import com.softwarehuset.projectmanager.exceptions.InvalidHourException;
import com.softwarehuset.projectmanager.exceptions.InvalidSpanException;
import com.softwarehuset.projectmanager.exceptions.NameUnavailableException;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.projectmanager.exceptions.ProjectIdGenerationException;
import com.softwarehuset.projectmanager.project.Journal;
import com.softwarehuset.projectmanager.project.PersonalActivity;
import com.softwarehuset.projectmanager.project.Project;
import com.softwarehuset.projectmanager.project.ProjectActivity;
import com.softwarehuset.projectmanager.project.WeekYearSpan;
import com.softwarehuset.projectmanager.selection.JournalSelection;
import com.softwarehuset.projectmanager.selection.ProjectSelection;
import com.softwarehuset.projectmanager.selection.SelectionType;
import com.softwarehuset.projectmanager.work.WorkUnit;

// Rasmus M. Larsen s184190
public class DefaultProjectManagerFacade implements ProjectManagerFacade {

	private ProjectManagerApp app;

	public DefaultProjectManagerFacade (ProjectManagerApp app) {
		this.app = app;
	}

	@Override
	public void signIn(String userId) throws InvalidEmployeeException {
		app.getSession().signin(userId);
	}

	@Override
	public void signOut() {
		app.getSession().signout();
	}

	@Override
	public String createProject(String name) throws PermissionDeniedException, ProjectIdGenerationException {
		return app.createProject(name).getId();
	}

	@Override
	public void selectProject(String id) {
		app.selectProject(id);
	}

	@Override
	public void setProjectManager(String userId) throws InvalidEmployeeException, PermissionDeniedException {
		app.setProjectManager(userId);
	}

	@Override
	public void createActivity(String name) throws NameUnavailableException, PermissionDeniedException {
		app.createActivity(name);
	}

	@Override
	public void removeActivityInProject(String name) throws PermissionDeniedException, NameUnavailableException {
		app.removeActivityInProject(name);
	}

	@Override
	public void selectActivity(String name) {
		app.selectActivity(name);
	}

	@Override
	public void assignEmployee(String employeeId) throws PermissionDeniedException, AssignableException, InvalidEmployeeException {
		app.assignEmployee(employeeId);
	}

	@Override
	public void setActivityStart(int week, int year) throws PermissionDeniedException, InvalidSpanException {
		app.setStartForSelectedActivity(week, year);
	}

	@Override
	public void setActivityEnd(int week, int year) throws PermissionDeniedException, InvalidSpanException {
		app.setEndForSelectedActivity(week, year);
	}

	@Override
	public void registerWork(double hours, String date) throws PermissionDeniedException, InvalidHourException {
		app.registerWork(hours, date);
	}

	@Override
	public void requestHelp(String employeeId) throws InvalidEmployeeException, PermissionDeniedException, InvalidHourException {
		app.requestHelp(employeeId);
	}

	@Override
	public void selectJournal() {
		app.selectJournal();
	}

	@Override
	public void unselect() {
		app.getSession().clearSelection();
	}

	@Override
	public boolean isActivitySelected() {
		if (app.getSession().getSelectionUnion().getType() == SelectionType.project) {
			ProjectSelection selection = app.getSession().getSelectionUnion().get();
			return selection.getActivity() != null;
		}

		if (app.getSession().getSelectionUnion().getType() == SelectionType.journal) {
			JournalSelection selection = app.getSession().getSelectionUnion().get();
			return selection.getActivity() != null;
		}

		return false;
	}

	@Override
	public Set<String> getActivityNamesOfSelection() {
		if (app.getSession().getSelectionUnion().getType() == SelectionType.project) {
			Project project = app.getProject();
			return Collections.unmodifiableSet(project.getActivityNames());
		}

		if (app.getSession().getSelectionUnion().getType() == SelectionType.journal) {
			Journal journal = app.getJournal();
			return Collections.unmodifiableSet(journal.getActivityNames());
		}

		throw new IllegalStateException("nothing selected");
	}

	@Override
	public List<String> getAllProjectIds() {
		return Collections.unmodifiableList(app.getDatabase().getAllProjects().map(u -> u.getId()).collect(Collectors.toList()));
	}

	@Override
	public List<ProjectInfo> getAllProjectInfos() {
		return Collections.unmodifiableList(
			app.getDatabase().getAllProjects()
				.map(this::getProjectInfo)
				.collect(Collectors.toList()));
	}

	@Override
	public ProjectInfo getProjectInfoFromId(String projectId) {
		return getProjectInfo(app.getDatabase().fetchProject(projectId));
	}

	@Override
	public List<String> getAllEmployeeIds() {
		return Collections.unmodifiableList(
			app.getDatabase().getAllUsers()
				.filter(u -> !u.isAdmin())
				.map(u -> u.getId())
				.collect(Collectors.toList()));
	}

	@Override
	public List<String> getAllUserIds() {
		return Collections.unmodifiableList(
			app.getDatabase().getAllUsers()
				.map(u -> u.getId())
				.collect(Collectors.toList()));
	}

	private ProjectInfo getProjectInfo(Project project) {
		return new ProjectInfo(project.getName(),
			project.getId(),
			project.getProjectManagerId(),
			project.getNumberOfActivities());
	}

	@Override
	public List<ProjectActivityInfo> getAllProjectActivityInfos(String projectId) {
		Project project = app.getDatabase().fetchProject(projectId);
		return Collections.unmodifiableList(
			project.getActivityNames().stream()
				.map(project::getActivity)
				.map(this::getProjectActivityInfo)
				.collect(Collectors.toList()));
	}

	@Override
	public ProjectActivityInfo getProjectActivityInfo(String name) {
		return getProjectActivityInfo(app.getProject().getActivity(name));
	}

	private ProjectActivityInfo getProjectActivityInfo(ProjectActivity activity) {
		WeekYearSpan span = activity.getSpan();
		return new ProjectActivityInfo(activity.getName(),
			span.getYearStart(),
			span.getYearEnd(),
			span.getWeekStart(),
			span.getWeekEnd(),
			activity.getExpectedHours(),
			activity.getNumberAssigned());
	}

	@Override
	public List<PersonalActivityInfo> getAllPersonalActivityInfos() {
		Journal journal = app.getJournal();
		return Collections.unmodifiableList(
			journal.getActivityNames().stream()
				.map(journal::getActivity)
				.map(this::getPersonalActivityInfo)
				.collect(Collectors.toList()));
	}

	@Override
	public PersonalActivityInfo getPersonalActivityInfo(String name) {
		return getPersonalActivityInfo(app.getJournal().getActivity(name));
	}

	private PersonalActivityInfo getPersonalActivityInfo(PersonalActivity activity) {
		WeekYearSpan span = activity.getSpan();
		return new PersonalActivityInfo(activity.getName(),
			span.getYearStart(),
			span.getYearEnd(),
			span.getWeekStart(),
			span.getWeekEnd());
	}

	@Override
	public List<WorkInfo> getWorkInfos() {
		ProjectSelection selection = app.getSession().getSelectionUnion().get();
		ProjectActivity activity = selection.getActivity();
		return Collections.unmodifiableList(
			activity.getMainRecord().getStream()
				.map(this::getWorkInfo)
				.collect(Collectors.toList()));
	}

	private WorkInfo getWorkInfo(WorkUnit unit) {
		return new WorkInfo(unit.getDate(), unit.getEmployeeId(), unit.getHours());
	}

	@Override
	public WorkInfo getWorkInfo(String employeeId, String date) {
		ProjectSelection selection = app.getSession().getSelectionUnion().get();
		ProjectActivity activity = selection.getActivity();
		return getWorkInfo(activity.getMainRecord().get(employeeId, date).get());
	}

	@Override
	public String getUserId() {
		return app.getSession().getUser().getId();
	}

	@Override
	public List<String> getAvailableEmployeeIdsToday() {
		return app.getAvailableEmployeesToday()
			.stream()
			.map(u -> u.getId())
			.filter(id -> !getUserId().equals(id))
			.collect(Collectors.toList());
	}

	/* *********** TESTING (mocking) only! *********** */
	public void attach(ProjectManagerApp app) {
		this.app = app;
	}

	public boolean isEmployeeAssignedToSelectedActivity(String employeeId) {
		ProjectSelection selection = app.getSession().getSelectionUnion().get();
		return selection.getActivity().hasEmployee(employeeId);
	}

	public String getProjectManagerId() {
		ProjectSelection selection = app.getSession().getSelectionUnion().get();
		return selection.getProject().getProjectManagerId();
	}

	public double getWorkHours(String date) {
		return app.getWorkHours(date);
	}

	public boolean existsUsers(String employeeId) {
		return app.existsUsers(employeeId);
	}

	public WeekYearSpan getActivitySpan() {
		if (app.getSession().getSelectionUnion().getType() == SelectionType.project) {
			ProjectSelection selection = app.getSession().getSelectionUnion().get();
			return selection.getActivity().getSpan();
		}

		if (app.getSession().getSelectionUnion().getType() == SelectionType.journal) {
			JournalSelection selection = app.getSession().getSelectionUnion().get();
			return selection.getActivity().getSpan();
		}

		throw new IllegalStateException("nothing selected");
	}

	public int countPendingWorkRegistrations(String employeeId) {
		return app.countPendingWorkRegistrations(employeeId);
	}

	public boolean isSignedIn() {
		return app.getSession().getUser() != null;
	}

	@Override
	public boolean isAdmin() {
		return app.getSession().getUser().isAdmin();
	}

	@Override
	public void setExpectedHours(int hours) throws PermissionDeniedException, InvalidHourException {
		app.setExpectedHoursOfSelected(hours);
	}

	@Override
	public int getExpectedHours() {
		return app.getExpectedHoursOfSelected();
	}

	@Override
	public List<String> getAssignedEmployees() {
		ProjectSelection selection = app.getSession().getSelectionUnion().get();
		return selection.getActivity().getAssignedEmployees();
	}
}
