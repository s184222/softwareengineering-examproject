package com.softwarehuset.projectmanager.app.facade;

// Gustav Gr�nvold s184217
public class ProjectActivityInfo extends ActivityInfo {

	private final int expectedHours;
	private final int numberAssigned;

	public ProjectActivityInfo(String name, int startYear, int endYear, int startWeek, int endWeek, int expectedHours, int numberAssigned) {
		super(name, startYear, endYear, startWeek, endWeek);
		
		this.expectedHours = expectedHours;
		this.numberAssigned = numberAssigned;
	}

	public int getExpectedHours() {
		return expectedHours;
	}
	
	public int getNumberAssigned() {
		return numberAssigned;
	}
}
