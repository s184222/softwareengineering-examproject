package com.softwarehuset.projectmanager.app;

import java.util.ArrayList;
import java.util.List;

import com.softwarehuset.projectmanager.database.Database;
import com.softwarehuset.projectmanager.date.DateServer;
import com.softwarehuset.projectmanager.exceptions.AssignableException;
import com.softwarehuset.projectmanager.exceptions.InvalidEmployeeException;
import com.softwarehuset.projectmanager.exceptions.InvalidHourException;
import com.softwarehuset.projectmanager.exceptions.InvalidSpanException;
import com.softwarehuset.projectmanager.exceptions.NameUnavailableException;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.projectmanager.exceptions.ProjectIdGenerationException;
import com.softwarehuset.projectmanager.project.Journal;
import com.softwarehuset.projectmanager.project.PersonalActivity;
import com.softwarehuset.projectmanager.project.Project;
import com.softwarehuset.projectmanager.project.ProjectActivity;
import com.softwarehuset.projectmanager.project.ProjectIdGenerator;
import com.softwarehuset.projectmanager.security.SecurityProtocol;
import com.softwarehuset.projectmanager.selection.JournalSelection;
import com.softwarehuset.projectmanager.selection.ProjectSelection;
import com.softwarehuset.projectmanager.selection.SelectionType;
import com.softwarehuset.projectmanager.selection.SelectionUnion;
import com.softwarehuset.projectmanager.session.Session;
import com.softwarehuset.projectmanager.user.Employee;
import com.softwarehuset.projectmanager.user.User;
import com.softwarehuset.projectmanager.work.WorkRegistry;

// Rasmus M. Larsen s184190
public class ProjectManagerApp {

	private DateServer dateServer;
	private ProjectIdGenerator projectIdGenerator;

	private Database database;
	private SecurityProtocol security;
	private Session session;
	private WorkRegistry workRegistry;

	// Used for testing. the tests do not know how to inject the pmAppConfig,
	// because it is a interface
	public ProjectManagerApp () {
		this(new DefaultProjectManagerAppConfiguration());
	}

	public ProjectManagerApp (ProjectManagerAppConfiguration config) {
		dateServer = config.getDateServer();
		database = config.getDatabase();
		security = config.getSecurityProtocol();
		session = config.getSession();
		workRegistry = config.getWorkRegistry();
		projectIdGenerator = config.getProjectIdGenerator();
	}

	public boolean existsUsers(String userId) {
		return database.fetchUser(userId) != null;
	}

	public void addEmployee(Employee employee) {
		/*
		assert employee != null;
		assert database != null;
		assert database.fetchUser(employee.getId()) == null;
		*/

		database.addUser(employee);
		database.addJournal(employee.getId(), new Journal());

		/*
		assert database.fetchUser(employee.getId()) != null;
		assert database.fetchJournal(employee.getId()) != null;
		*/
	}

	public Project createProject(String name) throws PermissionDeniedException, ProjectIdGenerationException {
		if (!security.isAuthorizedForProjectCreation(session.getUser())) {
			throw new PermissionDeniedException("not admin");
		}

		String id = projectIdGenerator.generateProjectId(dateServer);
		Project project = new Project(id, name);
		database.addProject(project);
		return project;
	}

	public void setDateServer(DateServer dateServer) {
		this.dateServer = dateServer;
	}

	public DateServer getDateserver() {
		return dateServer;
	}

	public void selectProject(String id) {
		ProjectSelection selection = new ProjectSelection();
		selection.setProject(database.fetchProject(id));
		session.setSeletionUnion(SelectionUnion.createProject(selection));
	}

	public void setProjectManager(String userId) throws InvalidEmployeeException, PermissionDeniedException {
		if (!security.isAuthorizedToSetProjectManager(session.getUser())) {
			throw new PermissionDeniedException("not admin");
		}

		if (database.fetchUser(userId) == null) {
			throw new InvalidEmployeeException("employee doesn't exist");
		}

		ProjectSelection selection = session.getSelectionUnion().get();
		selection.getProject().setProjectManager(userId);

		database.updateProject(selection.getProject());
	}

	public void createActivity(String activityName) throws NameUnavailableException, PermissionDeniedException {

		if (session.getSelectionUnion().getType() == SelectionType.journal) {
			addActivityToJournal(activityName);
		}

		if (session.getSelectionUnion().getType() == SelectionType.project) {
			addActivityToProject(activityName);
		}
	}

	private void addActivityToProject(String activityName) throws PermissionDeniedException, NameUnavailableException {
		ProjectSelection selection = session.getSelectionUnion().get();

		if (!security.isAuthorizedToCreateActivity(session.getUser(), selection.getProject())) {
			throw new PermissionDeniedException("not project manager");
		}

		selection.getProject().addActivity(activityName);

		database.updateProject(selection.getProject());
	}

	private void addActivityToJournal(String activityName) throws PermissionDeniedException, NameUnavailableException {
		if (!security.isAuthorizedToCreatePersonalActivity(session.getUser())) {
			throw new PermissionDeniedException("a fictive person cannot create personal activity");
		}

		JournalSelection selection = session.getSelectionUnion().get();
		selection.getJournal().register(new PersonalActivity(activityName));

		database.updateJournal(selection.getJournal());
	}

	public void removeActivityInProject(String activityName) throws PermissionDeniedException, NameUnavailableException {
		ProjectSelection selection = session.getSelectionUnion().get();

		if (!security.isAuthorizedToDeleteActivity(session.getUser(), selection.getProject())) {
			throw new PermissionDeniedException("not project manager");
		}

		selection.getProject().removeActivity(activityName);
		
		database.updateProject(selection.getProject());
	}

	public void setProjectIdGenerator(ProjectIdGenerator projectIdGenerator) {
		this.projectIdGenerator = projectIdGenerator;
	}

	public void selectActivity(String activityName) {
		/*
		assert activityName != null;
		assert session != null;
		assert session.getSelectionUnion() != null;
		assert session.getSelectionUnion().getType() != null;
		assert session.getSelectionUnion().getType() != SelectionType.none;
		*/

		SelectionUnion union = session.getSelectionUnion();
		if (union.getType() == SelectionType.journal) { // 1
			JournalSelection selection = session.getSelectionUnion().get();
			selection.setActivity(selection.getJournal().getActivity(activityName));

			// test for reference!
			// assert selection.getActivity() == selection.getJournal().getActivity(activityName);
		} else if (union.getType() == SelectionType.project) { // 2
			ProjectSelection selection = session.getSelectionUnion().get();
			selection.setActivity(selection.getProject().getActivity(activityName));

			// test for reference!
			// assert selection.getActivity() == selection.getProject().getActivity(activityName);
		}
	}

	public void assignEmployee(String employeeId) throws PermissionDeniedException, AssignableException, InvalidEmployeeException {
		/*
		assert employeeId != null;
		assert database != null;
		assert session != null;
		assert session.getSelectionUnion() != null;
		assert session.getSelectionUnion().getType() == SelectionType.project;
		assert session.getUser() != null;
		assert security != null;	
		*/

		ProjectSelection selection = session.getSelectionUnion().get();

		if (!security.isAuthorizedToAssignEmployeeToActivity(session.getUser(), selection.getProject())) { // 1
			throw new PermissionDeniedException("not project manager");
		}
		
		if (database.fetchUser(employeeId) == null) {	// 2
			throw new InvalidEmployeeException("employee doesn't exist");
		}

		// 3 (exception with getActivity().assign();
		// 4 no exception
		selection.getActivity().assign(employeeId);
		
		database.updateProject(selection.getProject());

		/*
		assert selection.getActivity().hasEmployee(employeeId);
		*/
	}

	public int getExpectedHoursOfSelected() {
		ProjectSelection selection = session.getSelectionUnion().get();
		return selection.getActivity().getExpectedHours();
	}

	public void setExpectedHoursOfSelected(int hours) throws PermissionDeniedException, InvalidHourException {
		ProjectSelection selection = session.getSelectionUnion().get();
		if (!security.isAuthorizedToSetExpectedHours(session.getUser(), selection.getProject())) {
			throw new PermissionDeniedException("not project manager");
		}

		selection.getActivity().setExpectedHours(hours);
		
		database.updateProject(selection.getProject());
	}

	public void setStartForSelectedActivity(int week, int year) throws PermissionDeniedException, InvalidSpanException {
		if (session.getSelectionUnion().getType() == SelectionType.project) {
			ProjectSelection selection = session.getSelectionUnion().get();
			if (!security.isAuthorizedToSetActivitySpan(session.getUser(), selection.getProject())) {
				throw new PermissionDeniedException("not project manager");
			}

			selection.getActivity().getSpan().setStart(week, year);
			
			database.updateProject(selection.getProject());
		}

		if (session.getSelectionUnion().getType() == SelectionType.journal) {
			JournalSelection selection = session.getSelectionUnion().get();
			selection.getActivity().getSpan().setStart(week, year);
			
			database.updateJournal(selection.getJournal());
		}

	}

	public void setEndForSelectedActivity(int week, int year) throws PermissionDeniedException, InvalidSpanException {
		if (session.getSelectionUnion().getType() == SelectionType.project) {
			ProjectSelection selection = session.getSelectionUnion().get();
			if (!security.isAuthorizedToSetActivitySpan(session.getUser(), selection.getProject())) {
				throw new PermissionDeniedException("not project manager");
			}

			selection.getActivity().getSpan().setEnd(week, year);
			
			database.updateProject(selection.getProject());
		}

		if (session.getSelectionUnion().getType() == SelectionType.journal) {
			JournalSelection selection = session.getSelectionUnion().get();
			selection.getActivity().getSpan().setEnd(week, year);
			database.updateJournal(selection.getJournal());
			
			database.updateJournal(selection.getJournal());
		}

	}

	public void registerWork(double hours, String date) throws PermissionDeniedException, InvalidHourException {
		ProjectSelection selection = session.getSelectionUnion().get();

		if (!canRegisterWork(session.getUser(), date, selection.getActivity())) {
			throw new PermissionDeniedException("found no foreign work");
		}

		workRegistry.register(hours, date, session.getUser().getId(), selection.getActivity().getMainRecord());
		
		database.updateProject(selection.getProject());
	}

	private boolean canRegisterWork(User user, String date, ProjectActivity selectedActivity) {
		return security.isAuthorizedToRegistrateWork(user, selectedActivity)
			|| security.isAuthorizedToEditForeignWorkRegistration(user, date, selectedActivity)
			|| security.isAuthorizedToRegistrateForeignWork(user, date, selectedActivity);
	}

	public int countPendingWorkRegistrations(String employeeId) {
		ProjectSelection selection = session.getSelectionUnion().get();
		return workRegistry.getWorkRegistrations(employeeId, selection.getActivity().getPreRecord());
	}

	public double getWorkHours(String date) {
		ProjectSelection selection = session.getSelectionUnion().get();
		return workRegistry.getWorkHours(session.getUser().getId(), date, selection.getActivity().getMainRecord());
	}

	public List<User> getAvailableEmployees(int week, int year) {
		User[] users = database.getAllUsers().toArray(value -> new User[value]);
		List<User> availableUsers = new ArrayList<User>();
		for (User user : users) {
			if (user.isAdmin()) continue;

			Journal journal = database.fetchJournal(user.getId());
			if (!journal.occupied(week, year)) availableUsers.add(user);
		}

		return availableUsers;
	}

	private boolean isAvailableToday(String employeeId) {
		return getAvailableEmployeesToday().contains(database.fetchUser(employeeId));
	}

	public List<User> getAvailableEmployeesToday() {
		return getAvailableEmployees(dateServer.getWeek(), dateServer.getYear());
	}

	public void requestHelp(String employeeId) throws InvalidEmployeeException, PermissionDeniedException, InvalidHourException {
		/*
		assert employeeId != null;
		assert session != null;
		assert session.getSelectionUnion() != null;
		assert session.getSelectionUnion().get() != null;
		assert session.getSelectionUnion().getType() != null;
		assert session.getSelectionUnion().getType() == SelectionType.project;
		assert security != null;
		assert database != null;
		assert workRegistry != null;
		assert dateServer != null;
		*/

		ProjectSelection selection = session.getSelectionUnion().get();

		if (!security.isAuthorizedToRequestHelp(session.getUser(), selection.getActivity())) { // 1
			throw new PermissionDeniedException("not assigned to selected activity");
		}

		if (database.fetchUser(employeeId) == null) { // 2
			throw new InvalidEmployeeException("employee doesn't exist");
		}

		if (selection.getActivity().hasEmployee(employeeId)) { // 3
			throw new InvalidEmployeeException("already assigned to activity");
		}
		
		if (!isAvailableToday(employeeId)) { // 4
			throw new InvalidEmployeeException("employee is busy");
		}

		// 5
		workRegistry.queue(dateServer.getDate(), employeeId, selection.getActivity().getPreRecord());
		database.updateProject(selection.getProject());

		/*
		assert workRegistry.getWorkRegistrations(employeeId, selection.getActivity().getPreRecord()) == 1;
		*/
	}

	public Session getSession() {
		return session;
	}

	public void selectJournal() {
		JournalSelection selection = new JournalSelection();
		selection.setJournal(database.fetchJournal(session.getUser().getId()));
		SelectionUnion union = SelectionUnion.createJournal(selection);
		session.setSeletionUnion(union);
	}

	public Journal getJournal() {
		JournalSelection selection = session.getSelectionUnion().get();
		return selection.getJournal();
	}

	public Project getProject() {
		ProjectSelection selection = session.getSelectionUnion().get();
		return selection.getProject();
	}

	public Database getDatabase() {
		return database;
	}

}