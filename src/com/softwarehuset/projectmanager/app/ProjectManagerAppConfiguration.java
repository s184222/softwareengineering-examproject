package com.softwarehuset.projectmanager.app;

import com.softwarehuset.projectmanager.database.Database;
import com.softwarehuset.projectmanager.date.DateServer;
import com.softwarehuset.projectmanager.project.ProjectIdGenerator;
import com.softwarehuset.projectmanager.security.SecurityProtocol;
import com.softwarehuset.projectmanager.session.Session;
import com.softwarehuset.projectmanager.work.WorkRegistry;

// Tobias H. M�ller s184217
public interface ProjectManagerAppConfiguration {

	public Database getDatabase();

	public SecurityProtocol getSecurityProtocol();

	public Session getSession();

	public WorkRegistry getWorkRegistry();

	public ProjectIdGenerator getProjectIdGenerator();
	
	public DateServer getDateServer();

}
