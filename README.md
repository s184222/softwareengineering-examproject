# ProjectManager

## Installation
The only way to currently install the app is to import it as an Eclipse project and
compile the code on your machine.

### Before installation
Before you import the project into Eclipse you need to make sure that the following
requirements / software is installed on your computer. We do not use any dependencies
other than the ones provided with this project, but still you have to make sure that;

* Java SDK and JRE version 1.8 or above is 
  [installed](https://www.oracle.com/technetwork/java/javase/downloads/index.html).
* Eclipse IDE version Luna or above (recommended is Eclipse 2018) is
  [installed](https://www.eclipse.org/downloads/).

### How to import
Open Eclipse in any given workspace and import the project. This can be done by
selecting *File->Import...->General->Existing Projects into Workspace*, checking
the *Select archive* radio-button and browsing the root directory of the zip-file
with the project files and clicking *Finish*. The Eclipse project wizard will
automatically set up the project with the necessary resources.

Alternatively you can view and download the project files using the following link
to the git repository on 
[GitLab](https://gitlab.gbar.dtu.dk/s184222/softwareengineering-examproject).


### How to run
You can either run the project by right-clicking on the project and selecting 
*Run as->JUnit Test* to run the test-scenarios or by selecting *Run as->Java Application*
and selecting the *com.softwarehuset.projectmanager.ui.Main* class for GUI.

When this is done the project should be running without compile-errors.
