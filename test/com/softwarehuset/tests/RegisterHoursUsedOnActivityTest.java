package com.softwarehuset.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import com.softwarehuset.projectmanager.app.ProjectManagerApp;
import com.softwarehuset.projectmanager.app.facade.DefaultProjectManagerFacade;
import com.softwarehuset.projectmanager.app.facade.WorkInfo;
import com.softwarehuset.projectmanager.exceptions.InvalidEmployeeException;
import com.softwarehuset.projectmanager.exceptions.InvalidHourException;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.tests.helpers.SessionHelper;
import com.softwarehuset.tests.holders.ErrorMessageHolder;
import com.softwarehuset.tests.holders.MockDateHolder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

// Rasmus M. Larsen
public class RegisterHoursUsedOnActivityTest {

	private ErrorMessageHolder holder;
	private SessionHelper sessionHelper;
	private MockDateHolder dateHolder;
	private DefaultProjectManagerFacade facade;

	public RegisterHoursUsedOnActivityTest (ProjectManagerApp app, DefaultProjectManagerFacade facade, ErrorMessageHolder holder, SessionHelper sessionHelper) {
		this.facade = facade;
		this.holder = holder;
		this.sessionHelper = sessionHelper;

		facade.attach(app);

		dateHolder = new MockDateHolder(app);
	}

	@When("I set the hours used to {double} on the selected activity on date {string}")
	public void iSetTheHoursUsedToOnTheSelectedActivityOnDate(Double hours, String date) {
		try {
			facade.registerWork(hours, date);
		} catch (PermissionDeniedException | InvalidHourException e) {
			holder.setMessage(e.getMessage());
		}
	}

	@Then("verify the hours I used on the activity {string} on date {string} is {double}")
	public void verifyTheHoursIUsedOnTheActivityOnDateIs(String activity, String date, Double hours) throws InvalidEmployeeException {
		sessionHelper.saveState();

		facade.selectActivity(activity);
		assertEquals(hours, facade.getWorkHours(date), 0);

		sessionHelper.restoreState();
	}

	@Given("the employee {string} has pending foreign registration for the activity with name {string} in the project with id {string} on the date {string}")
	public void theEmployeeHasPendingForeignRegistrationForTheActivityWithNameInTheProjectWithIdOnTheDate(String employeeId, String activityName, String projectId, String date) throws Exception {
		dateHolder.setDate(date);

		sessionHelper.saveState();

		facade.signIn("admin");
		facade.selectProject(projectId);
		facade.selectActivity(activityName);
		facade.requestHelp(employeeId);
		facade.signOut();

		sessionHelper.restoreState();

	}

	@Given("the hours used on the activity with name {string} in the project with id {string} on the date {string} for employee {string} is {double}")
	public void theHoursUsedOnTheActivityWithNameInTheProjectWithIdOnTheDateForEmployeeIs(String activityName, String projectId, String date, String employeeId, Double hours) throws Exception {
		assertTrue(facade.existsUsers(employeeId));

		sessionHelper.saveState();

		facade.signIn("admin");
		facade.selectProject(projectId);
		facade.selectActivity(activityName);

		if (!facade.isEmployeeAssignedToSelectedActivity(employeeId)) {
			dateHolder.setDate(date);
			facade.requestHelp(employeeId);
		}

		facade.signOut();

		facade.signIn(employeeId);
		facade.selectProject(projectId);
		facade.selectActivity(activityName);
		facade.registerWork(hours, date);

		// check that getWorkInfo() works correctly
		assertNotNull(facade.getWorkInfo(employeeId, date));

		// check that getWorkInfos() works correctly
		List<WorkInfo> infos = facade.getWorkInfos();
		Optional<WorkInfo> infoOpt = infos.stream().filter(a -> a.getEmployeeId().equals(facade.getUserId()) && a.getDate().equals(date)).findFirst();
		assertTrue(infoOpt.isPresent());
		WorkInfo info = infoOpt.get();
		assertEquals(hours, info.getHours(), 0);

		facade.signOut();

		sessionHelper.restoreState();
	}
}
