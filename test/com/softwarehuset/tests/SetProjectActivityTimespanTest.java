package com.softwarehuset.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import com.softwarehuset.projectmanager.app.facade.ProjectActivityInfo;
import com.softwarehuset.projectmanager.app.facade.DefaultProjectManagerFacade;
import com.softwarehuset.projectmanager.exceptions.InvalidSpanException;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.tests.helpers.SessionHelper;
import com.softwarehuset.tests.holders.ErrorMessageHolder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

// Rasmus M. Larsen s184190
public class SetProjectActivityTimespanTest {

	private DefaultProjectManagerFacade facade;
	private ErrorMessageHolder holder;
	private SessionHelper sessionHelper;

	public SetProjectActivityTimespanTest (DefaultProjectManagerFacade facade, ErrorMessageHolder holder, SessionHelper sessionHelper) {
		this.facade = facade;
		this.holder = holder;
		this.sessionHelper = sessionHelper;
	}

	@When("I set the start date to week {int} and year {int} for the selected activity")
	public void iSetTheStartDateToWeekAndYearForTheSelectedActivity(Integer week, Integer year) {
		try {
			facade.setActivityStart(week, year);
		} catch (InvalidSpanException | PermissionDeniedException e) {
			holder.setMessage(e.getMessage());
		}
	}

	@Then("verify the start date is week {int} and year {int} for the activity with name {string} in the project with id {string}")
	public void verifyTheStartDateIsWeekAndYearForTheActivityWithNameInTheProjectWithId(Integer week, Integer year, String activityName, String projectId) throws Exception {
		sessionHelper.saveState();

		facade.signIn("admin");
		facade.selectProject(projectId);

		assertFalse(facade.isActivitySelected());

		facade.selectActivity(activityName);
		
		assertNotNull(facade.getProjectActivityInfo(activityName));

		// 1. check activity selected is correct
		assertTrue(facade.isActivitySelected());
		assertEquals(week.intValue(), facade.getActivitySpan().getWeekStart());
		assertEquals(year.intValue(), facade.getActivitySpan().getYearStart());

		// 2. check the activity fulfills the span by fetching the activity info through getAllActicityInfos 
		// we do it twice due to code coverage (test that both methods work as intended!) 
		List<ProjectActivityInfo> infos = facade.getAllProjectActivityInfos(projectId);
		Optional<ProjectActivityInfo> infoOpt = infos.stream().filter(a -> a.getName().equals(activityName)).findFirst();
		assertTrue(infoOpt.isPresent());
		ProjectActivityInfo info = infoOpt.get();
		assertEquals(week.intValue(), info.getStartWeek());
		assertEquals(year.intValue(), info.getStartYear());

		sessionHelper.restoreState();
	}

	@When("I set the end date to week {int} and year {int} for the selected activity")
	public void iSetTheEndDateToWeekAndYearForTheSelectedActivity(Integer week, Integer year) {
		try {
			facade.setActivityEnd(week, year);
		} catch (InvalidSpanException | PermissionDeniedException e) {
			holder.setMessage(e.getMessage());
		}
	}

	@Then("verify the end date is week {int} and year {int} for the activity with name {string} in the project with id {string}")
	public void verifyTheEndDateIsWeekAndYearForTheActivityWithNameInTheProjectWithId(Integer week, Integer year, String activityName, String projectId) throws Exception {
		sessionHelper.saveState();

		facade.signIn("admin");
		facade.selectProject(projectId);

		assertFalse(facade.isActivitySelected());

		facade.selectActivity(activityName);
		assertNotNull(facade.getProjectActivityInfo(activityName));
		

		assertTrue(facade.isActivitySelected());
		assertEquals(week.intValue(), facade.getActivitySpan().getWeekEnd());
		assertEquals(year.intValue(), facade.getActivitySpan().getYearEnd());

		List<ProjectActivityInfo> infos = facade.getAllProjectActivityInfos(projectId);
		Optional<ProjectActivityInfo> infoOpt = infos.stream().filter(a -> a.getName().equals(activityName)).findFirst();
		assertTrue(infoOpt.isPresent());
		ProjectActivityInfo info = infoOpt.get();
		assertEquals(week.intValue(), info.getEndWeek());
		assertEquals(year.intValue(), info.getEndYear());
		
		sessionHelper.restoreState();
	}

	@Given("the start date is week {int} and year {int} for the activity with name {string} in the project with id {string}")
	public void theStartDateIsWeekAndYearForTheActivityWithNameInTheProjectWithId(Integer week, Integer year, String activityName, String projectId) throws Exception {
		sessionHelper.saveState();
		facade.signIn("admin");
		facade.selectProject(projectId);

		facade.selectActivity(activityName);
		assertTrue(facade.isActivitySelected());

		facade.setActivityStart(week, year);

		assertEquals(week.intValue(), facade.getActivitySpan().getWeekStart());
		assertEquals(year.intValue(), facade.getActivitySpan().getYearStart());

		sessionHelper.restoreState();
	}

	@Given("the end date is week {int} and year {int} for the activity with name {string} in the project with id {string}")
	public void theEndDateIsWeekAndYearForTheActivityWithNameInTheProjectWithId(Integer week, Integer year, String activityName, String projectId) throws Exception {
		sessionHelper.saveState();
		facade.signIn("admin");
		facade.selectProject(projectId);
		facade.selectActivity(activityName);
		assertTrue(facade.isActivitySelected());

		facade.setActivityEnd(week, year);

		assertEquals(week.intValue(), facade.getActivitySpan().getWeekEnd());
		assertEquals(year.intValue(), facade.getActivitySpan().getYearEnd());

		sessionHelper.restoreState();
	}

}
