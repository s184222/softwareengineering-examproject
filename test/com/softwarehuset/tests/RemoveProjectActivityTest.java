package com.softwarehuset.tests;

import static org.junit.Assert.assertFalse;

import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.exceptions.NameUnavailableException;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.tests.holders.ErrorMessageHolder;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

// Gustav Gr�nvold s184217
public class RemoveProjectActivityTest {

	private final ProjectManagerFacade facade;
	private final ErrorMessageHolder errorHolder;

	public RemoveProjectActivityTest (ProjectManagerFacade facade, ErrorMessageHolder errorHolder) {
		this.facade = facade;
		this.errorHolder = errorHolder;
	}

	@When("I remove the activity with the name {string}")
	public void iRemoveTheActivityWithTheName(String activityName) {
		try {
			facade.removeActivityInProject(activityName);
		} catch (PermissionDeniedException | NameUnavailableException e) {
			errorHolder.setMessage(e.getMessage());
		}
	}

	@Then("verify the activity with name {string} does not exist")
	public void verifyTheActivityWithNameDoesNotExist(String activityName) {
		assertFalse(facade.getActivityNamesOfSelection().contains(activityName));
	}
}
