package com.softwarehuset.tests.helpers;

import com.softwarehuset.projectmanager.app.ProjectManagerApp;
import com.softwarehuset.projectmanager.exceptions.InvalidEmployeeException;
import com.softwarehuset.projectmanager.selection.SelectionUnion;
import com.softwarehuset.projectmanager.session.Session;
import com.softwarehuset.projectmanager.user.User;

// Rasmus M. Larsen s184190
public class SessionHelper {

	private ProjectManagerApp app;

	private User user;
	private SelectionUnion selection;

	public SessionHelper (ProjectManagerApp app) {
		this.app = app;
	}

	public void saveState() {
		Session session = app.getSession();
		user = session.getUser();
		selection = session.getSelectionUnion();
	}

	public void reloadSelected() {
		app.getSession().setSeletionUnion(selection);
	}

	public void restoreState() throws InvalidEmployeeException {
		if (user != null) app.getSession().signin(user.getId());
		app.getSession().setSeletionUnion(selection);

	}

}
