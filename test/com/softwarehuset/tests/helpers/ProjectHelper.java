package com.softwarehuset.tests.helpers;

import com.softwarehuset.projectmanager.app.ProjectManagerApp;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.projectmanager.exceptions.ProjectIdGenerationException;

// Tobias H. M�ller s184217
public class ProjectHelper {
	
	private ProjectManagerApp app;
	
	public ProjectHelper (ProjectManagerApp app) {
		this.app = app;
	}
	
	public void createSampleProjects(int numberOfProjects) throws PermissionDeniedException, ProjectIdGenerationException {
		for(int i = 0; i < numberOfProjects; i++) {
			app.createProject(String.format("project-%d", i));
		}
	}
}
