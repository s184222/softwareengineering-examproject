package com.softwarehuset.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import com.softwarehuset.projectmanager.app.facade.DefaultProjectManagerFacade;
import com.softwarehuset.projectmanager.app.facade.ProjectActivityInfo;
import com.softwarehuset.projectmanager.exceptions.InvalidHourException;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.tests.helpers.SessionHelper;
import com.softwarehuset.tests.holders.ErrorMessageHolder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

// Tobias H. M�ller s184217
public class SetExpectedHoursOnProjectActivityTest {

	private DefaultProjectManagerFacade facade;
	private ErrorMessageHolder holder;
	private SessionHelper sessionHelper;

	public SetExpectedHoursOnProjectActivityTest (DefaultProjectManagerFacade facade, ErrorMessageHolder holder, SessionHelper sessionHelper) {
		this.facade = facade;
		this.holder = holder;
		this.sessionHelper = sessionHelper;
	}

	@When("I set the expected hours to {int}")
	public void iSetTheExpectedHoursTo(Integer hours) {
		
		try {
			facade.setExpectedHours(hours);
		} catch (InvalidHourException | PermissionDeniedException e) {
			holder.setMessage(e.getMessage());
		}
	}

	@Then("verify the expected hours is {int} for the activity with name {string} in the project with id {string}")
	public void verifyTheExpectedHoursIsForTheActivityWithNameInTheProjectWithId(Integer hours, String activityName, String projectId) throws Exception {
		sessionHelper.saveState();

		facade.signIn("admin");
		facade.selectProject(projectId);
		facade.selectActivity(activityName);

		// 1. check activity selected is correct
		assertTrue(facade.isActivitySelected());
		assertEquals(hours.intValue(), facade.getExpectedHours());

		// 2. check the activity fulfills the span by fetching the activity info through getAllActicityInfos 
		// we do it twice due to code coverage (test that both methods work as intended!) 
		List<ProjectActivityInfo> infos = facade.getAllProjectActivityInfos(projectId);
		Optional<ProjectActivityInfo> infoOpt = infos.stream().filter(a -> a.getName().equals(activityName)).findFirst();
		assertTrue(infoOpt.isPresent());
		ProjectActivityInfo info = infoOpt.get();
		assertEquals(hours.intValue(), info.getExpectedHours());
		
		facade.signOut();

		sessionHelper.restoreState();

	}

	@Given("the the expected hours {int} for the activity with name {string} in the project with id {string}")
	public void theTheExpectedHoursForTheActivityWithNameInTheProjectWithId(Integer hours, String activityName, String projectId) throws Exception {
		sessionHelper.saveState();

		facade.signIn("admin");
		facade.selectProject(projectId);
		facade.selectActivity(activityName);
		facade.setExpectedHours(hours);
		facade.signOut();
		
		sessionHelper.restoreState();
	}

}
