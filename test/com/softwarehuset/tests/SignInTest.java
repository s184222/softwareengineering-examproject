package com.softwarehuset.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.softwarehuset.projectmanager.app.ProjectManagerApp;
import com.softwarehuset.projectmanager.app.facade.DefaultProjectManagerFacade;
import com.softwarehuset.projectmanager.exceptions.InvalidEmployeeException;
import com.softwarehuset.projectmanager.user.Employee;
import com.softwarehuset.tests.holders.ErrorMessageHolder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

// Gustav Gr�nvold s184217
public class SignInTest {

	private final DefaultProjectManagerFacade facade;
	private final ProjectManagerApp app;
	private final ErrorMessageHolder errorHolder;

	public SignInTest (ProjectManagerApp app, DefaultProjectManagerFacade facade, ErrorMessageHolder errorHolder) {
		this.app = app;
		this.facade = facade;
		this.errorHolder = errorHolder;

		facade.attach(app);
	}

	@Given("no employee is currently identified")
	public void noEmployeeIsCurrentlyIdentified() {
		assertFalse(facade.isSignedIn());
	}

	@Given("the employee {string} exists")
	public void theEmployeeExists(String userId) {
		Employee user = new Employee(userId);
		app.addEmployee(user); // internal. feature not supported for initial release. So hide it in the backend!

		assertTrue(facade.getAllUserIds().contains(user.getId()));
		assertTrue(facade.getAllEmployeeIds().contains(user.getId()));
		
	}

	@Given("the employee {string} does not exist")
	public void theEmployeeDoesNotExist(String userId) {
		assertFalse(facade.existsUsers(userId));
	}

	@When("I identify as {string}")
	public void iIdentifyAs(String userId) {
		try {
			facade.signIn(userId);
		} catch (InvalidEmployeeException e) {
			errorHolder.setMessage(e.getMessage());
		}
	}

	@Then("I get the error message {string}")
	public void iGetTheErrorMessage(String msg) {
		assertEquals(msg, errorHolder.getMessage());
	}

	@Then("the system accepts the employee")
	public void theSystemAcceptsTheEmployee() {
		assertTrue(facade.isSignedIn());
	}

	@Then("the currently identified user is not an admin")
	public void theCurrentlyIdentifiedUserIsNotAnAdmin() {
		assertFalse(facade.isAdmin());
	}

	@Then("the system rejects the employee")
	public void theSystemRejectsTheEmployee() {
		assertFalse(facade.isSignedIn());
	}

	@Then("the currently identified user is an admin")
	public void theCurrentlyIdentifiedUserIsAnAdmin() {
		assertTrue(facade.isAdmin());
	}
}
