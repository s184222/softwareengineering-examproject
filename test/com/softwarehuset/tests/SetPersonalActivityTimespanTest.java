package com.softwarehuset.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import com.softwarehuset.projectmanager.app.facade.DefaultProjectManagerFacade;
import com.softwarehuset.projectmanager.app.facade.PersonalActivityInfo;
import com.softwarehuset.tests.helpers.SessionHelper;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

// Rasmus M. Larsen s184190
public class SetPersonalActivityTimespanTest {

	private DefaultProjectManagerFacade facade;
	private SessionHelper sessionHelper;

	public SetPersonalActivityTimespanTest (DefaultProjectManagerFacade facade, SessionHelper sessionHelper) {
		this.facade = facade;
		this.sessionHelper = sessionHelper;
	}

	@Given("the activity with name {string} in the journal of employee {string} exists")
	public void theActivityWithNameInTheJournalOfEmployeeExists(String activityName, String employeeId) throws Exception {
		sessionHelper.saveState();

		facade.signIn(employeeId);

		assertFalse(facade.isActivitySelected());
		
		facade.selectJournal();
		facade.createActivity(activityName);

		facade.selectActivity(activityName);
		assertTrue(facade.isActivitySelected());

		assertTrue(facade.getActivityNamesOfSelection().contains(activityName));
		facade.signOut();

		sessionHelper.restoreState();
	}

	@Then("verify the start date is week {int} and year {int} for the activity with name {string} in the journal of employee {string}")
	public void verifyTheStartDateIsWeekAndYearForTheActivityWithNameInTheJournalOfEmployee(Integer week, Integer year, String activityName, String employeeId) throws Exception {
		sessionHelper.saveState();

		facade.signIn(employeeId);
		facade.selectJournal();

		assertFalse(facade.isActivitySelected());

		facade.selectActivity(activityName);
		assertTrue(facade.isActivitySelected());
		
		assertNotNull(facade.getPersonalActivityInfo(activityName));

		assertTrue(facade.getActivityNamesOfSelection().contains(activityName));
		assertEquals(week.intValue(), facade.getActivitySpan().getWeekStart());
		assertEquals(year.intValue(), facade.getActivitySpan().getYearStart());

		List<PersonalActivityInfo> infos = facade.getAllPersonalActivityInfos();
		Optional<PersonalActivityInfo> infoOpt = infos.stream().filter(a -> a.getName().equals(activityName)).findFirst();
		assertTrue(infoOpt.isPresent());
		PersonalActivityInfo info = infoOpt.get();
		assertEquals(week.intValue(), info.getStartWeek());
		assertEquals(year.intValue(), info.getStartYear());

		facade.signOut();

		sessionHelper.restoreState();
	}

	@Then("verify the end date is week {int} and year {int} for the activity with name {string} in the journal of employee {string}")
	public void verifyTheEndDateIsWeekAndYearForTheActivityWithNameInTheJournalOfEmployee(Integer week, Integer year, String activityName, String employeeId) throws Exception {
		sessionHelper.saveState();

		facade.signIn(employeeId);

		facade.selectJournal();
		facade.selectActivity(activityName);
		assertTrue(facade.isActivitySelected());
		assertTrue(facade.getActivityNamesOfSelection().contains(activityName));
		assertEquals(week.intValue(), facade.getActivitySpan().getWeekEnd());
		assertEquals(year.intValue(), facade.getActivitySpan().getYearEnd());
		
		assertNotNull(facade.getPersonalActivityInfo(activityName));
		
		List<PersonalActivityInfo> infos = facade.getAllPersonalActivityInfos();
		Optional<PersonalActivityInfo> infoOpt = infos.stream().filter(a -> a.getName().equals(activityName)).findFirst();
		assertTrue(infoOpt.isPresent());
		PersonalActivityInfo info = infoOpt.get();
		assertEquals(week.intValue(), info.getEndWeek());
		assertEquals(year.intValue(), info.getEndYear());

		facade.signOut();

		sessionHelper.restoreState();
	}

	@Given("the start date is week {int} and year {int} for the activity with name {string} in the journal of the employee {string}")
	public void theStartDateIsWeekAndYearForTheActivityWithNameInTheJournalOfTheEmployee(Integer week, Integer year, String activityName, String employeeId) throws Exception {
		sessionHelper.saveState();

		facade.signIn(employeeId);

		facade.selectJournal();
		facade.selectActivity(activityName);
		assertTrue(facade.isActivitySelected());

		facade.setActivityStart(week, year);
		

		assertTrue(facade.getActivityNamesOfSelection().contains(activityName));
		assertEquals(week.intValue(), facade.getActivitySpan().getWeekStart());
		assertEquals(year.intValue(), facade.getActivitySpan().getYearStart());

		facade.signOut();

		sessionHelper.restoreState();
	}

	@Given("the end date is week {int} and year {int} for the activity with name {string} in the journal of the employee {string}")
	public void theEndDateIsWeekAndYearForTheActivityWithNameInTheJournalOfTheEmployee(Integer week, Integer year, String activityName, String employeeId) throws Exception {
		sessionHelper.saveState();

		facade.signIn(employeeId);

		facade.selectJournal();
		facade.selectActivity(activityName);
		assertTrue(facade.isActivitySelected());

		facade.setActivityEnd(week, year);

		assertTrue(facade.getActivityNamesOfSelection().contains(activityName));
		assertEquals(week.intValue(), facade.getActivitySpan().getWeekEnd());
		assertEquals(year.intValue(), facade.getActivitySpan().getYearEnd());

		facade.signOut();

		sessionHelper.restoreState();

	}
}
