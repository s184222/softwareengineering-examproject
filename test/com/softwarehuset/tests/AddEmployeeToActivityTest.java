package com.softwarehuset.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.softwarehuset.projectmanager.app.facade.DefaultProjectManagerFacade;
import com.softwarehuset.projectmanager.app.facade.ProjectActivityInfo;
import com.softwarehuset.projectmanager.exceptions.AssignableException;
import com.softwarehuset.projectmanager.exceptions.InvalidEmployeeException;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.tests.helpers.SessionHelper;
import com.softwarehuset.tests.holders.ErrorMessageHolder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

// Tobias H. M�ller s184217
public class AddEmployeeToActivityTest {

	private final DefaultProjectManagerFacade facade;
	private final ErrorMessageHolder errorHolder;
	private final SessionHelper sessionHelper;

	public AddEmployeeToActivityTest (DefaultProjectManagerFacade facade, ErrorMessageHolder errorHolder, SessionHelper sessionHelper) {
		this.facade = facade;
		this.errorHolder = errorHolder;
		this.sessionHelper = sessionHelper;
	}

	@Given("the employee {string} is not assigned to the selected activity")
	public void theEmployeeIsNotAssignedToTheSelectedActivity(String employeeId) {
		assertFalse(facade.isEmployeeAssignedToSelectedActivity(employeeId));
	}

	@When("I assign the employee {string} to the selected activity")
	public void iAssignTheEmployeeToTheSelectedActivity(String employeeId) {
		try {
			facade.assignEmployee(employeeId);
		} catch (PermissionDeniedException | AssignableException | InvalidEmployeeException e) {
			errorHolder.setMessage(e.getMessage());
		}
	}
	
	@Then("verify the employee {string} is assigned to the activity with name {string} in the project with id {string}")
	public void verifyTheEmployeeIsAssignedToTheActivityWithNameInTheProjectWithId(String employeeId, String activityName, String projectId) throws Exception {
		sessionHelper.saveState();
		
		facade.selectProject(projectId);
		facade.selectActivity(activityName);
		
		ProjectActivityInfo info = facade.getProjectActivityInfo(activityName);
		assertTrue(info.getNumberAssigned() > 0);
		
		assertTrue(facade.getAssignedEmployees().contains(employeeId));
		assertTrue(facade.isEmployeeAssignedToSelectedActivity(employeeId));
		
		sessionHelper.restoreState();
	}

	@Then("verify the employee {string} is not assigned to the selected activity")
	public void verifyTheEmployeeIsNotAssignedToTheSelectedActivity(String employeeId) {
		assertFalse(facade.getAssignedEmployees().contains(employeeId));
		assertFalse(facade.isEmployeeAssignedToSelectedActivity(employeeId));
	}

	@Given("the employee {string} is not assigned to the activity with name {string} in the project with id {string}")
	public void theEmployeeIsNotAssignedToTheActivityWithNameInTheProjectWithId(String employeeId, String activityName, String projectId) throws Exception {
		sessionHelper.saveState();

		facade.signIn("admin");
		facade.selectProject(projectId);
		facade.selectActivity(activityName);
		assertFalse(facade.isEmployeeAssignedToSelectedActivity(employeeId));

		facade.signOut();

		sessionHelper.restoreState();
	}

	@Given("the employee {string} is assigned to the activity with name {string} in the project with id {string}")
	public void theEmployeeIsAssignedToTheActivityWithNameInTheProjectWithId(String employeeId, String activityName, String projectId) throws Exception {
		sessionHelper.saveState();

		facade.signIn("admin");
		facade.selectProject(projectId);
		facade.selectActivity(activityName);

		facade.assignEmployee(employeeId);
		assertTrue(facade.isEmployeeAssignedToSelectedActivity(employeeId));

		facade.signOut();

		sessionHelper.restoreState();

	}

	@Given("I selected the activity with name {string}")
	public void iSelectedTheActivityWithName(String activityName) {
		facade.selectActivity(activityName);
	}

}
