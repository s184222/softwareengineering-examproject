package com.softwarehuset.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import com.softwarehuset.projectmanager.app.ProjectManagerApp;
import com.softwarehuset.projectmanager.app.facade.DefaultProjectManagerFacade;
import com.softwarehuset.projectmanager.app.facade.ProjectInfo;
import com.softwarehuset.projectmanager.exceptions.InvalidEmployeeException;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.tests.helpers.SessionHelper;
import com.softwarehuset.tests.holders.ErrorMessageHolder;
import com.softwarehuset.tests.holders.MockProjectIdGenerator;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

// Christian M. Fuglsang s184222
public class ChooseProjectManagerTest {

	private final ErrorMessageHolder errorHolder;
	private final SessionHelper sessionHelper;

	private final MockProjectIdGenerator projectIdGenerator;

	private DefaultProjectManagerFacade facade;

	public ChooseProjectManagerTest (ProjectManagerApp app, DefaultProjectManagerFacade facade, ErrorMessageHolder errorHolder, SessionHelper sessionHelper) {
		this.facade = facade;
		this.errorHolder = errorHolder;
		this.sessionHelper = sessionHelper;

		facade.attach(app);

		projectIdGenerator = new MockProjectIdGenerator(app);
	}

	@Given("the project with id {string} exists")
	public void theProjectWithIdExists(String projectId) throws Exception {
		sessionHelper.saveState();

		projectIdGenerator.setProjectId(projectId);
		
		facade.signIn("admin");
		facade.createProject("fly");
		facade.signOut();

		sessionHelper.restoreState();
	}

	@Given("the employee {string} is not project manager of project with id {string}")
	public void theEmployeeIsNotProjectManagerOfProjectWithId(String employeeId, String projectId) throws Exception {
		sessionHelper.saveState();

		facade.signIn("admin");

		facade.selectProject(projectId);
		assertNotEquals(employeeId, facade.getProjectManagerId());

		facade.signOut();

		sessionHelper.restoreState();
	}

	@When("I assign the employee {string} as project manager")
	public void iAssignTheEmployeeAsProjectManager(String employeeId) {
		try {
			facade.setProjectManager(employeeId);
		} catch (InvalidEmployeeException | PermissionDeniedException e) {
			errorHolder.setMessage(e.getMessage());
		}
	}

	@Then("verify the employee {string} is project manager for the project with id {string}")
	public void verifyTheEmployeeIsProjectManagerForTheProjectWithId(String employeeId, String projectId) throws Exception {
		sessionHelper.saveState();

		facade.signIn("admin");
		facade.selectProject(projectId);

		assertEquals(employeeId, facade.getProjectManagerId());
		
		ProjectInfo info = facade.getProjectInfoFromId(projectId);
		assertEquals(employeeId, info.getProjectManagerId());

		facade.signOut();

		sessionHelper.restoreState();
	}

	@Then("verify the employee {string} is not project manager for the project with id {string}")
	public void verifyTheEmployeeIsNotProjectManagerForTheProjectWithId(String employeeId, String projectId) throws Exception {
		sessionHelper.saveState();

		facade.signIn("admin");
		facade.selectProject(projectId);

		assertNotEquals(employeeId, facade.getProjectManagerId());

		facade.signOut();

		sessionHelper.restoreState();
	}

}
