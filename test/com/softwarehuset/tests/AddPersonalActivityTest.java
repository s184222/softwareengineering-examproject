package com.softwarehuset.tests;

import static org.junit.Assert.assertTrue;

import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

// Christian M. Fuglsang s184222
public class AddPersonalActivityTest {

	private final ProjectManagerFacade facade;

	public AddPersonalActivityTest (ProjectManagerFacade app) {
		this.facade = app;
	}

	@Given("I selected the journal")
	public void iSelectedTheJournal() {
		facade.selectJournal();
	}

	@Then("verify the activity with name {string} in the journal exists")
	public void verifyTheActivityWithNameInTheJournalExists(String activityName) {
		assertTrue(facade.getActivityNamesOfSelection().contains(activityName));
	}
}
