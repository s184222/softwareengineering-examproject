package com.softwarehuset.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import com.softwarehuset.projectmanager.app.ProjectManagerApp;
import com.softwarehuset.projectmanager.app.facade.ProjectInfo;
import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.projectmanager.exceptions.ProjectIdGenerationException;
import com.softwarehuset.tests.helpers.ProjectHelper;
import com.softwarehuset.tests.holders.ErrorMessageHolder;
import com.softwarehuset.tests.holders.MockDateHolder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

// Rasmus M. Larsen
public class CreateProjectTest {

	private final ProjectManagerFacade facade;
	private final ErrorMessageHolder errorHolder;

	private MockDateHolder dateHolder;
	private ProjectHelper helper;

	public CreateProjectTest (ProjectManagerApp app, ProjectManagerFacade facade, ErrorMessageHolder errorHolder, ProjectHelper helper) {
		this.facade = facade;
		this.errorHolder = errorHolder;
		this.helper = helper;

		dateHolder = new MockDateHolder(app);
	}

	@Given("the year is {int}")
	public void theYearIs(Integer year) {
		dateHolder.setYear(year);
	}

	@Given("the week is {int}")
	public void theWeekIs(Integer week) {
		dateHolder.setWeek(week);
	}

	@Given("{int} projects exist")
	public void projectsExist(Integer numberOfProjects) throws Exception {
		helper.createSampleProjects(numberOfProjects);
	}

	@Given("I am identified as {string}")
	public void iAmIdentifiedAs(String userId) throws Exception {
		facade.signIn(userId);
		
		assertEquals(userId, facade.getUserId());
	}

	@When("I create a project with name {string}")
	public void iCreateAProjectWithName(String projectName) {
		try {
			facade.createProject(projectName);
		} catch (PermissionDeniedException | ProjectIdGenerationException e) {
			errorHolder.setMessage(e.getMessage());
		}
	}

	@Then("verify the project with id {string} and name {string} exists")
	public void verifyTheProjectWithIdAndNameExists(String projectId, String projectName) {
		assertTrue(facade.getAllProjectIds().contains(projectId));

		// 1.
		ProjectInfo info = facade.getProjectInfoFromId(projectId);
		assertEquals(projectId, info.getProjectId());
		assertEquals(projectName, info.getName());

		// 2.
		// we do it twice due to code coverage (test that both facade methods work as intended!) 
		List<ProjectInfo> infos = facade.getAllProjectInfos();
		Optional<ProjectInfo> infoOpt = infos.stream().filter(a -> a.getProjectId().equals(projectId)).findFirst();
		assertTrue(infoOpt.isPresent());
		ProjectInfo info2 = infoOpt.get();
		assertEquals(projectName, info2.getName());

	}

	@Then("verify the project with id {string} and name {string} does not exist")
	public void verifyTheProjectWithIdAndNameDoesNotExist(String projectId, String projectName) {
		assertFalse(facade.getAllProjectIds().contains(projectId));
		
		List<ProjectInfo> infos = facade.getAllProjectInfos();
		Optional<ProjectInfo> infoOpt = infos.stream().filter(a -> a.getProjectId().equals(projectId)).findFirst();
		assertFalse(infoOpt.isPresent());
	}

	@Given("a project with name {string} exists")
	public void aProjectWithNameExists(String projectName) throws Exception {
		facade.createProject(projectName);
		//		assertTrue(app.hasProject(projectName));
	}
}
