package com.softwarehuset.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.softwarehuset.projectmanager.app.facade.ProjectInfo;
import com.softwarehuset.projectmanager.app.facade.ProjectManagerFacade;
import com.softwarehuset.projectmanager.exceptions.NameUnavailableException;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.tests.helpers.SessionHelper;
import com.softwarehuset.tests.holders.ErrorMessageHolder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

// Rasmus M. Larsen s184190
public class AddProjectActivityTest {

	private final ProjectManagerFacade facade;
	private final ErrorMessageHolder errorHolder;
	private final SessionHelper sessionHelper;

	public AddProjectActivityTest (ProjectManagerFacade app, ErrorMessageHolder errorHolder, SessionHelper sessionHelper) {
		this.facade = app;
		this.errorHolder = errorHolder;
		this.sessionHelper = sessionHelper;
	}

	@Given("the activity with name {string} in the project with id {string} does not exist")
	public void theActivityWithNameInTheProjectWithIdDoesNotExist(String activityName, String projectId) throws Exception {
		sessionHelper.saveState();

		facade.signIn("admin");
		facade.selectProject(projectId);

		assertFalse(facade.getActivityNamesOfSelection().contains(activityName));
		
		facade.unselect();
		facade.signOut();

		sessionHelper.restoreState();
	}

	@Then("verify the activity with name {string} in the project with id {string} does not exist")
	public void verifyTheActivityWithNameInTheProjectWithIdDoesNotExist(String activityName, String projectId) throws Exception {
		sessionHelper.saveState();

		facade.signIn("admin");
		facade.selectProject(projectId);
		
		assertFalse(facade.getActivityNamesOfSelection().contains(activityName));

		facade.unselect();
		facade.signOut();

		sessionHelper.restoreState();
	}

	@When("I add an activity with the name {string}")
	public void iAddAnActivityWithTheName(String activityName) {
		try {
			facade.createActivity(activityName);
		} catch (PermissionDeniedException | NameUnavailableException e) {
			errorHolder.setMessage(e.getMessage());
		}
	}

	@Given("the employee {string} is project manager of the project with id {string}")
	public void theEmployeeIsProjectManagerOfTheProjectWithId(String employeeId, String projectId) throws Exception {
		sessionHelper.saveState();

		facade.signIn("admin");
		facade.selectProject(projectId);
		facade.setProjectManager(employeeId);
		facade.signOut();

		sessionHelper.restoreState();
	}

	@Given("I selected the project with id {string}")
	public void iSelectedTheProjectWithId(String projectId) {
		facade.selectProject(projectId);
	}

	@Given("the activity with name {string} in the project with id {string} exists")
	public void theActivityWithNameInTheProjectWithIdExists(String activityName, String projectId) throws Exception {
		sessionHelper.saveState();

		facade.signIn("admin");

		facade.selectProject(projectId);
		facade.createActivity(activityName);

		assertTrue(facade.getActivityNamesOfSelection().contains(activityName));

		facade.unselect();
		facade.signOut();

		sessionHelper.restoreState();

	}
	
	@Then("verify the activity with name {string} in the project with id {string} exists")
	public void verifyTheActivityWithNameInTheProjectWithIdExists(String activityName, String projectId) throws Exception {
		sessionHelper.saveState();

		facade.signIn("admin");
		facade.selectProject(projectId);
		
		assertTrue(facade.getActivityNamesOfSelection().contains(activityName));

		ProjectInfo info = facade.getProjectInfoFromId(projectId);
		assertTrue(info.getNumberOfActivities() > 0);
		
		facade.unselect();
		facade.signOut();

		sessionHelper.restoreState();
	}

}
