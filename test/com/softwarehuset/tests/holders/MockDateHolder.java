package com.softwarehuset.tests.holders;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.softwarehuset.projectmanager.app.ProjectManagerApp;
import com.softwarehuset.projectmanager.date.DateServer;

// Rasmus M. Larsen s184190
public class MockDateHolder {

	DateServer dateServer = mock(DateServer.class);

	public MockDateHolder (ProjectManagerApp app) {
		setRealMethodCalls();
		app.setDateServer(dateServer);
	}

	public void setYear(int year) {
		when(this.dateServer.getYear()).thenReturn(year);
	}
	
	public void setWeek(int week) {
		when(this.dateServer.getWeek()).thenReturn(week);
	}

	public void setDate(String date) {
		when(this.dateServer.getDate()).thenReturn(date);
	}

	private void setRealMethodCalls() {
		when(this.dateServer.getPartialYear()).thenCallRealMethod();
	}
}
