package com.softwarehuset.tests.holders;

// Christian M. Fuglsang s184222
public class ErrorMessageHolder {

	private String message;
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
}
