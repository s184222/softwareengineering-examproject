package com.softwarehuset.tests.holders;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.softwarehuset.projectmanager.app.ProjectManagerApp;
import com.softwarehuset.projectmanager.exceptions.ProjectIdGenerationException;
import com.softwarehuset.projectmanager.project.DefaultProjectIdGenerator;

// Gustav Gr�nvold s184217
public class MockProjectIdGenerator {
	
	private DefaultProjectIdGenerator generator = mock(DefaultProjectIdGenerator.class);
	private ProjectManagerApp app;
	
	public MockProjectIdGenerator (ProjectManagerApp app) {
		this.app = app;
		
		app.setProjectIdGenerator(generator);
	}
	
	public void setProjectId(String id) throws ProjectIdGenerationException {
		when(generator.generateProjectId(app.getDateserver())).thenReturn(id);
	}
}
