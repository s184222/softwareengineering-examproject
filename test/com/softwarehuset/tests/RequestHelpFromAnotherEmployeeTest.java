package com.softwarehuset.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.softwarehuset.projectmanager.app.ProjectManagerApp;
import com.softwarehuset.projectmanager.app.facade.DefaultProjectManagerFacade;
import com.softwarehuset.projectmanager.exceptions.InvalidEmployeeException;
import com.softwarehuset.projectmanager.exceptions.InvalidHourException;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.tests.holders.ErrorMessageHolder;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

// Gustav Gr�nvold s184217
public class RequestHelpFromAnotherEmployeeTest {

	private DefaultProjectManagerFacade facade;
	private ErrorMessageHolder holder;

	public RequestHelpFromAnotherEmployeeTest (ProjectManagerApp app, DefaultProjectManagerFacade facade, ErrorMessageHolder holder) {
		this.facade = facade;
		this.holder = holder;
		
		facade.attach(app);
	}

	@When("I request help from an employee named {string} with the selected activity")
	public void iRequestHelpFromAnEmployeeNamedWithTheSelectedActivity(String employeeId) {
		
		try {
			facade.requestHelp(employeeId);
			
			assertTrue(facade.getAvailableEmployeeIdsToday().contains(employeeId));
		} catch (InvalidEmployeeException | PermissionDeniedException | InvalidHourException e) {
			holder.setMessage(e.getMessage());
		}

	}

	@Then("verify the employee {string} has foreign registrations on selected activity")
	public void verifyTheEmployeeHasForeignRegistrationsOnSelectedActivity(String employeeId) {
		assertEquals(1, facade.countPendingWorkRegistrations(employeeId));
	}

	@Then("verify the employee {string} has no foreign registrations on selected activity")
	public void verifyTheEmployeeHasNoForeignRegistrationsOnSelectedActivity(String employeeId) {
		assertEquals(0, facade.countPendingWorkRegistrations(employeeId));
	}
	

}
