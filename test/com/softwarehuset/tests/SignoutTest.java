package com.softwarehuset.tests;

import static org.junit.Assert.assertFalse;

import com.softwarehuset.projectmanager.app.facade.DefaultProjectManagerFacade;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

// Tobias H. M�ller s184217
public class SignoutTest {
	
	private final DefaultProjectManagerFacade facade;
	
	public SignoutTest(DefaultProjectManagerFacade facade) {
		this.facade = facade;
	}
	
	@When("I sign out")
	public void iSignOut() {
		facade.signOut();
	}

	@Then("verify i am signed out")
	public void verifyIAmSignedOut() {
		assertFalse(facade.isSignedIn());
	}

}
