package com.softwarehuset.whitebox.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.softwarehuset.projectmanager.selection.JournalSelection;
import com.softwarehuset.projectmanager.selection.ProjectSelection;
import com.softwarehuset.projectmanager.user.Employee;

// Gustav Gr�nvold s184217
public class SelectActivityWhiteboxTest extends WhiteBoxTest {

	/*
	 * -------------- Table 1 ---------------
	 * Path				           	        Input data set				 Input Property
	 * 1 true, not 2                  A                      existing activity (Name) in selection + selectionType is journal
	 * not 1, 2 true                  B                      existing activity (Name) in selection + selectionType is project
	 * 
	 */

	/*
	 * -------------- Table 2 ---------------
	 * Input data set              Contents                            Expected Output
	 * A                           "review", JournalSelection					 review is selected in "journal selection"-mode
	 * B													 "review", ProjectSelection					 review is selected in "project selection"-mode
	 */

	@Test
	public void testInputDataSetA() throws Exception {
		// setup
		app.addEmployee(new Employee("ABCD"));

		facade.signIn("ABCD");
		facade.selectJournal();
		facade.createActivity("review");

		// action
		app.selectActivity("review");

		// assertion
		JournalSelection selection = app.getSession().getSelectionUnion().get();
		assertEquals(selection.getJournal().getActivity("review"), selection.getActivity());
	}

	@Test
	public void testInputDataSetB() throws Exception {
		// setup
		app.addEmployee(new Employee("ABCD"));

		facade.signIn("admin");
		String id = facade.createProject("MyProject");
		facade.createActivity("review");
		facade.signOut();
		facade.signIn("ABCD");

		facade.selectProject(id);

		// action
		app.selectActivity("review");

		// assertion
		ProjectSelection selection = app.getSession().getSelectionUnion().get();
		assertEquals(selection.getProject().getActivity("review"), selection.getActivity());
	}
}
