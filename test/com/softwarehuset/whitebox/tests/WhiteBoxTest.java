package com.softwarehuset.whitebox.tests;

import com.softwarehuset.projectmanager.app.ProjectManagerApp;
import com.softwarehuset.projectmanager.app.facade.DefaultProjectManagerFacade;

// Christian M. Fuglsang s184222
public class WhiteBoxTest {

	protected final ProjectManagerApp app;
	protected final DefaultProjectManagerFacade facade;

	public WhiteBoxTest () {
		this.app = new ProjectManagerApp();
		this.facade = new DefaultProjectManagerFacade(app);
	}
}
