package com.softwarehuset.whitebox.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.softwarehuset.projectmanager.exceptions.AssignableException;
import com.softwarehuset.projectmanager.exceptions.InvalidEmployeeException;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.projectmanager.selection.ProjectSelection;
import com.softwarehuset.projectmanager.user.Employee;
// Christian M. Fuglsang s184222
public class AssignEmployeeWhiteboxTest extends WhiteBoxTest {

	/*
	 * -------------- Table 1 ---------------
	 * Path				           	        Input data set				 Input Property
	 * 1 true				                  A                      employee that assigns does not have the rights to assign
	 * not 1, 2 true,                 B                      employee has the right, and employee that gets assigned doesn't exist
	 * not 1, not 2, 3 true           C											 employee has the right, and registers an already registered employee to the activity
	 * not 1, not 2, not 3, 4 true    D                      employee that assigns has the rights and and registers an employee to the activity
	 */

	/*
	 * -------------- Table 2 ---------------
	 * Input data set              Contents                											                 Expected Output
	 * A                           "RFLF", (no rights user is doing the action)	                 PermissiondeniedException
	 * B                           "KNIX", (doesn't exist) (no rights user is doing the action)	 InvalidEmployeeException
	 * C                           "RFLF" (already registered), admin is doing the action        AssignableException
	 * D													 "RFLF" (exists), admin is doing the action)	           			 "RFLF" is assigned to the selected activity
	 */

	@Test(expected = PermissionDeniedException.class)
	public void testInputDataSetA() throws Exception {
		// setup
		app.addEmployee(new Employee("ABCD"));
		app.addEmployee(new Employee("rege"));

		facade.signIn("admin");
		String id = facade.createProject("SpaceX");

		facade.selectProject("admin");
		facade.selectProject(id);
		facade.createActivity("stage 1 - early life");
		facade.signOut();

		facade.signIn("rege");
		facade.selectProject(id);
		facade.selectActivity("stage 1 - early life");

		// action
		facade.assignEmployee("ABCD");

	}
	
	@Test(expected = InvalidEmployeeException.class)
	public void testInputDataSetB() throws Exception {
		// setup

		facade.signIn("admin");
		String id = facade.createProject("SpaceX");

		facade.selectProject("admin");
		facade.selectProject(id);
		facade.createActivity("stage 1 - early life");

		// action
		facade.assignEmployee("ABCD");

	}
	
	@Test(expected = AssignableException.class)
	public void testInputDataSetC() throws Exception {
		// setup
		app.addEmployee(new Employee("ABCD"));

		facade.signIn("admin");
		String id = facade.createProject("SpaceX");

		facade.selectProject("admin");
		facade.selectProject(id);
		facade.createActivity("stage 1 - early life");
		facade.selectActivity("stage 1 - early life");

		app.assignEmployee("ABCD");

		// action
		app.assignEmployee("ABCD");
	}

	@Test
	public void testInputDataSetD() throws Exception {
		// setup
		app.addEmployee(new Employee("ABCD"));

		facade.signIn("admin");
		String id = facade.createProject("SpaceX");

		facade.selectProject("admin");
		facade.selectProject(id);
		facade.createActivity("stage 1 - early life");
		facade.selectActivity("stage 1 - early life");

		// action
		app.assignEmployee("ABCD");

		// assertion
		ProjectSelection selection = app.getSession().getSelectionUnion().get();
		assertTrue(selection.getActivity().hasEmployee("ABCD"));
	}

}
