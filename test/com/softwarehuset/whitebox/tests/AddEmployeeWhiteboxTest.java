package com.softwarehuset.whitebox.tests;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.softwarehuset.projectmanager.user.Employee;

// Tobias H. M�ller s184217
public class AddEmployeeWhiteboxTest extends WhiteBoxTest {

	/*
	 * -------------- Table 1 ---------------
	 * Path				           	Input data set				Input Property
	 * 1 true 				    		A						         	An employee
	 */

	/*
	 * -------------- Table 2 ---------------
	 * Input data set              	Contents                	Expected Output
	 * A						               	Employee with name "ABCD"	Employee "ABCD" added to database
	 */

	 
	@Test 
	public void testInputDataSetA() throws Exception {
		// action
		app.addEmployee(new Employee("ABCD"));
		// assertion
		assertNotNull(app.getDatabase().fetchUser("ABCD"));
	}

}
