package com.softwarehuset.whitebox.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.softwarehuset.projectmanager.exceptions.InvalidEmployeeException;
import com.softwarehuset.projectmanager.exceptions.PermissionDeniedException;
import com.softwarehuset.projectmanager.user.Employee;
import com.softwarehuset.tests.holders.MockDateHolder;

// Rasmus M. Larsen
public class RequestHelpWhiteboxTest extends WhiteBoxTest {

	/*
	 * -------------- Table 1 ---------------
	 * Path				           	            Input data set				 Input Property
	 * 1 true                             A                      Not authorized to request help (not part of the activity)
	 * not 1, 2 true                      B                      requested employee does not exist + employee is authorized to request help
	 * not 1, not 2, 3 true               C                      requested employee already assigned to activity + employee is authorized to request help
	 * not 1, not 2, not 3, 4 true        D                      requested employee is busy + employee is authorized to request help
	 * not 1, not 2, not 3, not 4, 5 true	E											 employee is authorized to request help + requested employee is exists, available and not assigned to the activity
	 */

	/*
	 * -------------- Table 2 ---------------
	 * Input data set              Contents                                                	        Expected Output
	 * A                           "ABCD", signed in "AAAA" (not assigned to activity)			 				PermissionDeniedException
	 * B                           "ABCD" (doesn't exist), signed in "admin"          			 				InvalidEmployeeException
	 * C                           "ABCD", signed in "admin", pre-registered "ABCD"   			 				InvalidEmployeeException
	 * D                           "ABCD", signed in "admin", personal activity in the span  				InvalidEmployeeException
	 * E                           "ABCD" (exists,available and not registered), signed in "admin"  "ABCD" has pending work registration
	 */

	@Test(expected = PermissionDeniedException.class)
	public void testInputDataSetA() throws Exception {
		// setup
		app.addEmployee(new Employee("AAAA"));

		facade.signIn("admin");
		String id = facade.createProject("MyProject");
		facade.selectProject(id);
		facade.createActivity("MyActivity");
		facade.signOut();

		facade.signIn("AAAA");
		facade.selectProject(id);
		facade.selectActivity("MyActivity");

		// action
		facade.requestHelp("ABCD");
	}

	@Test(expected = InvalidEmployeeException.class)
	public void testInputDataSetB() throws Exception {
		// setup
		facade.signIn("admin");
		String id = facade.createProject("MyProject");
		facade.selectProject(id);
		facade.createActivity("MyActivity");
		facade.selectActivity("MyProject");

		// action
		facade.requestHelp("ABCD");
	}

	@Test(expected = InvalidEmployeeException.class)
	public void testInputDataSetC() throws Exception {
		// setup
		app.addEmployee(new Employee("ABCD"));

		facade.signIn("admin");
		String id = facade.createProject("MyProject");
		facade.selectProject(id);
		facade.createActivity("MyActivity");
		facade.selectActivity("MyActivity");
		facade.assignEmployee("ABCD");

		// action
		app.requestHelp("ABCD");
	}

	@Test(expected = InvalidEmployeeException.class)
	public void testInputDataSetD() throws Exception {
		MockDateHolder mockDate = new MockDateHolder(app);
		mockDate.setYear(2010);
		mockDate.setWeek(10);

		// setup
		app.addEmployee(new Employee("ABCD"));

		facade.signIn("ABCD");
		facade.selectJournal();
		facade.createActivity("Couple retrieve to Hawaii");
		facade.selectActivity("Couple retrieve to Hawaii");
		facade.setActivityStart(8, 2010);
		facade.setActivityEnd(11, 2010);
		facade.signOut();

		facade.signIn("admin");
		String id = facade.createProject("MyProject");
		facade.selectProject(id);
		facade.createActivity("MyActivity");
		facade.selectActivity("MyActivity");

		// action
		app.requestHelp("ABCD");
	}

	@Test
	public void testInputDataSetE() throws Exception {
		// setup
		app.addEmployee(new Employee("ABCD"));

		facade.signIn("admin");
		String id = facade.createProject("MyProject");
		facade.selectProject(id);
		facade.createActivity("MyActivity");
		facade.selectActivity("MyActivity");

		// action
		app.requestHelp("ABCD");

		// assertion
		assertEquals(1, facade.countPendingWorkRegistrations("ABCD"));
	}

}
