Feature: Add activity 
	Description: Add an activity to a project
	Actors: Project manager
	
Background: The project associated with the project manager contains a project activity 
	Given the project with id "070001" exists 
	And the employee "RFLF" exists 
	And the employee "RFLF" is project manager of the project with id "070001" 
	
Scenario: Project manager adds an activity to a project 
	Given the activity with name "review2" in the project with id "070001" does not exist 
	And I am identified as "RFLF" 
	And I selected the project with id "070001" 
	When I add an activity with the name "review2" 
	Then verify the activity with name "review2" in the project with id "070001" exists 
	
Scenario: Non-project manager adds an activity to the project 
	Given the employee "KNIX" exists 
	And I am identified as "KNIX" 
	And I selected the project with id "070001" 
	When I add an activity with the name "review" 
	Then verify the activity with name "review" in the project with id "070001" does not exist 
	And I get the error message "not project manager" 
	
Scenario: Project manager adds an existing activity to a project 
	Given the activity with name "review" in the project with id "070001" exists 
	And I am identified as "RFLF" 
	And I selected the project with id "070001" 
	When I add an activity with the name "review" 
	Then verify the activity with name "review" in the project with id "070001" exists 
	And I get the error message "activity already exists in the selected project" 
