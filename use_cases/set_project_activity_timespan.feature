Feature: Set activity timespan 
	Description: The project manager sets the timespan of an existing project activity
	Actors: Project manager

Background: The project associated with the project manager contains an activity 
	Given the project with id "03092" exists 
	And the activity with name "review" in the project with id "03092" exists 
	And the employee "RFLF" exists 
	And the employee "RFLF" is project manager of the project with id "03092" 
	
Scenario: Project manager sets start of activity timespan 
	Given I am identified as "RFLF" 
	And I selected the project with id "03092" 
	And I selected the activity with name "review" 
	When I set the start date to week 8 and year 2018 for the selected activity 
	Then verify the start date is week 8 and year 2018 for the activity with name "review" in the project with id "03092" 
	
Scenario: Project manager sets end of activity timespan 
	Given I am identified as "RFLF" 
	And I selected the project with id "03092" 
	And I selected the activity with name "review" 
	When I set the end date to week 12 and year 2018 for the selected activity 
	Then verify the end date is week 12 and year 2018 for the activity with name "review" in the project with id "03092" 
	
Scenario: Project manager sets negativ start week of activity 
	Given I am identified as "RFLF" 
	And I selected the project with id "03092" 
	And I selected the activity with name "review" 
	And the start date is week 8 and year 2018 for the activity with name "review" in the project with id "03092" 
	When I set the start date to week -1 and year 2018 for the selected activity 
	Then verify the start date is week 8 and year 2018 for the activity with name "review" in the project with id "03092" 
	And I get the error message "invalid start date" 
	
Scenario: Project manager sets to large start week of activity 
	Given I am identified as "RFLF" 
	And I selected the project with id "03092" 
	And I selected the activity with name "review" 
	And the start date is week 8 and year 2018 for the activity with name "review" in the project with id "03092" 
	When I set the start date to week 60 and year 2018 for the selected activity 
	Then verify the start date is week 8 and year 2018 for the activity with name "review" in the project with id "03092" 
	And I get the error message "invalid start date" 
	
Scenario: Project manager sets negativ end week of activity 
	Given I am identified as "RFLF" 
	And I selected the project with id "03092" 
	And I selected the activity with name "review" 
	And the end date is week 12 and year 2018 for the activity with name "review" in the project with id "03092" 
	When I set the end date to week -12 and year 2018 for the selected activity 
	Then verify the end date is week 12 and year 2018 for the activity with name "review" in the project with id "03092" 
	And I get the error message "invalid end date" 
	
Scenario: Project manager sets to large end week of activity 
	Given I am identified as "RFLF" 
	And I selected the project with id "03092" 
	And I selected the activity with name "review" 
	And the end date is week 12 and year 2018 for the activity with name "review" in the project with id "03092" 
	When I set the end date to week 60 and year 2018 for the selected activity 
	Then verify the end date is week 12 and year 2018 for the activity with name "review" in the project with id "03092" 
	And I get the error message "invalid end date" 
	
Scenario: Non Project manager sets the start week of activity 
	Given the employee "KNIX" exists 
	And I am identified as "KNIX" 
	And I selected the project with id "03092" 
	And I selected the activity with name "review" 
	And the start date is week 8 and year 2018 for the activity with name "review" in the project with id "03092" 
	When I set the start date to week 20 and year 2018 for the selected activity 
	Then verify the start date is week 8 and year 2018 for the activity with name "review" in the project with id "03092" 
	And I get the error message "not project manager" 
	
Scenario: Non Project manager sets the end week of activity 
	Given the employee "KNIX" exists 
	And I am identified as "KNIX" 
	And I selected the project with id "03092" 
	And I selected the activity with name "review" 
	And the end date is week 12 and year 2018 for the activity with name "review" in the project with id "03092" 
	When I set the end date to week 20 and year 2018 for the selected activity 
	Then verify the end date is week 12 and year 2018 for the activity with name "review" in the project with id "03092" 
	And I get the error message "not project manager"