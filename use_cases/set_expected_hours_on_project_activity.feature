Feature: Set expected hours on activity
	Description: set the expected hours on an activity
	Actors: Project manager
	
Background: The project associated with the project manager contains an activity 
	Given the project with id "03092" exists 
	And the activity with name "review" in the project with id "03092" exists 
	And the employee "RFLF" exists 
	And the employee "RFLF" is project manager of the project with id "03092" 
	
Scenario: Project manager set valid expected hours on activity to a project 
	Given I am identified as "RFLF" 
	And I selected the project with id "03092" 
	And I selected the activity with name "review" 
	When I set the expected hours to 9
	Then verify the expected hours is 9 for the activity with name "review" in the project with id "03092" 

Scenario: Project manager set invalid (negative) expected hours on activity to a project 
	Given I am identified as "RFLF" 
	And I selected the project with id "03092" 
	And I selected the activity with name "review" 
	And the the expected hours 4 for the activity with name "review" in the project with id "03092" 
	When I set the expected hours to -9
	Then verify the expected hours is 4 for the activity with name "review" in the project with id "03092"
	And I get the error message "invalid hours"
	
Scenario: Non-Project manager set valid expected hours on activity to a project 
	Given the employee "KNIX" exists
	And I am identified as "KNIX" 
	And I selected the project with id "03092" 
	And I selected the activity with name "review" 
	And the the expected hours 4 for the activity with name "review" in the project with id "03092" 
	When I set the expected hours to 8
	Then verify the expected hours is 4 for the activity with name "review" in the project with id "03092"
	And I get the error message "not project manager"
	

