Feature: Set Personal activity timespan 
	Description: The employee sets the timespan of an existing activity
	Actors: Employee

Background: The employee has a personal activity 
	Given the employee "RFLF" exists 
	And the activity with name "Germany holiday 2.0" in the journal of employee "RFLF" exists 
	
Scenario: Employee sets start week of activity timespan 
	Given I am identified as "RFLF" 
	And I selected the journal 
	And I selected the activity with name "Germany holiday 2.0" 
	When I set the start date to week 8 and year 2018 for the selected activity 
	Then verify the start date is week 8 and year 2018 for the activity with name "Germany holiday 2.0" in the journal of employee "RFLF" 
	
Scenario: Employee sets end week of activity timespan 
	Given I am identified as "RFLF" 
	And I selected the journal 
	And I selected the activity with name "Germany holiday 2.0" 
	When I set the end date to week 8 and year 2018 for the selected activity 
	Then verify the end date is week 8 and year 2018 for the activity with name "Germany holiday 2.0" in the journal of employee "RFLF" 
	
Scenario: Employee sets invalid start week of activity 
	Given I am identified as "RFLF" 
	And I selected the journal 
	And I selected the activity with name "Germany holiday 2.0" 
	And the start date is week 3 and year 2018 for the activity with name "Germany holiday 2.0" in the journal of the employee "RFLF" 
	When I set the start date to week -1 and year 2018 for the selected activity 
	Then verify the start date is week 3 and year 2018 for the activity with name "Germany holiday 2.0" in the journal of employee "RFLF" 
	
Scenario: Employee sets invalid end week of activity 
	Given I am identified as "RFLF" 
	And I selected the journal 
	And I selected the activity with name "Germany holiday 2.0" 
	And the end date is week 3 and year 2018 for the activity with name "Germany holiday 2.0" in the journal of the employee "RFLF" 
	When I set the start date to week -1 and year 2018 for the selected activity 
	Then verify the end date is week 3 and year 2018 for the activity with name "Germany holiday 2.0" in the journal of employee "RFLF" 
	