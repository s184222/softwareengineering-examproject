Feature: Choose project manager
	Description: The admin chooses one of the employees as project manager
	Actors: Admin

Scenario: Admin chooses project manager
	Given the project with id "070001" exists
	And the employee "RFLF" exists
	And I am identified as "admin"
	And I selected the project with id "070001"
	When I assign the employee "RFLF" as project manager
	Then verify the employee "RFLF" is project manager for the project with id "070001"
	
Scenario: Admin chooses a non-existing project manager
	Given the project with id "070001" exists
	And the employee "RFLF" does not exist
	And I am identified as "admin"
	And I selected the project with id "070001"
	When I assign the employee "RFLF" as project manager
	Then verify the employee "RFLF" is not project manager for the project with id "070001"
	And I get the error message "employee doesn't exist"
	
Scenario: Non Admin chooses project manager
	Given the project with id "070001" exists
	And the employee "RFLF" exists
	And the employee "RFLF" is not project manager of project with id "070001"
	And the employee "KNIX" exists
	And I am identified as "KNIX"
	And I selected the project with id "070001"
	When I assign the employee "RFLF" as project manager
	Then verify the employee "RFLF" is not project manager for the project with id "070001"
	And I get the error message "not admin"