Feature: Register hours used on activity 
	Description: An employee registers the hours used on an activity
	Actor: Employee
	
Background: The project associated with the project manager contains an activity 
	Given the project with id "03096" exists 
	And the activity with name "review" in the project with id "03096" exists
	
Scenario: Employee registers hours used on activity 
	Given the employee "DDDD" exists 
	And the employee "DDDD" is assigned to the activity with name "review" in the project with id "03096" 
	And I am identified as "DDDD" 
	And I selected the project with id "03096" 
	And I selected the activity with name "review" 
	When I set the hours used to 8.0 on the selected activity on date "08-01-2018" 
	Then verify the hours I used on the activity "review" on date "08-01-2018" is 8.0 
	
Scenario: Employee edits hours used on activity
	Given the employee "XYZW" exists 
	And the employee "XYZW" is assigned to the activity with name "review" in the project with id "03096" 
	And the hours used on the activity with name "review" in the project with id "03096" on the date "10-01-2018" for employee "XYZW" is 5.0
	And the employee "RFLF" exists 
	And the employee "RFLF" is assigned to the activity with name "review" in the project with id "03096" 
	And I am identified as "RFLF" 
	And I selected the project with id "03096" 
	And I selected the activity with name "review" 
	And the hours used on the activity with name "review" in the project with id "03096" on the date "10-01-2018" for employee "RFLF" is 5.0
	When I set the hours used to 8.0 on the selected activity on date "10-01-2018" 
	Then verify the hours I used on the activity "review" on date "10-01-2018" is 8.0 
	
Scenario: Employee registers negative number of hours used on activity 
	Given the employee "RFLF" exists 
	And the employee "RFLF" is assigned to the activity with name "review" in the project with id "03096" 
	And I am identified as "RFLF" 
	And I selected the project with id "03096" 
	And I selected the activity with name "review" 
	When I set the hours used to -6.0 on the selected activity on date "08-01-2018" 
	Then verify the hours I used on the activity "review" on date "08-01-2018" is 0.0 
	And I get the error message "invalid number of hours" 
	
Scenario: Employee registers more than 24 hours used on activity 
	Given the employee "RFLF" exists 
	And the employee "RFLF" is assigned to the activity with name "review" in the project with id "03096" 
	And I am identified as "RFLF" 
	And I selected the project with id "03096" 
	And I selected the activity with name "review" 
	When I set the hours used to 25.0 on the selected activity on date "08-01-2018" 
	Then verify the hours I used on the activity "review" on date "08-01-2018" is 0.0 
	And I get the error message "invalid number of hours" 
	
Scenario: Employee not registered to activity and not foreign worker registers hours used on activity 
	Given the employee "KNIX" exists
	And the employee "KNIX" is not assigned to the activity with name "review" in the project with id "03096" 
	And I am identified as "KNIX" 
	And I selected the project with id "03096" 
	And I selected the activity with name "review" 
	When I set the hours used to 8.0 on the selected activity on date "08-01-2018" 
	Then verify the hours I used on the activity "review" on date "08-01-2018" is 0.0 
	And I get the error message "found no foreign work" 
	
Scenario: Foreign worker registers hours used on activity 
	Given the employee "ABCD" exists 
	And the employee "ABCD" is not assigned to the activity with name "review" in the project with id "03096" 
	And the employee "ABCD" has pending foreign registration for the activity with name "review" in the project with id "03096" on the date "09-01-2018" 
	And I am identified as "ABCD" 
	And I selected the project with id "03096" 
	And I selected the activity with name "review" 
	When I set the hours used to 9.0 on the selected activity on date "09-01-2018" 
	Then verify the hours I used on the activity "review" on date "09-01-2018" is 9.0 
	
Scenario: Foreign worker edits hours used on activity 
	Given the employee "RFLF" exists 
	And the employee "RFLF" is not assigned to the activity with name "review" in the project with id "03096"
	And the hours used on the activity with name "review" in the project with id "03096" on the date "08-03-2018" for employee "RFLF" is 5.0 
	And I am identified as "RFLF" 
	And I selected the project with id "03096" 
	And I selected the activity with name "review" 
	When I set the hours used to 8.0 on the selected activity on date "08-03-2018" 
	Then verify the hours I used on the activity "review" on date "08-03-2018" is 8.0 