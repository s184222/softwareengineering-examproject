Feature: Request help from another employee 
	Description: An employee requests help from another employee for an activity
	Actors: Employee
	
Background: The project associated with the project manager contains an activity 
	Given the project with id "03094" exists 
	And the year is 2018 
	And the week is 10 
	And the activity with name "review" in the project with id "03094" exists 
	
Scenario: Employee requests help from another employee 
	Given the employee "FLAX" exists 
	And the employee "FLAX" is assigned to the activity with name "review" in the project with id "03094" 
	And the employee "RASD" exists 
	And I am identified as "FLAX" 
	And I selected the project with id "03094" 
	And I selected the activity with name "review" 
	When I request help from an employee named "RASD" with the selected activity 
	Then verify the employee "RASD" has foreign registrations on selected activity 
	
Scenario: Employee requests help from another employee from the same activity 
	Given the employee "FLAX" exists 
	And the employee "FLAX" is assigned to the activity with name "review" in the project with id "03094" 
	And the employee "RFLF" exists 
	And the employee "RFLF" is assigned to the activity with name "review" in the project with id "03094" 
	And I am identified as "FLAX" 
	And I selected the project with id "03094" 
	And I selected the activity with name "review" 
	When I request help from an employee named "RFLF" with the selected activity 
	Then verify the employee "RFLF" has no foreign registrations on selected activity 
	
Scenario: 
	Employee not assigned to the activity requests help from another employee 
	Given the employee "KNIX" exists 
	And I am identified as "KNIX" 
	And I selected the project with id "03094" 
	And I selected the activity with name "review" 
	When I request help from an employee named "RFLF" with the selected activity 
	Then verify the employee "RFLF" has no foreign registrations on selected activity 
	And I get the error message "not assigned to selected activity" 
	
Scenario: Employee requests help from non existing employee 
	Given the employee "KNIX" exists 
	And the employee "KNIX" is assigned to the activity with name "review" in the project with id "03094" 
	And I am identified as "KNIX" 
	And I selected the project with id "03094" 
	And I selected the activity with name "review" 
	When I request help from an employee named "RFLF" with the selected activity 
	Then verify the employee "RFLF" has no foreign registrations on selected activity 
	And I get the error message "employee doesn't exist" 
	
Scenario: Employee requests help from an employee that is busy 
	Given the employee "KNIX" exists 
	And the employee "KNIX" is assigned to the activity with name "review" in the project with id "03094" 
	And the employee "RFLF" exists 
	And the activity with name "Germany holiday 1.0" in the journal of employee "RFLF" exists 
	And the start date is week 3 and year 2012 for the activity with name "Germany holiday 1.0" in the journal of the employee "RFLF" 
	And the end date is week 12 and year 2020 for the activity with name "Germany holiday 1.0" in the journal of the employee "RFLF" 
	And I am identified as "KNIX" 
	And I selected the project with id "03094" 
	And I selected the activity with name "review" 
	When I request help from an employee named "RFLF" with the selected activity 
	Then verify the employee "RFLF" has no foreign registrations on selected activity 
	And I get the error message "employee is busy" 
	
Scenario:
Employee requests help from an employee that has personal activities before the request 
	Given the employee "KNIX" exists 
	And the employee "KNIX" is assigned to the activity with name "review" in the project with id "03094" 
	And the employee "RFLF" exists 
	And the activity with name "Germany holiday 1.0" in the journal of employee "RFLF" exists 
	And the start date is week 3 and year 2012 for the activity with name "Germany holiday 1.0" in the journal of the employee "RFLF" 
	And the end date is week 3 and year 2014 for the activity with name "Germany holiday 1.0" in the journal of the employee "RFLF" 
	And I am identified as "KNIX" 
	And I selected the project with id "03094" 
	And I selected the activity with name "review" 
	When I request help from an employee named "RFLF" with the selected activity 
	Then verify the employee "RFLF" has foreign registrations on selected activity 
	
Scenario:
Employee requests help from an employee has personal activities after the request 
	Given the employee "KNIX" exists 
	And the employee "KNIX" is assigned to the activity with name "review" in the project with id "03094" 
	And the employee "RFLF" exists 
	And the activity with name "Germany holiday 1.0" in the journal of employee "RFLF" exists 
	And the start date is week 3 and year 2022 for the activity with name "Germany holiday 1.0" in the journal of the employee "RFLF" 
	And the end date is week 3 and year 2025 for the activity with name "Germany holiday 1.0" in the journal of the employee "RFLF" 
	And I am identified as "KNIX" 
	And I selected the project with id "03094" 
	And I selected the activity with name "review" 
	When I request help from an employee named "RFLF" with the selected activity 
	Then verify the employee "RFLF" has foreign registrations on selected activity 