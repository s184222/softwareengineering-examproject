Feature: Signout
	Description: An employee signs out of management system
	Actors: Employee
	
Scenario: An employee signs out successfully 
	Given the employee "KNIX" exists
	And I am identified as "KNIX"
	When I sign out
	Then verify i am signed out

Scenario: An admin signs out successfully 
	Given I am identified as "admin"
	When I sign out
	Then verify i am signed out