Feature: Remove activity 
	Description: Remove an activity to a project
	Actors: Project manager, Admin
	
Background: The project associated with the project manager contains an activity 
	Given the project with id "070001" exists 
	And the employee "RFLF" exists 
	And the employee "RFLF" is project manager of the project with id "070001" 
	
Scenario: Project manager removes an activity from a project 
	Given I am identified as "RFLF" 
	And I selected the project with id "070001" 
	And the activity with name "review" in the project with id "070001" exists 
	When I remove the activity with the name "review"
	Then verify the activity with name "review" in the project with id "070001" does not exist 
	
Scenario: Non-project manager removes activity to the project 
	Given the employee "KNIX" exists 
	And I am identified as "KNIX" 
	And I selected the project with id "070001" 
	When I remove the activity with the name "review" 
	Then I get the error message "not project manager" 
	
Scenario: Project manager removes an non-existing activity from a project 
	Given I am identified as "RFLF" 
	And I selected the project with id "070001" 
	And the activity with name "review" in the project with id "070001" does not exist 
	When I remove the activity with the name "review" 
	Then I get the error message "Activity does not exist in the selected project" 
	
Scenario: Admin removes an activity from a project 
	Given I am identified as "admin" 
	And I selected the project with id "070001" 
	And the activity with name "review" in the project with id "070001" exists 
	When I remove the activity with the name "review"
	Then verify the activity with name "review2" in the project with id "070001" does not exist 
	
Scenario: Admin removes an non-existing activity from a project 
	Given I am identified as "admin" 
	And I selected the project with id "070001" 
	And the activity with name "review" in the project with id "070001" does not exist 
	When I remove the activity with the name "review" 
	Then I get the error message "Activity does not exist in the selected project"