Feature: Signin/Identify
	Description: An unidentified employee identifies himself to the management system
	Actors: Unidentified employee
	
Scenario: An unidentified non-admin employee identifies himself successfully 
	Given no employee is currently identified
	And the employee "RFLF" exists
	When I identify as "RFLF"
	Then the system accepts the employee
	And the currently identified user is not an admin

Scenario: An unidentified employee identifies himself with incorrect initials
	Given no employee is currently identified
	When I identify as "RFLFF"
	Then the system rejects the employee
	And I get the error message "Incorrect number of initials"

Scenario: An unidentified employee identifies himself as none-existent employee
	Given no employee is currently identified
	And the employee "HOFL" does not exist
	When I identify as "HOFL"
	Then the system rejects the employee
	And I get the error message "Employee not available"

Scenario: An unidentified employee identifies as admin 
	Given no employee is currently identified
	When I identify as "admin"
	Then the currently identified user is an admin