Feature: Add employee to activity 
	Description: The project manager assigns an employee to an activity
	Actors: Project manager
	
Background: The project associated with the project manager contains an activity 
	Given the project with id "03093" exists 
	And the activity with name "review" in the project with id "03093" exists 
	
Scenario: Project manager assigns employee to activity 
	Given the employee "RFLF" exists 
	And the employee "RFLF" is project manager of the project with id "03093" 
	And the employee "FLAX" exists 
	And the employee "FLAX" is not assigned to the activity with name "review" in the project with id "03093" 
	And I am identified as "RFLF" 
	And I selected the project with id "03093" 
	And I selected the activity with name "review" 
	When I assign the employee "FLAX" to the selected activity 
	Then verify the employee "FLAX" is assigned to the activity with name "review" in the project with id "03093" 
	
Scenario: Non Project manager assigns employee to activity 
	Given the employee "KNIX" exists 
	And the employee "ABCD" exists 
	And I am identified as "KNIX" 
	And I selected the project with id "03093" 
	And I selected the activity with name "review" 
	And the employee "ABCD" is not assigned to the selected activity 
	When I assign the employee "ABCD" to the selected activity 
	Then verify the employee "ABCD" is not assigned to the selected activity 
	And I get the error message "not project manager" 
	
Scenario: Project manager assigns already assigned employee to activity 
	Given the employee "RFLF" exists 
	And the employee "RFLF" is project manager of the project with id "03093" 
	And the employee "FLAX" exists 
	And the employee "FLAX" is assigned to the activity with name "review" in the project with id "03093" 
	And I am identified as "RFLF" 
	And I selected the project with id "03093" 
	And I selected the activity with name "review" 
	When I assign the employee "FLAX" to the selected activity
	Then verify the employee "FLAX" is assigned to the activity with name "review" in the project with id "03093"
	Then I get the error message "employee already assigned"

Scenario: Project manager assigns non existing employee to activity 
	Given the employee "RFLF" exists 
	And the employee "RFLF" is project manager of the project with id "03093" 
	And I am identified as "RFLF" 
	And I selected the project with id "03093" 
	And I selected the activity with name "review" 
	When I assign the employee "FLAX" to the selected activity 
	Then verify the employee "FLAX" is not assigned to the selected activity 
	Then I get the error message "employee doesn't exist"