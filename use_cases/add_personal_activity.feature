Feature: Add personal activity 
	Description: Register vacation, illness or courses
	Actors: User
	
Scenario: Employee registers a personal activity 
	Given the employee "RFLF" exists
	And I am identified as "RFLF" 
	And I selected the journal 
	When I add an activity with the name "Spain trip" 
	Then verify the activity with name "Spain trip" in the journal exists 

Scenario: Employee registers a personal activity 
	Given the employee "RFLF" exists
	And I am identified as "RFLF" 
	And I selected the journal 
	And the activity with name "Spain trip" in the journal of employee "RFLF" exists 
	When I add an activity with the name "Spain trip" 
	Then verify the activity with name "Spain trip" in the journal exists
	And I get the error message "activity already exists"
