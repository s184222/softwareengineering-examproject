Feature: Create project
	Description: The admin creates a project
	Actors: Admin

Scenario: Admin creates a project
	Given the year is 2007
	And I am identified as "admin"
	When I create a project with name "Netflax"
	Then verify the project with id "070001" and name "Netflax" exists
	
Scenario: Non admin creates a project
	Given the year is 2007
	And the employee "RFLF" exists
	And I am identified as "RFLF"
	When I create a project with name "Netflax"
	Then verify the project with id "070001" and name "Netflax" does not exist
	And I get the error message "not admin"

Scenario: Admin creates a project with the same name as another project
	Given the year is 2007
	And I am identified as "admin"
	And a project with name "flux" exists
	When I create a project with name "flux"
	Then verify the project with id "070002" and name "flux" exists
	
Scenario: Admin creates one to many projects
	Given the year is 2007
	And I am identified as "admin"
	And 9999 projects exist
	When I create a project with name "Netflax"
	Then verify the project with id "0710000" and name "Netflax" does not exist
	And I get the error message "Project number overflow"